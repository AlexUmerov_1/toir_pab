#Область ОбработчикиСобытийФормы

&НаСервере
Процедура проф_ПриСозданииНаСервереПосле(Отказ, СтандартнаяОбработка)
	
	//++ Проф-ИТ, #265, Башинская А.Ю., 25.09.2023
	Список.ТекстЗапроса = "ВЫБРАТЬ
	|	Справочникторо_ВидыДефектов.Ссылка,
	|	Справочникторо_ВидыДефектов.ПометкаУдаления,
	|	Справочникторо_ВидыДефектов.Родитель,
	|	Справочникторо_ВидыДефектов.ЭтоГруппа,
	|	Справочникторо_ВидыДефектов.Код,
	|	Справочникторо_ВидыДефектов.Наименование, 
	|	Справочникторо_ВидыДефектов.проф_ВидРемонта Как ВидРемонта,
	|	Справочникторо_ВидыДефектов.Комментарий
	|	ИЗ
	|	Справочник.торо_ВидыДефектов КАК Справочникторо_ВидыДефектов";  
	
	НовыйЭлемент  = Элементы.Добавить("ВидРемонта", Тип("ПолеФормы"), Элементы.Список);
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеВвода;      
	НовыйЭлемент.ПутьКДанным = "Список.ВидРемонта";
	//-- Проф-ИТ, #265, Башинская А.Ю., 25.09.2023
	
КонецПроцедуры

#КонецОбласти