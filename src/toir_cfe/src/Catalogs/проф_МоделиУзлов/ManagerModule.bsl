
#Область ОбработчикиСобытий

//++ Проф-ИТ, #262, Горетовская М.С., 20.09.2023

&После("ОбработкаПолученияПредставления")
Процедура проф_ОбработкаПолученияПредставления(Данные, Представление, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	Представление = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Данные.Ссылка, "Наименование");
	
КонецПроцедуры

//-- Проф-ИТ, #262, Горетовская М.С., 20.09.2023

#КонецОбласти