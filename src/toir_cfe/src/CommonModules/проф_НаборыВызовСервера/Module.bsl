////////////////////////////////////////////////////////////////////////////////
// Модуль "НаборыВызовСервера", содержит процедуры и функции для
// работы с наборами.
//
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Процедура дополняет массив строк табличной части оставшимися строками наборов,
// если указанные наборы в массиве строк представлены не полностью.
// 
// Параметры:
//  ТабличнаяЧасть - ТабличнаяЧасть - Табличная часть.
//  МассивСтрок - Массив - Строки табличной части для дополнения.
//  УчитыватьКодСтроки - Булево - Признак учета кода строки.
//
Процедура ДополнитьДоПолногоНабора(ТабличнаяЧасть, МассивСтрок, УчитыватьКодСтроки = Ложь) Экспорт
	
	Если Не ЗначениеЗаполнено(ТабличнаяЧасть) Или Не ЗначениеЗаполнено(МассивСтрок)
		Или Не ЕстьРеквизитОбъекта(ТабличнаяЧасть[0], "НоменклатураНабора") Тогда
		Возврат;
	КонецЕсли;
	
	ТаблицаНоменклатура = Новый ТаблицаЗначений;
	ТаблицаНоменклатура.Колонки.Добавить("НоменклатураНабора", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	ТаблицаНоменклатура.Колонки.Добавить("ХарактеристикаНабора", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	Для Каждого СтрокаТЧ Из МассивСтрок Цикл
		Если ЗначениеЗаполнено(СтрокаТЧ.НоменклатураНабора) Тогда
			НоваяСтрока = ТаблицаНоменклатура.Добавить();
			НоваяСтрока.НоменклатураНабора = СтрокаТЧ.НоменклатураНабора;
			НоваяСтрока.ХарактеристикаНабора = СтрокаТЧ.ХарактеристикаНабора;
		КонецЕсли;
	КонецЦикла;
	ТаблицаНоменклатура.Свернуть("НоменклатураНабора,ХарактеристикаНабора");
	
	Если Не ЗначениеЗаполнено(ТаблицаНоменклатура) Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	Т.НоменклатураНабора,
	|	Т.ХарактеристикаНабора
	|ПОМЕСТИТЬ Таблица
	|ИЗ
	|	&Таблица КАК Т
	|;
	|ВЫБРАТЬ
	|	Т.НоменклатураНабора,
	|	Т.ХарактеристикаНабора,
	|	НЕОПРЕДЕЛЕНО КАК ВариантРасчетаЦеныНабора 
	|ИЗ
	|	Таблица КАК Т
	|");
	
	Запрос.УстановитьПараметр("Таблица", ТаблицаНоменклатура);
	
	Данные = Запрос.Выполнить().Выгрузить();
	
	ДобавляемыеСтроки = Новый Массив;
	Для Каждого СтрокаТЧ Из МассивСтрок Цикл
		
		Если ЗначениеЗаполнено(СтрокаТЧ.НоменклатураНабора) Тогда
			
			Отбор = Новый Структура;
			Отбор.Вставить("НоменклатураНабора", СтрокаТЧ.НоменклатураНабора);
			Отбор.Вставить("ХарактеристикаНабора", СтрокаТЧ.ХарактеристикаНабора);
			
			НайденныеСтроки = Данные.НайтиСтроки(Отбор);
			Если НайденныеСтроки.Количество() > 0 Тогда
				Если НайденныеСтроки[0].ВариантРасчетаЦеныНабора = Перечисления.ВариантыРасчетаЦенНаборов.ЦенаЗадаетсяЗаНаборРаспределяетсяПоДолям
					ИЛИ НайденныеСтроки[0].ВариантРасчетаЦеныНабора = Перечисления.ВариантыРасчетаЦенНаборов.ЦенаЗадаетсяЗаНаборРаспределяетсяПоЦенам Тогда
					НайденныеСтрокиТабличнойЧасти = ТабличнаяЧасть.НайтиСтроки(Отбор);
					Для Каждого НайденнаяСтрока Из НайденныеСтрокиТабличнойЧасти Цикл
						
						Если УчитыватьКодСтроки И НайденнаяСтрока.КодСтроки <> 0 Тогда
							Продолжить;
						КонецЕсли;
						
						Если МассивСтрок.Найти(НайденнаяСтрока) = Неопределено Тогда
							 ДобавляемыеСтроки.Добавить(НайденнаяСтрока);
						КонецЕсли;
						
					КонецЦикла;
				КонецЕсли;
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЦикла;
	
	Для Каждого СтрокаТЧ Из ДобавляемыеСтроки Цикл
		МассивСтрок.Добавить(СтрокаТЧ);
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция КолонкиНабора()
	
	Колонки = Новый Массив;
	Колонки.Добавить("НомерСтроки");
	Колонки.Добавить("НоменклатураКомплекта");
	Колонки.Добавить("ХарактеристикаКомплекта");
	Колонки.Добавить("Номенклатура");
	Колонки.Добавить("Характеристика");
	Колонки.Добавить("Цена");
	Колонки.Добавить("ВидЦены");
	Колонки.Добавить("Упаковка");
	Колонки.Добавить("Количество");
	Колонки.Добавить("КоличествоУпаковок");
	Колонки.Добавить("ПроцентРучнойСкидки");
	Колонки.Добавить("СуммаРучнойСкидки");
	Колонки.Добавить("Доступно");
	Колонки.Добавить("ВНаличии");
	
	Возврат Колонки;
	
КонецФункции

Функция ЕстьРеквизитОбъекта(Объект, ИмяРеквизита) Экспорт
	
	КлючУникальности   = Новый УникальныйИдентификатор;
	СтруктураРеквизита = Новый Структура(ИмяРеквизита, КлючУникальности);

	ЗаполнитьЗначенияСвойств(СтруктураРеквизита, Объект);
	
	Возврат СтруктураРеквизита[ИмяРеквизита] <> КлючУникальности;
	
КонецФункции

#КонецОбласти
