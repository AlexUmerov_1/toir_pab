
#Область ОбработчикиСобытий

// ++ Проф-ИТ, #82, Ровенский О.В., 19.09.2023 

&После("ЗакрытьЗаказыПоставщику")
Процедура проф_ЗакрытьЗаказыПоставщику(Запрос)

	//++ Проф-ИТ, #82, Иванова Е.С., 18.09.2023 
	
	ФОИспользоватьСерии = Константы.ИспользоватьСерииНоменклатуры.Получить();
	ФОИспользоватьХарактеристики = Константы.торо_ИспользоватьХарактеристикиНоменклатуры.Получить();
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	Таблица.Номенклатура КАК Номенклатура,
				   |	Таблица.проф_Назначение КАК Назначение,
	               |	&Характеристика КАК Характеристика,
	               |	СУММА(Таблица.Количество) КАК Количество,
	               |	&Серия КАК Серия,
	               |	Таблица.Склад КАК Склад
	               |ПОМЕСТИТЬ ТабличнаяЧасть
	               |ИЗ
	               |	Документ.ПоступлениеТоваровУслуг.Товары КАК Таблица
	               |ГДЕ
	               |	Таблица.Ссылка = &Ссылка
	               |
	               |СГРУППИРОВАТЬ ПО
	               |	Таблица.Номенклатура,
				   |	Таблица.проф_Назначение,
	               |	Таблица.Склад,
				   |	&Характеристика,
				   |	&Серия
	               |
	               |ИНДЕКСИРОВАТЬ ПО
	               |	Склад,
	               |	Номенклатура,
				   |	проф_Назначение,
	               |	Характеристика,
	               |	Серия";
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Если ФОИспользоватьСерии Тогда 
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Серия", "Таблица.Серия");
	Иначе 
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Серия", "ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)");
	КонецЕсли;

	Если ФОИспользоватьХарактеристики Тогда 
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Характеристика", "Таблица.Характеристика");
	Иначе 
		Запрос.Текст = СтрЗаменить(Запрос.Текст,
							"&Характеристика", "ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)");
	КонецЕсли;

	Запрос.Выполнить();
	
	Движения.проф_ЗаказыПоставщику.Записывать = Истина;
	Запрос.Текст = ТекстЗапросаЗакрытьЗаказыПоставщику();		
	Запрос.УстановитьПараметр("Контрагент", Контрагент);
	Запрос.УстановитьПараметр("Договор", Договор);
	Запрос.УстановитьПараметр("Дата", Новый Граница(ТекущаяДатаСеанса(), ВидГраницы.Включая));

	РезЗапроса = Запрос.Выполнить();
	Таблица = Новый ТаблицаЗначений();
	Таблица.Колонки.Добавить("Серия");
	Таблица.Колонки.Добавить("Списано");
	
	Структура = Новый Структура();
	Структура.Вставить("Серия");
	ВыборкаСкладов = РезЗапроса.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	Пока ВыборкаСкладов.Следующий() Цикл 
		ВыборкаНоменклатур = ВыборкаСкладов.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
		Пока ВыборкаНоменклатур.Следующий() Цикл
			ВыборкаХарактеристик = ВыборкаНоменклатур.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
			Пока ВыборкаХарактеристик.Следующий() Цикл 
				Таблица.Очистить();
				ВыборкаЗаказов = ВыборкаХарактеристик.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
				Пока ВыборкаЗаказов.Следующий() Цикл 
					НадоСписать = ВыборкаЗаказов.НадоСписать;
					ВыборкаДетальныеЗаписи = ВыборкаЗаказов.Выбрать();					
					ОбработатьВыборкуДетальныеЗаписиПоЗаказамПоставщика(ВыборкаДетальныеЗаписи, Таблица, Структура, НадоСписать);										
				КонецЦикла;
			КонецЦикла;
		КонецЦикла;
	КонецЦикла;
	
	//-- Проф-ИТ, #82, Иванова Е.С., 18.09.2023
	
КонецПроцедуры

 
// Возвращаемое значение:
// ТекстЗапроса - Строка, содержит текст запроса для процедуры ЗакрытьЗаказыПоставщику
//
Функция ТекстЗапросаЗакрытьЗаказыПоставщику()
	
	ТекстЗапроса = "ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	проф_ЗаказыПоставщикуОстатки.Номенклатура КАК Номенклатура,
		|	проф_ЗаказыПоставщикуОстатки.Характеристика КАК Характеристика,
		|	проф_ЗаказыПоставщикуОстатки.Склад КАК Склад,
		|	проф_ЗаказыПоставщикуОстатки.ЗаказПоставщику КАК ЗаказПоставщику,
		|	проф_ЗаказыПоставщикуОстатки.КоличествоОстаток КАК КоличествоОстаток,
		|	проф_ЗаказыПоставщикуОстатки.Назначение КАК Назначение
		|ПОМЕСТИТЬ ЗаказыПоставщику
		|ИЗ
		|	РегистрНакопления.проф_ЗаказыПоставщику.Остатки(
		|			&Дата,
		|			(Номенклатура, Характеристика, Склад) В
		|				(ВЫБРАТЬ
		|					ВТ.Номенклатура КАК Номенклатура,
		|					ВТ.Характеристика КАК Характеристика,
		|					ВТ.Склад КАК Склад
		|				ИЗ
		|					ТабличнаяЧасть КАК ВТ)) КАК проф_ЗаказыПоставщикуОстатки
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Номенклатура,
		|	Характеристика,
		|	Склад
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	проф_ЗаказыПоставщикуОстатки.Номенклатура КАК Номенклатура,
		|	ТабличнаяЧасть.Характеристика КАК Характеристика,
		|	ТабличнаяЧасть.Назначение КАК Назначение,
		|	проф_ЗаказыПоставщикуОстатки.Склад КАК Склад,
		|	проф_ЗаказыПоставщикуОстатки.ЗаказПоставщику КАК ЗаказПоставщику,
		|	проф_ЗаказыПоставщикуОстатки.КоличествоОстаток КАК НадоСписать,
		|	СУММА(ТабличнаяЧасть.Количество) КАК Поступило,
		|	ТабличнаяЧасть.Серия КАК Серия
		|ИЗ
		|	ТабличнаяЧасть КАК ТабличнаяЧасть
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ЗаказыПоставщику КАК проф_ЗаказыПоставщикуОстатки
		|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.торо_ЗаказПоставщику КАК торо_ЗаказПоставщику
		|			ПО проф_ЗаказыПоставщикуОстатки.ЗаказПоставщику = торо_ЗаказПоставщику.Ссылка
		|		ПО ТабличнаяЧасть.Склад = проф_ЗаказыПоставщикуОстатки.Склад
		|			И ТабличнаяЧасть.Номенклатура = проф_ЗаказыПоставщикуОстатки.Номенклатура
		|			И ТабличнаяЧасть.Характеристика = проф_ЗаказыПоставщикуОстатки.Характеристика
		|			И ТабличнаяЧасть.Назначение = проф_ЗаказыПоставщикуОстатки.Назначение
		|ГДЕ
		|	торо_ЗаказПоставщику.Договор = &Договор
		|	И торо_ЗаказПоставщику.Контрагент = &Контрагент
		|
		|СГРУППИРОВАТЬ ПО
		|	проф_ЗаказыПоставщикуОстатки.Номенклатура,
		|	ТабличнаяЧасть.Характеристика,
		|	ТабличнаяЧасть.Назначение,
		|	проф_ЗаказыПоставщикуОстатки.Склад,
		|	ТабличнаяЧасть.Серия,
		|	проф_ЗаказыПоставщикуОстатки.ЗаказПоставщику,
		|	проф_ЗаказыПоставщикуОстатки.КоличествоОстаток,
		|	торо_ЗаказПоставщику.Дата,
		|	торо_ЗаказПоставщику.ДатаПоставки
		|
		|УПОРЯДОЧИТЬ ПО
		|	торо_ЗаказПоставщику.ДатаПоставки,
		|	торо_ЗаказПоставщику.Дата
		|ИТОГИ
		|	МИНИМУМ(НадоСписать)
		|ПО
		|	Склад,
		|	Номенклатура,
		|	Характеристика,
		|	ЗаказПоставщику";
	
	Возврат ТекстЗапроса;
	
КонецФункции	

Процедура ОбработатьВыборкуДетальныеЗаписиПоЗаказамПоставщика(ВыборкаДетальныеЗаписи, Таблица, Структура, НадоСписать)
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		
		Если НадоСписать = 0 Тогда
			Прервать;
		КонецЕсли;
		
		Имеется = ВыборкаДетальныеЗаписи.Поступило;
		ЗаполнитьЗначенияСвойств(Структура, ВыборкаДетальныеЗаписи);
		
		ТекСтрока = Неопределено;
		Массив = Таблица.НайтиСтроки(Структура);
		Если Массив.Количество() Тогда
			ТекСтрока = Массив[0];
			Имеется = Имеется - ТекСтрока.Списано;
		Иначе	
			ТекСтрока = Таблица.Добавить();
			ЗаполнитьЗначенияСвойств(ТекСтрока, Структура);
			ТекСтрока.Списано = 0;
		КонецЕсли;
		
		Если Имеется = 0 Тогда
			Продолжить;			 
		КонецЕсли;
		
		Списываем = 0;
		Если Имеется > НадоСписать Тогда
			Списываем = НадоСписать;	
		Иначе
			Списываем = Имеется;	
		КонецЕсли;
		
		НадоСписать = НадоСписать - Списываем;
		
		Движение = Движения.проф_ЗаказыПоставщику.ДобавитьРасход();
		ЗаполнитьЗначенияСвойств(Движение, ВыборкаДетальныеЗаписи);
		Движение.Период = Дата;
		Движение.ДатаПоставки = Дата;
		Движение.Количество = Списываем;
		
		ТекСтрока.Списано = ТекСтрока.Списано + Списываем;
	КонецЦикла;
	
КонецПроцедуры	

//-- Проф-ИТ, #82, Ровенский О.В., 19.09.2023

//++ Проф-ИТ, #501, Ахмадеев И.Г., 06.03.2024
&Вместо("ОбработкаПроведения")
Процедура проф_ОбработкаПроведения(Отказ, РежимПроведения)
	
	ФОИспользоватьСерии = Константы.ИспользоватьСерииНоменклатуры.Получить();
	ФОИспользоватьХарактеристики = Константы.торо_ИспользоватьХарактеристикиНоменклатуры.Получить();
	
	Если НЕ проф_ИзERP Тогда
		// регистр ТоварыНаСкладах Приход
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		               |	ПоступлениеТоваровУслуг.Номенклатура КАК Номенклатура,
		               |	ПоступлениеТоваровУслуг.Характеристика КАК Характеристика,
		               |	СУММА(ПоступлениеТоваровУслуг.Количество) КАК Количество,
		               |	ПоступлениеТоваровУслуг.Серия КАК Серия,
		               |	ПоступлениеТоваровУслуг.Номенклатура.ВидНоменклатуры.ТипНоменклатуры КАК ТипНоменклатуры,
		               |	ПоступлениеТоваровУслуг.Склад КАК Склад
		               |ИЗ
		               |	Документ.ПоступлениеТоваровУслуг.Товары КАК ПоступлениеТоваровУслуг
		               |ГДЕ
		               |	ПоступлениеТоваровУслуг.Ссылка = &Ссылка
		               |
		               |СГРУППИРОВАТЬ ПО
		               |	ПоступлениеТоваровУслуг.Номенклатура,
		               |	ПоступлениеТоваровУслуг.Характеристика,
		               |	ПоступлениеТоваровУслуг.Серия,
		               |	ПоступлениеТоваровУслуг.Номенклатура.ВидНоменклатуры.ТипНоменклатуры,
		               |	ПоступлениеТоваровУслуг.Склад";
		Запрос.УстановитьПараметр("Ссылка", Ссылка);
		ТЧТовары = Запрос.Выполнить().Выгрузить();

		Движения.ТоварыНаСкладах.Записывать = Истина;
		Для Каждого ТекСтрокаТовары Из ТЧТовары Цикл
			Если ТекСтрокаТовары.ТипНоменклатуры = Перечисления.ТипыНоменклатуры.Товар 
				ИЛИ ТекСтрокаТовары.ТипНоменклатуры = Перечисления.ТипыНоменклатуры.МногооборотнаяТара Тогда
				
				Движение = Движения.ТоварыНаСкладах.Добавить();
				Движение.ВидДвижения    = ВидДвиженияНакопления.Приход;
				Движение.Период         = Дата;
				Движение.Номенклатура   = ТекСтрокаТовары.Номенклатура;
				Движение.Характеристика = ТекСтрокаТовары.Характеристика;
				Движение.Склад          = ТекСтрокаТовары.Склад;
				Движение.ВНаличии       = ТекСтрокаТовары.Количество;
				Если ФОИспользоватьСерии Тогда
					Движение.Серия      = ТекСтрокаТовары.Серия;
				КонецЕсли; 			
			КонецЕсли;	
		КонецЦикла;
	КонецЕсли;
		
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	Таблица.Номенклатура КАК Номенклатура,
	               |	&Характеристика КАК Характеристика,
	               |	СУММА(Таблица.Количество) КАК Количество,
	               |	&Серия КАК Серия,
	               |	Таблица.Склад КАК Склад
	               |ПОМЕСТИТЬ ТабличнаяЧасть
	               |ИЗ
	               |	Документ.ПоступлениеТоваровУслуг.Товары КАК Таблица
	               |ГДЕ
	               |	Таблица.Ссылка = &Ссылка
	               |
	               |СГРУППИРОВАТЬ ПО
	               |	Таблица.Номенклатура,
	               |	Таблица.Склад,
				   |	&Характеристика,
				   |	&Серия
	               |
	               |ИНДЕКСИРОВАТЬ ПО
	               |	Склад,
	               |	Номенклатура,
	               |	Характеристика,
	               |	Серия";
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Если ФОИспользоватьСерии Тогда 
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Серия", "Таблица.Серия");
	Иначе 
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Серия", "ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)");
	КонецЕсли;

	Если ФОИспользоватьХарактеристики Тогда 
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Характеристика", "Таблица.Характеристика");
	Иначе 
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Характеристика", "ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)");
	КонецЕсли;

	РезЗапроса = Запрос.Выполнить();
	
	Если НЕ проф_ИзERP Тогда
		ДобавитьРезервы(Запрос); 
	КонецЕсли;
	ЗакрытьЗаказыПоставщику(Запрос); 
	
	Если НЕ проф_ИзERP Тогда
		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	ПоступлениеТоваровУслуг.Дата КАК Период,
		|	ПоступлениеТоваровУслуг.Склад КАК Склад,
		|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
		|	ПоступлениеТоваровУслугТовары.Номенклатура КАК Номенклатура,
		|	ПоступлениеТоваровУслугТовары.проф_Назначение КАК Назначение,
		|	ВЫБОР
		|		КОГДА &ФОИспользоватьХарактеристики
		|			ТОГДА ПоступлениеТоваровУслугТовары.Характеристика
		|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)
		|	КОНЕЦ КАК Характеристика,
		|	ВЫБОР
		|		КОГДА &ФОИспользоватьСерии
		|			ТОГДА ПоступлениеТоваровУслугТовары.Серия
		|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
		|	КОНЕЦ КАК Серия,
		|	СУММА(ПоступлениеТоваровУслугТовары.Количество) КАК ВНаличии
		|ИЗ
		|	Документ.ПоступлениеТоваровУслуг КАК ПоступлениеТоваровУслуг
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ПоступлениеТоваровУслуг.Товары КАК ПоступлениеТоваровУслугТовары
		|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СправочникНоменклатура
		|				ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ВидыНоменклатуры КАК ВидыНоменклатуры
		|				ПО СправочникНоменклатура.ВидНоменклатуры = ВидыНоменклатуры.Ссылка
		|					И (ВидыНоменклатуры.ТипНоменклатуры В (ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Товар), ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.МногооборотнаяТара)))
		|			ПО ПоступлениеТоваровУслугТовары.Номенклатура = СправочникНоменклатура.Ссылка
		|		ПО ПоступлениеТоваровУслуг.Ссылка = ПоступлениеТоваровУслугТовары.Ссылка
		|ГДЕ
		|	ПоступлениеТоваровУслуг.Ссылка = &Ссылка
		|
		|СГРУППИРОВАТЬ ПО
		|	ПоступлениеТоваровУслуг.Дата,
		|	ПоступлениеТоваровУслуг.Склад,
		|	ПоступлениеТоваровУслугТовары.Номенклатура,
		|	ВЫБОР
		|		КОГДА &ФОИспользоватьХарактеристики
		|			ТОГДА ПоступлениеТоваровУслугТовары.Характеристика
		|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)
		|	КОНЕЦ,
		|	ПоступлениеТоваровУслугТовары.проф_Назначение,
		|	ВЫБОР
		|		КОГДА &ФОИспользоватьСерии
		|			ТОГДА ПоступлениеТоваровУслугТовары.Серия
		|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
		|	КОНЕЦ";
		
		Запрос.УстановитьПараметр("Ссылка", Ссылка);
		Запрос.УстановитьПараметр("ФОИспользоватьСерии", ФОИспользоватьСерии);
		Запрос.УстановитьПараметр("ФОИспользоватьХарактеристики", ФОИспользоватьХарактеристики);
		
		РезультатЗапроса = Запрос.Выполнить();
		Если РезультатЗапроса.Пустой() Тогда 
			Возврат;
		КонецЕсли;
		
		Движения.проф_ТоварыНаСкладах.Записывать = Истина;
		
		Выборка = РезультатЗапроса.Выбрать();
		Пока Выборка.Следующий() Цикл
			Движение = Движения.проф_ТоварыНаСкладах.Добавить();
			ЗаполнитьЗначенияСвойств(Движение, Выборка);
		КонецЦикла;
		
		//-- Проф-ИТ, #27, Соловьев А.А., 25.08.2023
	КонецЕсли;
КонецПроцедуры 
//-- Проф-ИТ, #501, Ахмадеев И.Г., 06.03.2024

#КонецОбласти