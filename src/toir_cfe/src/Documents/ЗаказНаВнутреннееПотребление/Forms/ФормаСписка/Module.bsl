
#Область ОбработчикиСобытийФормы
	
&НаСервере
Процедура проф_ПриСозданииНаСервереПосле(Отказ, СтандартнаяОбработка)
	
	//++ Проф-ИТ, #73, Соловьев А.А., 01.09.2023
	Элементы.торо_СтатусУтверждения.Видимость = Истина;

	ЗаказыДляЗакупки = ЭтотОбъект.Параметры.Свойство("проф_ЗаказДляЗакупки")
		Или (ЭтотОбъект.Параметры.ТекущаяСтрока <> Неопределено
		И ЭтотОбъект.Параметры.ТекущаяСтрока.проф_ЗаказДляЗакупки);	
	
	ЭлементОтбора = Список.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ЭлементОтбора.ЛевоеЗначение 	= Новый ПолеКомпоновкиДанных("проф_ЗаказДляЗакупки");
	ЭлементОтбора.ВидСравнения 		= ВидСравненияКомпоновкиДанных.Равно;
	ЭлементОтбора.ПравоеЗначение 	= ЗаказыДляЗакупки;
	ЭлементОтбора.РежимОтображения 	= РежимОтображенияЭлементаНастройкиКомпоновкиДанных.Недоступный;
	ЭлементОтбора.Использование 	= Истина;
	
	Если ЗаказыДляЗакупки Тогда 
		ЭтотОбъект.АвтоЗаголовок = Ложь;
		ЭтотОбъект.Заголовок = НСтр("ru = 'Заказы на внутренние потребления (для закупки)'");
		Элементы.ФормаСоздатьНаОсновании.Доступность = Ложь; 
	КонецЕсли;
	//-- Проф-ИТ, #73, Соловьев А.А., 01.09.2023
	
	//++ Проф-ИТ, #338, Корнилов М.С., 04.11.2023
	Элементы.ФормаСоздатьНаОсновании1.Видимость = Ложь;
	Элементы.ПодменюСоздатьНаОсновании.Отображение = ОтображениеКнопки.Текст; 
	//-- Проф-ИТ, #338, Корнилов М.С., 04.11.2023
	
КонецПроцедуры

#КонецОбласти