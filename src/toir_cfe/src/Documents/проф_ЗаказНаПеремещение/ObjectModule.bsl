#Область ОбработчикиСобытий

// ++ Проф-ИТ, #72, Соловьев А.А., 19.09.2023

&После("ПередЗаписью")
Процедура проф_ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьКлючВСтрокахТабличнойЧасти(ЭтотОбъект, "Товары");
	
	ДополнительныеСвойства.Вставить("ЭтоНовый",    ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);
	
	Если ЭтоНовый()
	И НЕ ЗначениеЗаполнено(Номер) Тогда
		УстановитьНовыйНомер();
	КонецЕсли;
	
	Для Каждого СтрокаТовары Из Товары Цикл
		Если СтрокаТовары.ДатаОтгрузки < Дата Тогда 
			СтрокаТовары.ДатаОтгрузки = Дата;
		КонецЕсли;
	КонецЦикла;
	
    ПроверкаПройдена = ПроверитьДокументПередЗапускомПроцессаПоСнятиюРезерва();
	Если ПроверкаПройдена Тогда
		ОтправитьНаСогласование();
	КонецЕсли; 
	
	//++ Проф-ИТ, #401, Соловьев А.А., 11.12.2023
	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда 
		проф_ОбщегоНазначенияВызовСервера.ПроверитьПризнакПодразделенияОрганизации(ЭтотОбъект["Подразделение"], Отказ);
	КонецЕсли;
	//-- Проф-ИТ, #401, Соловьев А.А., 11.12.2023
	
	//++ Проф-ИТ, #457, Соловьев А.А., 01.02.2024
	Если СкладОтправитель = СкладПолучатель Тогда 
		ТекстСообщения = НСтр("ru = 'Один склад не может быть как отправителем, так и получателем. Измените один из складов'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, "СкладПолучатель", , Отказ);
	КонецЕсли;
	//-- Проф-ИТ, #457, Соловьев А.А., 01.02.2024
	
КонецПроцедуры

&После("ОбработкаПроверкиЗаполнения")
Процедура проф_ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)

	МассивНепроверяемыхРеквизитов = Новый Массив;
	МассивНепроверяемыхРеквизитов.Добавить("ДокументОснование");
	Если ЗначениеЗаполнено(ДокументОснование) Тогда 
		МассивНепроверяемыхРеквизитов.Добавить("Подразделение");
	КонецЕсли;
	
	ОбщегоНазначенияУТ.ПроверитьЗаполнениеКоличества(ЭтотОбъект, ПроверяемыеРеквизиты, Отказ);
				
	ФОИспользоватьХарактеристикиНоменклатуры = ПолучитьФункциональнуюОпцию("торо_ИспользоватьХарактеристикиНоменклатуры");
	МассивНепроверяемыхРеквизитов.Добавить("Товары.Характеристика");
	Если ФОИспользоватьХарактеристикиНоменклатуры = Истина Тогда
		НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект,МассивНепроверяемыхРеквизитов,Отказ);
	КонецЕсли;
	
	МассивНепроверяемыхРеквизитов.Добавить("Товары.Серия");	
	МассивНепроверяемыхРеквизитов.Добавить("Товары.Доступно");
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
	//++ Проф-ИТ, #349, Соловьев А.А., 17.11.2023
	Если Не ЗначениеЗаполнено(ЖелаемаяДатаПоступления) Тогда 
		ЖелаемаяДатаПоступления = Дата;
	ИначеЕсли ЖелаемаяДатаПоступления < НачалоДня(Дата) Тогда 
		ТекстСообщения = НСтр("ru = 'Желаемая дата поступления должна быть не меньше или равна дате документа %1'");
		ТекстСообщения = СтрШаблон(ТекстСообщения, Формат(Дата, "ДЛФ=Д"));
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Объект.ЖелаемаяДатаПоступления", , Отказ);
	КонецЕсли;
	//-- Проф-ИТ, #349, Соловьев А.А., 17.11.2023
	
	СтрОбъектТовары = "Объект.Товары[";
	СтрНазначение = "].НазначениеИсходное";
	
	ПодразделенияСлужбыТОИР = Справочники.проф_НастройкиСистемы.ПолучитьСпЗначНастройкиСистемы(
		"Подразделения", "ПодразделенияСлужбыТОИР");
	
	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапросаПередЗаписью();
	Запрос.УстановитьПараметр("ПодразделенияСлужбыТОИР", ПодразделенияСлужбыТОИР);
	Запрос.УстановитьПараметр("Товары", Товары.Выгрузить());
	Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
	Запрос.УстановитьПараметр("ДатаДокумента", Дата);
	Запрос.УстановитьПараметр("Склад", СкладОтправитель);
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	
	ВыборкаОстатки = РезультатЗапроса[РезультатЗапроса.ВГраница()].Выбрать();
	
	ТекстСообщенияШаблон = НСтр("ru = 'В строке %1 на складе-отправителе не хватает номенклатуры в количестве %2'");
	Если ВыборкаОстатки.Следующий() Тогда 
		ТекстСообщения = СтрШаблон(ТекстСообщенияШаблон, ВыборкаОстатки.НомерСтроки, ВыборкаОстатки.КоличествоРазница);
		ПолеСообщения = СтрОбъектТовары + (ВыборкаОстатки.НомерСтроки - 1) + "].КоличествоУпаковок"; 
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , ПолеСообщения, , Отказ);
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ДокументОснование) Тогда 
		
		ВыборкаТЧ = РезультатЗапроса[РезультатЗапроса.ВГраница()-1].Выбрать();
		
		Пока ВыборкаТЧ.Следующий() Цикл
			ТекстСообщения = СтрШаблон(НСтр("ru = 'Строки %1 нет в документе-основании'"), ВыборкаТЧ.НомерСтроки);
			ПолеСообщения = СтрОбъектТовары + (ВыборкаТЧ.НомерСтроки - 1) + СтрНазначение; 
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , ПолеСообщения, , Отказ);
		КонецЦикла;
		
		РеквизитыОснования = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ДокументОснование,
											"проф_НаправлениеДеятельности, проф_Назначение");
		
		Если НаправлениеДеятельности <> РеквизитыОснования.проф_НаправлениеДеятельности Тогда 
			ТекстСообщения = НСтр("ru = 'Направление деятельности не равно направлению деательности в документе-основании'");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Объект.НаправлениеДеятельности", , Отказ);
		КонецЕсли;	
		
		Если Назначение <> РеквизитыОснования.проф_Назначение Тогда 
			ТекстСообщения = НСтр("ru = 'Назначение не равно назначению в документе-основании'");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Объект.Назначение", , Отказ);
		КонецЕсли;
		
		ПроверитьСоответствиеНазначенияНазначениюВДокументеОсновании(РеквизитыОснования,
																	 СтрОбъектТовары,
																	 СтрНазначение,
																	 Отказ);
		
	КонецЕсли;
	
	//++ Проф-ИТ, #424, Соловьев А.А., 16.01.2024
	Для Каждого СтрокаТЧ Из Товары Цикл
		Если СтрокаТЧ.Отменено И Не ЗначениеЗаполнено(СтрокаТЧ.ПричинаОтмены) Тогда 
			ТекстСообщения = СтрШаблон(НСтр("ru = 'Не заполнена причина отмены в строке %1'"), СтрокаТЧ.НомерСтроки);
			Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("Товары", СтрокаТЧ.НомерСтроки, "ПричинаОтмены");
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Поле, , Отказ);
		КонецЕсли;
	КонецЦикла;
	//-- Проф-ИТ, #424, Соловьев А.А., 16.01.2024
	
КонецПроцедуры

// -- Проф-ИТ, #72, Соловьев А.А., 19.09.2023

&После("ОбработкаЗаполнения")
Процедура проф_ОбработкаЗаполнения(ДанныеЗаполнения, ТекстЗаполнения, СтандартнаяОбработка)

	//++ Проф-ИТ, #27, Соловьев А.А., 26.09.2023
	Если ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ЗаказНаВнутреннееПотребление") Тогда 
		ЗаполнитьПоЗаказуНаВнутреннееПотребление(ДанныеЗаполнения);
	КонецЕсли;	
	//-- Проф-ИТ, #27, Соловьев А.А., 26.09.2023
	
	ЗаполнитьРеквизитыПоУмолчанию();	

КонецПроцедуры

// ++ Проф-ИТ, #72, Соловьев А.А., 19.09.2023

&После("ОбработкаПроведения")
Процедура проф_ОбработкаПроведения(Отказ, РежимПроведения)

	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);
	
	Документы.проф_ЗаказНаПеремещение.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	
	Отразитьпроф_ЗаказыНаПеремещение(ДополнительныеСвойства, Движения, Отказ);
	
	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);	
	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);	
	
КонецПроцедуры

&После("ПриКопировании")
Процедура проф_ПриКопировании(ОбъектКопирования)
	
	ЗаполнитьРеквизитыПоУмолчанию();
	Для Каждого СтрокаТЧ Из Товары Цикл
		СтрокаТЧ.Доступно = 0;
		СтрокаТЧ.Назначение = Справочники.проф_Назначения.ПустаяСсылка();
		СтрокаТЧ.НазначениеИсходное = Справочники.проф_Назначения.ПустаяСсылка();
		СтрокаТЧ.СогласованиеКорректировки = Перечисления.проф_СогласованиеКорректировки.ПустаяСсылка();
		//++ Проф-ИТ, #415, Корнилов М.С., 11.01.2024
		СтрокаТЧ.ТекстОшибки = "";
		//-- Проф-ИТ, #415, Корнилов М.С., 11.01.2024
	КонецЦикла;	
		
КонецПроцедуры

// -- Проф-ИТ, #72, Соловьев А.А., 19.09.2023

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// ++ Проф-ИТ, #72, Соловьев А.А., 19.09.2023

Функция ТекстЗапросаПередЗаписью()
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	&ДокументОснование КАК ДокументОснование,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Характеристика КАК Характеристика,
	|	Товары.Серия КАК Серия,
	|	Товары.Упаковка КАК Упаковка,
	|	Товары.Количество КАК Количество,
	|	Товары.Доступно КАК Доступно,
	|	Товары.Отменено КАК Отменено,
	|	Товары.НазначениеИсходное КАК НазначениеИсходное,
	|	Товары.НомерСтроки КАК НомерСтроки
	|ПОМЕСТИТЬ втТовары
	|ИЗ
	|	&Товары КАК Товары
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втТовары.ДокументОснование КАК ДокументОснование,
	|	втТовары.Номенклатура КАК Номенклатура,
	|	втТовары.Характеристика КАК Характеристика,
	|	втТовары.Серия КАК Серия,
	|	втТовары.Упаковка КАК Упаковка,
	|	СУММА(втТовары.Количество) КАК Количество,
	|	СУММА(втТовары.Доступно) КАК Доступно,
	|	втТовары.Отменено КАК Отменено,
	|	втТовары.НазначениеИсходное КАК НазначениеИсходное,
	|	МИНИМУМ(втТовары.НомерСтроки) КАК НомерСтроки
	|ПОМЕСТИТЬ втДанныеДокумента
	|ИЗ
	|	втТовары КАК втТовары
	|ГДЕ
	|	втТовары.Отменено = ЛОЖЬ
	|	И втТовары.Доступно > 0
	|
	|СГРУППИРОВАТЬ ПО
	|	втТовары.Номенклатура,
	|	втТовары.Характеристика,
	|	втТовары.НазначениеИсходное,
	|	втТовары.ДокументОснование,
	|	втТовары.Отменено,
	|	втТовары.Упаковка,
	|	втТовары.Серия
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	втТовары.ДокументОснование,
	|	втТовары.Номенклатура,
	|	втТовары.Характеристика,
	|	втТовары.Серия,
	|	втТовары.Упаковка,
	|	СУММА(втТовары.Количество),
	|	СУММА(втТовары.Доступно),
	|	втТовары.Отменено,
	|	втТовары.НазначениеИсходное,
	|	МИНИМУМ(втТовары.НомерСтроки)
	|ИЗ
	|	втТовары КАК втТовары
	|ГДЕ
	|	втТовары.Отменено = ЛОЖЬ
	|	И втТовары.Доступно = 0
	|
	|СГРУППИРОВАТЬ ПО
	|	втТовары.Номенклатура,
	|	втТовары.Характеристика,
	|	втТовары.НазначениеИсходное,
	|	втТовары.ДокументОснование,
	|	втТовары.Отменено,
	|	втТовары.Упаковка,
	|	втТовары.Серия
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	проф_ЗапасыИПотребностиОстатки.Номенклатура КАК Номенклатура,
	|	проф_ЗапасыИПотребностиОстатки.Характеристика КАК Характеристика,
	|	проф_ЗапасыИПотребностиОстатки.Назначение КАК Назначение,
	|	СУММА(проф_ЗапасыИПотребностиОстатки.ВНаличииОстаток - проф_ЗапасыИПотребностиОстатки.РезервироватьНаСкладеОстаток - проф_ЗапасыИПотребностиОстатки.РезервироватьПоМереПоступленияОстаток) КАК ВНаличии
	|ПОМЕСТИТЬ втЗапасыИПотребности
	|ИЗ
	|	РегистрНакопления.проф_ЗапасыИПотребности.Остатки(
	|			&ДатаДокумента,
	|			(Номенклатура, Характеристика, Склад) В
	|				(ВЫБРАТЬ
	|					втДанныеДокумента.Номенклатура КАК Номенклатура,
	|					втДанныеДокумента.Характеристика КАК Характеристика,
	|					&Склад КАК Склад
	|				ИЗ
	|					втДанныеДокумента КАК втДанныеДокумента)) КАК проф_ЗапасыИПотребностиОстатки
	|
	|СГРУППИРОВАТЬ ПО
	|	проф_ЗапасыИПотребностиОстатки.Номенклатура,
	|	проф_ЗапасыИПотребностиОстатки.Характеристика,
	|	проф_ЗапасыИПотребностиОстатки.Назначение
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втДанныеДокумента.ДокументОснование КАК ДокументОснование,
	|	втДанныеДокумента.Номенклатура КАК Номенклатура,
	|	втДанныеДокумента.Характеристика КАК Характеристика,
	|	втДанныеДокумента.Серия КАК Серия,
	|	втДанныеДокумента.Упаковка КАК Упаковка,
	|	втДанныеДокумента.Отменено КАК Отменено,
	|	СУММА(втДанныеДокумента.Количество) КАК Количество
	|ПОМЕСТИТЬ втТоварыОснования
	|ИЗ
	|	втДанныеДокумента КАК втДанныеДокумента
	|
	|СГРУППИРОВАТЬ ПО
	|	втДанныеДокумента.ДокументОснование,
	|	втДанныеДокумента.Характеристика,
	|	втДанныеДокумента.Серия,
	|	втДанныеДокумента.Упаковка,
	|	втДанныеДокумента.Отменено,
	|	втДанныеДокумента.Номенклатура
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втЗапасыИПотребности.Номенклатура КАК Номенклатура,
	|	втЗапасыИПотребности.Характеристика КАК Характеристика,
	|	СУММА(втЗапасыИПотребности.ВНаличии) КАК ВНаличии
	|ПОМЕСТИТЬ втРезервНеТОиР
	|ИЗ
	|	втЗапасыИПотребности КАК втЗапасыИПотребности
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.проф_Назначения КАК проф_Назначения
	|		ПО втЗапасыИПотребности.Назначение = проф_Назначения.Ссылка
	|			И (НЕ проф_Назначения.Подразделение В (&ПодразделенияСлужбыТОИР))
	|ГДЕ
	|	втЗапасыИПотребности.ВНаличии > 0
	|
	|СГРУППИРОВАТЬ ПО
	|	втЗапасыИПотребности.Номенклатура,
	|	втЗапасыИПотребности.Характеристика
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втДанныеДокумента.НомерСтроки КАК НомерСтроки
	|ИЗ
	|	втТоварыОснования КАК втТоварыОснования
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ЗаказНаВнутреннееПотребление.Товары КАК ЗаказНаВнутреннееПотреблениеТовары
	|		ПО втТоварыОснования.ДокументОснование = ЗаказНаВнутреннееПотреблениеТовары.Ссылка
	|			И втТоварыОснования.Номенклатура = ЗаказНаВнутреннееПотреблениеТовары.Номенклатура
	|			И втТоварыОснования.Характеристика = ЗаказНаВнутреннееПотреблениеТовары.Характеристика
	|			И втТоварыОснования.Серия = ЗаказНаВнутреннееПотреблениеТовары.Серия
	|			И втТоварыОснования.Упаковка = ЗаказНаВнутреннееПотреблениеТовары.Упаковка
	|			И втТоварыОснования.Отменено = ЗаказНаВнутреннееПотреблениеТовары.Отменено
	|			И втТоварыОснования.Количество <= ЗаказНаВнутреннееПотреблениеТовары.Количество
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ втДанныеДокумента КАК втДанныеДокумента
	|		ПО втТоварыОснования.ДокументОснование = втДанныеДокумента.ДокументОснование
	|			И втТоварыОснования.Номенклатура = втДанныеДокумента.Номенклатура
	|			И втТоварыОснования.Характеристика = втДанныеДокумента.Характеристика
	|			И втТоварыОснования.Серия = втДанныеДокумента.Серия
	|			И втТоварыОснования.Упаковка = втДанныеДокумента.Упаковка
	|			И втТоварыОснования.Отменено = втДанныеДокумента.Отменено
	|			И втТоварыОснования.Количество = втДанныеДокумента.Количество
	|ГДЕ
	|	ЗаказНаВнутреннееПотреблениеТовары.Ссылка ЕСТЬ NULL
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втДанныеДокумента.НомерСтроки КАК НомерСтроки,
	|	втДанныеДокумента.Количество - ЕСТЬNULL(втЗапасыИПотребности.ВНаличии, 0) КАК КоличествоРазница
	|ИЗ
	|	втДанныеДокумента КАК втДанныеДокумента
	|		ЛЕВОЕ СОЕДИНЕНИЕ втЗапасыИПотребности КАК втЗапасыИПотребности
	|		ПО втДанныеДокумента.Номенклатура = втЗапасыИПотребности.Номенклатура
	|			И втДанныеДокумента.Характеристика = втЗапасыИПотребности.Характеристика
	|			И втДанныеДокумента.НазначениеИсходное = втЗапасыИПотребности.Назначение
	|ГДЕ
	|	втДанныеДокумента.Количество > ЕСТЬNULL(втЗапасыИПотребности.ВНаличии, 0)
	|	И втДанныеДокумента.Отменено = ЛОЖЬ
	|	И втДанныеДокумента.Доступно > 0
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	втДанныеДокумента.НомерСтроки,
	|	втДанныеДокумента.Количество - ЕСТЬNULL(втРезервНеТОиР.ВНаличии, 0)
	|ИЗ
	|	втДанныеДокумента КАК втДанныеДокумента
	|		ЛЕВОЕ СОЕДИНЕНИЕ втРезервНеТОиР КАК втРезервНеТОиР
	|		ПО втДанныеДокумента.Номенклатура = втРезервНеТОиР.Номенклатура
	|			И втДанныеДокумента.Характеристика = втРезервНеТОиР.Характеристика
	|ГДЕ
	|	втДанныеДокумента.Доступно = 0
	|	И втДанныеДокумента.Количество > ЕСТЬNULL(втРезервНеТОиР.ВНаличии, 0)";
	
	Возврат ТекстЗапроса;
	
КонецФункции	

Процедура ПроверитьСоответствиеНазначенияНазначениюВДокументеОсновании(РеквизитыОснования,
																	   СтрОбъектТовары,
																	   СтрНазначение,
																	   Отказ)
	
	Для Каждого СтрокаТЧ Из Товары Цикл
		Если СтрокаТЧ.Назначение <> РеквизитыОснования.проф_Назначение Тогда 
			ТекстСообщения = СтрШаблон(
				НСтр("ru = 'В строке %1 назначение не равно назначению в документе-основании'"), СтрокаТЧ.НомерСтроки);
			ПолеСообщения = СтрОбъектТовары + (СтрокаТЧ.НомерСтроки - 1) + СтрНазначение;
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , ПолеСообщения, , Отказ);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры	

Процедура ЗаполнитьРеквизитыПоУмолчанию()
	
	Дата = ТекущаяДатаСеанса();
	Ответственный = ПараметрыСеанса.ТекущийПользователь;
	Если ЗначениеЗаполнено(Ответственный) И Не ЗначениеЗаполнено(Подразделение) Тогда 
		Подразделение = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Ответственный, "Подразделение");
	КонецЕсли;
	Автор = Ответственный;
	СтатусСогласованияЗаказа = Перечисления.торо_СтатусыУтвержденияЗаказовНаВП.проф_Новый;
	//++ Проф-ИТ, #349, Соловьев А.А., 15.11.2023
	СпособДоставки = Перечисления.проф_СпособДоставки.Самовывоз;
	//-- Проф-ИТ, #349, Соловьев А.А., 15.11.2023
	
КонецПроцедуры

Процедура Отразитьпроф_ЗаказыНаПеремещение(ДополнительныеСвойства, Движения, Отказ)
	
	Таблицапроф_ЗаказыНаПеремещение = ДополнительныеСвойства.ТаблицыДляДвижений.проф_ЗаказыНаПеремещение;
	
	Если Отказ ИЛИ Таблицапроф_ЗаказыНаПеремещение.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Движенияпроф_ЗаказыНаПеремещение = Движения.проф_ЗаказыНаПеремещение;
	Движенияпроф_ЗаказыНаПеремещение.Записывать = Истина;
	Движенияпроф_ЗаказыНаПеремещение.Загрузить(Таблицапроф_ЗаказыНаПеремещение);		
	
КонецПроцедуры

// -- Проф-ИТ, #72, Соловьев А.А., 19.09.2023

Процедура ЗаполнитьПоЗаказуНаВнутреннееПотребление(ДанныеЗаполнения)
	
	//++ Проф-ИТ, #27, Соловьев А.А., 26.09.2023
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ЗаказНаВнутреннееПотребление.Ссылка КАК ДокументОснование,
	|	ЗаказНаВнутреннееПотребление.Организация КАК Организация,
	|	ЗаказНаВнутреннееПотребление.проф_НаправлениеДеятельности КАК НаправлениеДеятельности,
	|	ЗаказНаВнутреннееПотребление.Подразделение КАК Подразделение,
	|	ЗаказНаВнутреннееПотребление.проф_Назначение КАК Назначение
	|ИЗ
	|	Документ.ЗаказНаВнутреннееПотребление КАК ЗаказНаВнутреннееПотребление
	|ГДЕ
	|	ЗаказНаВнутреннееПотребление.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ЗаказНаВнутреннееПотреблениеТовары.Номенклатура КАК Номенклатура,
	|	ЗаказНаВнутреннееПотреблениеТовары.Характеристика КАК Характеристика,
	|	ЗаказНаВнутреннееПотреблениеТовары.КоличествоУпаковок КАК КоличествоУпаковок,
	|	ЗаказНаВнутреннееПотреблениеТовары.Количество КАК Количество,
	|	ЗаказНаВнутреннееПотреблениеТовары.Упаковка КАК Упаковка,
	|	ЗаказНаВнутреннееПотребление.проф_Назначение КАК Назначение
	|ИЗ
	|	Документ.ЗаказНаВнутреннееПотребление КАК ЗаказНаВнутреннееПотребление
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ЗаказНаВнутреннееПотребление.Товары КАК ЗаказНаВнутреннееПотреблениеТовары
	|		ПО ЗаказНаВнутреннееПотреблениеТовары.Ссылка = ЗаказНаВнутреннееПотребление.Ссылка
	|ГДЕ
	|	ЗаказНаВнутреннееПотребление.Ссылка = &Ссылка";
	
	Запрос.УстановитьПараметр("Ссылка", ДанныеЗаполнения);
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	ВыборкаШапка = РезультатЗапроса[0].Выбрать();
	ВыборкаТЧ = РезультатЗапроса[1].Выбрать();
	ВыборкаШапка.Следующий();
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, ВыборкаШапка);
	
	Пока ВыборкаТЧ.Следующий() Цикл
		НоваяСтрокаТовары = Товары.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрокаТовары, ВыборкаТЧ);
	КонецЦикла;
	//-- Проф-ИТ, #27, Соловьев А.А., 26.09.2023
	
КонецПроцедуры	

// ++ Проф-ИТ, #72, Соловьев А.А., 19.09.2023

Процедура ОтправитьНаСогласование()
	
	ДокументДляСогласования = Ссылка;
	//Если Не ЗначениеЗаполнено(ДокументОснование)
	//	Или Не ТипЗнч(ДокументОснование) = Тип("ДокументСсылка.ЗаказНаВнутреннееПотребление") Тогда	
	//	Возврат;
	//КонецЕсли;
		
	ТаблицаТовары = ТаблицаТоварыПередСогласованием();
	
	Если ТаблицаТовары.Количество() = 0 Тогда
	    Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапросаОтправитьНаСогласование();	
	Запрос.УстановитьПараметр("ТаблицаТовары", ТаблицаТовары);
	РезультатЗапроса = Запрос.Выполнить();
	
	НаименованиеШаблона = "Согласование изменения заказа";
	
	ВыборкаНом = РезультатЗапроса.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	Пока ВыборкаНом.Следующий() Цикл
		
		ВыборкаПодразделение = ВыборкаНом.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
		
		Пока ВыборкаПодразделение.Следующий() Цикл  
			
			ВыборкаИДЗаказа = ВыборкаПодразделение.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
			Пока ВыборкаИДЗаказа.Следующий() Цикл
				
				ТекстОписанияИтог = "";
				
				ЗаполнитьТекстОписания(ВыборкаИДЗаказа, ТекстОписанияИтог);
				
				ПредметID = "";
				Если ЗначениеЗаполнено(ВыборкаИДЗаказа.ИДЗаказа) Тогда			
					ПредметID = ВыборкаИДЗаказа.ИДЗаказа;			
				КонецЕсли;
				
				ПараметрыФормы = Новый Структура;
				
				ПредметБизнесПроцесса = Новый Структура;
				ПредметБизнесПроцесса.Вставить("ID", ПредметID); //ПредметID
				ПредметБизнесПроцесса.Вставить("type", "DMInternalDocument");     
				
				name = ПолучитьОписаниеПредметаСогласования(ДокументДляСогласования);
				
				ПредметБизнесПроцесса.Вставить("name", name); 
				
				Если ЗначениеЗаполнено(ПредметID) Тогда
					ПараметрыФормы.Вставить("Предмет", ПредметБизнесПроцесса);
				КонецЕсли;  
				
				ПараметрыФормы.Вставить("Описание", ТекстОписанияИтог);
				
				ШаблонБизнесПроцесса = Новый Структура;
				ШаблонБизнесПроцесса.Вставить("ID", "32fccb2e-ac94-11ed-bca8-3cecef0dcbf1");
				ШаблонБизнесПроцесса.Вставить("type", "DMBusinessProcessApprovalTemplate");
				ПараметрыФормы.Вставить("Шаблон", ШаблонБизнесПроцесса);
				
				ТипПроцесса = "DMBusinessProcessApproval"; 	
				ПроцессID = ""; 
				
				Выборка = ВыборкаИДЗаказа.Выбрать();
				Выборка.Следующий();
				ПараметрыОтбора = Новый Структура("Номенклатура, НазначениеИсходное", Выборка.Номенклатура, Выборка.Назначение); 
				НайденныеСтроки = Товары.НайтиСтроки(ПараметрыОтбора);

				Попытка
					Если ЗаполнитьСтартоватьПроцесс(ТипПроцесса, ПараметрыФормы, ПроцессID) Тогда
						
						ПараметрыОповещения = Новый Структура;
						ПараметрыОповещения.Вставить("ID", ПроцессID);
						ПараметрыОповещения.Вставить("Стартован", Истина);
						
						// Соберем предметы.
						Предметы = Новый Массив;
						Предметы.Добавить(ПредметБизнесПроцесса);
						
						ПараметрыОповещения.Вставить("Предметы", Предметы);
						
					КонецЕсли;
				Исключение 
					ПричинаОшибки = ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()); 
					ТекстОшибки = Нстр("ru = 'Задание по снятию резерва по %1, %2, %3, %4 в ДО не сформировано! Причина: %5%6'");
					ТекстОшибки = СтрШаблон(ТекстОшибки, ВыборкаИДЗаказа.НоменклатураПредставление, ВыборкаИДЗаказа.Количество, 
						ВыборкаИДЗаказа.ЕдиницаИзмеренияПредставление, ВыборкаИДЗаказа.НазначениеПредставление, Символы.ПС, ПричинаОшибки);
					ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстОшибки); 
					Для каждого текСтрока Из НайденныеСтроки Цикл
						текСтрока.СогласованиеКорректировки = Перечисления.проф_СогласованиеКорректировки.Ошибка;
						//++ Проф-ИТ, #415, Корнилов М.С., 11.01.2024
						текСтрока.ТекстОшибки = ТекстОшибки;
						//-- Проф-ИТ, #415, Корнилов М.С., 11.01.2024
					КонецЦикла
				КонецПопытки;
			КонецЦикла;
			
			
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры

Функция ТаблицаТоварыПередСогласованием()
	
	ТаблицаТовары = Товары.Выгрузить(, "НомерСтроки, Номенклатура, НазначениеИсходное, Количество");
	ТаблицаТовары.Колонки.Добавить("Склад", Новый ОписаниеТипов("СправочникССылка.Склады"));
	ТаблицаТовары.Очистить();
	Для Каждого ТекСтрока Из Товары Цикл  
		Если ТекСтрока.СогласованиеКорректировки = Перечисления.проф_СогласованиеКорректировки.ПодготовленКСогласованию 
			ИЛИ ТекСтрока.СогласованиеКорректировки = Перечисления.проф_СогласованиеКорректировки.Отклонен Тогда			
			новСтрока = ТаблицаТовары.Добавить();
			ЗаполнитьЗначенияСвойств(новСтрока, ТекСтрока); 
			новСтрока.Склад = СкладОтправитель; 
		КонецЕсли;
	КонецЦикла; 	
	
	Возврат ТаблицаТовары;
	
КонецФункции

Функция ТекстЗапросаОтправитьНаСогласование()
	
	ТекстЗапроса = "ВЫБРАТЬ
	               |	ТаблицаТовары.Номенклатура КАК Номенклатура,
	               |	ТаблицаТовары.Склад КАК Склад,
	               |	ТаблицаТовары.НазначениеИсходное КАК Назначение,
	               |	ТаблицаТовары.Количество КАК Количество,
	               |	ТаблицаТовары.НомерСтроки КАК НомерСтроки
	               |ПОМЕСТИТЬ ВТ_Товары
	               |ИЗ
	               |	&ТаблицаТовары КАК ТаблицаТовары
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	проф_НастройкиСистемыСписок.Значение КАК Значение
	               |ПОМЕСТИТЬ ВТ_ПодразделениеИсключить
	               |ИЗ
	               |	Справочник.проф_НастройкиСистемы.Список КАК проф_НастройкиСистемыСписок
	               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.проф_НастройкиСистемы КАК проф_НастройкиСистемы
	               |		ПО проф_НастройкиСистемыСписок.Ссылка = проф_НастройкиСистемы.Ссылка
	               |ГДЕ
	               |	проф_НастройкиСистемы.Наименование = ""ПодразделенияСлужбыТОиР""
	               |
	               |ОБЪЕДИНИТЬ ВСЕ
	               |
	               |ВЫБРАТЬ
	               |	проф_НастройкиСистемы.Значение
	               |ИЗ
	               |	Справочник.проф_НастройкиСистемы КАК проф_НастройкиСистемы
	               |ГДЕ
	               |	проф_НастройкиСистемы.Наименование = ""ПодразделенияСлужбыТОиР""
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	проф_Назначения.Подразделение КАК Подразделение,
	               |	ВТ_Товары.Количество КАК Количество,
	               |	ВТ_Товары.Номенклатура КАК Номенклатура,
	               |	ПРЕДСТАВЛЕНИЕССЫЛКИ(ВТ_Товары.Номенклатура) КАК НоменклатураПредставление,
	               |	ПРЕДСТАВЛЕНИЕССЫЛКИ(проф_Назначения.Подразделение) КАК ПодразделениеПредставление,
	               |	ПРЕДСТАВЛЕНИЕССЫЛКИ(ВТ_Товары.Назначение) КАК НазначениеПредставление,
	               |	ПРЕДСТАВЛЕНИЕССЫЛКИ(СпрНоменклатура.ЕдиницаИзмерения) КАК ЕдиницаИзмеренияПредставление,
	               |	ВТ_Товары.НомерСтроки КАК НомерСтроки,
	               |	проф_Назначения.ИДЗаказа КАК ИДЗаказа,
	               |	ВТ_Товары.Назначение КАК Назначение
	               |ИЗ
	               |	ВТ_Товары КАК ВТ_Товары
	               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.проф_Назначения КАК проф_Назначения
	               |			ЛЕВОЕ СОЕДИНЕНИЕ ВТ_ПодразделениеИсключить КАК ВТ_ПодразделениеИсключить
	               |			ПО проф_Назначения.Подразделение = ВТ_ПодразделениеИсключить.Значение
	               |		ПО ВТ_Товары.Назначение = проф_Назначения.Ссылка
	               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СпрНоменклатура
	               |		ПО ВТ_Товары.Номенклатура = СпрНоменклатура.Ссылка
	               |ГДЕ
	               |	ВТ_ПодразделениеИсключить.Значение ЕСТЬ NULL
	               |ИТОГИ ПО
	               |	Подразделение,
	               |	Номенклатура,
	               |	ИДЗаказа";
	
	Возврат ТекстЗапроса;
	
КонецФункции	

Процедура ЗаполнитьТекстОписания(ВыборкаПодразделение, ТекстОписанияИтог)
	
	Выборка = ВыборкаПодразделение.Выбрать();
	Пока Выборка.Следующий() Цикл
		Если Выборка.Количество = 0 Тогда
			Продолжить;
		КонецЕсли;   
		
		ТекстОписания = Нстр("ru = 'Согласовать снятие резерва: %1 - %2 - %3 %4 для назначения: %5'");
		ТекстОписания = СтрШаблон(ТекстОписания, Символы.ПС, Выборка.НоменклатураПредставление, Выборка.Количество,
							Выборка.ЕдиницаИзмеренияПредставление, Выборка.НазначениеПредставление);
		
		ТекстОписанияИтог = ?(ТекстОписанияИтог = "", ТекстОписания, ТекстОписанияИтог + Символы.ПС + ТекстОписания);
		
		Товары[Выборка.НомерСтроки - 1].СогласованиеКорректировки = ПредопределенноеЗначение(
									"Перечисление.проф_СогласованиеКорректировки.НаСогласовании");
	КонецЦикла;	
	
КонецПроцедуры	

Функция ЗаполнитьСтартоватьПроцесс(ТипПроцесса, ПараметрыЗапуска, ПроцессID)
	
	УстановитьПривилегированныйРежим(Истина);
	//Прокси = ИнтеграцияС1СДокументооборотБазоваяФункциональностьПовтИсп.ПолучитьПрокси();
	ВызыватьИсключение = Истина; 
	ИмяПользователя    = Константы.ИнтеграцияС1СДокументооборотИмяПользователяДляОбмена.Получить(); 
	Пароль             = Константы.ИнтеграцияС1СДокументооборотПарольДляОбмена.Получить();
	
	Прокси = ИнтеграцияС1СДокументооборотБазоваяФункциональность.ПолучитьПрокси(, ИмяПользователя, Пароль);
	
	Если ПараметрыЗапуска.Свойство("Предмет") Тогда
		ШаблонПроцесса = ИнтеграцияС1СДокументооборот.НовыйБизнесПроцессПоШаблону(
			Прокси, ТипПроцесса, ПараметрыЗапуска.Шаблон, ПараметрыЗапуска.Предмет);
	Иначе
		ШаблонПроцесса = ИнтеграцияС1СДокументооборот.НовыйБизнесПроцессПоШаблону(
			Прокси, ТипПроцесса, ПараметрыЗапуска.Шаблон);
	КонецЕсли;
	ШаблонПроцесса.description = ПараметрыЗапуска.Описание;   
	
	НовыйПроцесс = ИнтеграцияС1СДокументооборотБазоваяФункциональность.СоздатьОбъект(Прокси,
																					 ШаблонПроцесса.ObjectID.type);
	ИнтеграцияС1СДокументооборотБазоваяФункциональность.ЗаполнитьЗначенияСвойствXDTO(Прокси,
																					 НовыйПроцесс,
																					 ШаблонПроцесса);
	
	РезультатЗапуска = ИнтеграцияС1СДокументооборот.ЗапуститьБизнесПроцесс(Прокси, НовыйПроцесс);
		
	Если ИнтеграцияС1СДокументооборотБазоваяФункциональность.ПроверитьТип(Прокси, РезультатЗапуска, "DMError") Тогда
		Возврат Ложь;
	Иначе
		ПроцессID = РезультатЗапуска.businessProcess.ObjectID.ID;
		Возврат Истина;
	КонецЕсли; 
	
	УстановитьПривилегированныйРежим(Ложь);
	
КонецФункции 

Функция ПолучитьОписаниеПредметаСогласования(ДокументДляСогласования)
	
	Если ЗначениеЗаполнено(ДокументДляСогласования) Тогда		
		Возврат СтрШаблон("Заказ на ВПТ %1 от %2/%3",
						  ДокументДляСогласования.Номер,
						  ДокументДляСогласования.Дата,
						  ДокументДляСогласования.Подразделение);				
	Иначе		
		Возврат "";		
	КонецЕсли; 
		
КонецФункции

Процедура УстановитьКлючВСтрокахТабличнойЧасти(Объект, ИмяТабличнойЧасти,
											   РеквизитМаксимальныйКодСтроки = "МаксимальныйКодСтроки")

	СтрокиБезКлюча = Объект[ИмяТабличнойЧасти].НайтиСтроки(Новый Структура("КодСтроки", 0));
	Если СтрокиБезКлюча.Количество() > 0 Тогда
		
		ТекущийКод = Объект[РеквизитМаксимальныйКодСтроки];
		
		Для Каждого СтрокаТовары Из СтрокиБезКлюча Цикл
			
			ТекущийКод = ТекущийКод + 1;
			СтрокаТовары.КодСтроки = ТекущийКод;
			
		КонецЦикла;
		
		Объект[РеквизитМаксимальныйКодСтроки] = ТекущийКод;
		
	КонецЕсли;

КонецПроцедуры

// -- Проф-ИТ, #72, Соловьев А.А., 19.09.2023

Функция ПроверитьДокументПередЗапускомПроцессаПоСнятиюРезерва()
	
	ПараметрыОтбора = Новый Структура("СогласованиеКорректировки", Перечисления.проф_СогласованиеКорректировки.ПодготовленКСогласованию);
	НайденныеСтроки = Товары.НайтиСтроки(ПараметрыОтбора); 
	
	Если НайденныеСтроки.Количество() > 0 Тогда
		Возврат Истина;
	Иначе
		Возврат Ложь;
	КонецЕсли;
	
КонецФункции // ПроверитьДокументПередЗапускомПроцессаПоСнятиюРезерва()


#КонецОбласти
