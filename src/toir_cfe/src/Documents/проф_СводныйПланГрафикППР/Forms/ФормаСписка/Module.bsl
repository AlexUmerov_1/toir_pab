
#Область ОбработчикиСобытийФормы

// ++ Проф-ИТ, #227, Соловьев А.А., 19.09.2023

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Элементы.Список.РежимВыбора = Параметры.РежимВыбора;
	Если Параметры.МножественныйВыбор <> Неопределено Тогда
		Элементы.Список.МножественныйВыбор = Параметры.МножественныйВыбор;
	КонецЕсли;
	
	Если Параметры.РежимВыбора И Не ЗначениеЗаполнено(Параметры.КлючПользовательскихНастроек) Тогда
		Параметры.КлючПользовательскихНастроек = "РежимВыбора";
		Список.АвтоматическоеСохранениеПользовательскихНастроек = Ложь;
	КонецЕсли;
	
КонецПроцедуры

// -- Проф-ИТ, #227, Соловьев А.А., 19.09.2023

#КонецОбласти
