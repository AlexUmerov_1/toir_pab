
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура проф_ПриСозданииНаСервереПосле(Отказ, СтандартнаяОбработка)
	
	//++ Проф-ИТ, #326, Соловьев А.А., 31.10.2023
	Элементы.Завершить.Группировка = ГруппировкаПодчиненныхЭлементовФормы.ГоризонтальнаяВсегда;
	НовыйЭлемент = Элементы.Добавить("проф_ЭксплуатацияВозможна", Тип("ПолеФормы"), Элементы.Завершить);
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеПереключателя;
	НовыйЭлемент.ВидПереключателя = ВидПереключателя.Тумблер;
	НовыйЭлемент.ПутьКДанным = "Объект.проф_ЭксплуатацияВозможна";
	НовыйЭлемент.СписокВыбора.Добавить("Да", "Да");
	НовыйЭлемент.СписокВыбора.Добавить("Нет", "Нет");
	//-- Проф-ИТ, #326, Соловьев А.А., 31.10.2023
	
	//++ Проф-ИТ, #350, Соловьев А.А., 16.11.2023
	НовыйЭлемент = Элементы.Добавить("МатериальныеЗатратыОтменено", Тип("ПолеФормы"), Элементы.МатериальныеЗатраты);
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеФлажка;
	НовыйЭлемент.Заголовок = НСтр("ru = 'Отменено'");
	НовыйЭлемент.ПутьКДанным = "Объект.МатериальныеЗатраты.проф_Отменено";
	НовыйЭлемент.УстановитьДействие("ПриИзменении", "проф_МатериальныеЗатратыОтмененоПриИзменении");
	//-- Проф-ИТ, #350, Соловьев А.А., 16.11.2023
	
	//++ Проф-ИТ, #401, Соловьев А.А., 11.12.2023
	проф_ОбщегоНазначенияВызовСервера.УстановитьПараметрВыбораПодразделения(Элементы);
	//-- Проф-ИТ, #401, Соловьев А.А., 11.12.2023
	
	//++ Проф-ИТ, #413, Соловьев А.А., 22.12.2023
	РеквизитДинамическийСписок = ЭтаФорма["ДокументыСостоянияПоказателиНаработки"];
	РеквизитДинамическийСписок.ТекстЗапроса = ТекстЗапросаДокументыСостоянияПоказателиНаработки();
	РеквизитДинамическийСписок.Параметры.УстановитьЗначениеПараметра("ЗавершитьРемонтныеРаботы", Объект.ЗавершитьРемонтныеРаботы);
	//-- Проф-ИТ, #413, Соловьев А.А., 22.12.2023
	
	//++ Проф-ИТ, #434, Сергеев Д.Н., 16.01.2024
	НоваяГруппа = Элементы.Добавить("проф_ГруппаДатаВхДокумента", Тип("ГруппаФормы"), Элементы.ГруппаКомиссия);
	НоваяГруппа.Вид                 = ВидГруппыФормы.ОбычнаяГруппа; 
	НоваяГруппа.Группировка         = ГруппировкаПодчиненныхЭлементовФормы.Вертикальная;  
	НоваяГруппа.Отображение         = ОтображениеОбычнойГруппы.Нет; 
	НоваяГруппа.ОтображатьЗаголовок = Ложь;
	
	НовыйЭлемент = Элементы.Добавить("проф_ДатаВходящегоДокумента", Тип("ПолеФормы"), НоваяГруппа);
	НовыйЭлемент.Заголовок = НСтр("ru = 'Дата входящего документа'");
	НовыйЭлемент.ПутьКДанным = "Объект.проф_ДатаВходящегоДокумента"; 
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеВвода;
	
	НовыйЭлемент = Элементы.Добавить("проф_НаименованиеВходящегоДокумента", Тип("ПолеФормы"), НоваяГруппа);
	НовыйЭлемент.Заголовок = НСтр("ru = 'Наименование входящего документа'");
	НовыйЭлемент.ПутьКДанным = "Объект.проф_НаименованиеВходящегоДокумента";
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеВвода;

	НовыйЭлемент = Элементы.Добавить("проф_НомерВходящегоДокумента", Тип("ПолеФормы"), НоваяГруппа);
	НовыйЭлемент.Заголовок = НСтр("ru = 'Номер входящего документа'");
	НовыйЭлемент.ПутьКДанным = "Объект.проф_НомерВходящегоДокумента"; 
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеВвода;
	//-- Проф-ИТ, #434, Сергеев Д.Н., 16.01.2024	
	
	//++ Проф-ИТ, #190, Сергеев Д.Н., 22.12.2023
	УстановитьДоступностьЭлементовФормы();      
	//-- Проф-ИТ, #190, Сергеев Д.Н., 22.12.2023
	
	//++ Проф-ИТ, #515, Соловьев А.А., 26.02.2024
	НовыйЭлемент = Элементы.Добавить("МатериальныеЗатратыПричинаОтмены", Тип("ПолеФормы"), Элементы.МатериальныеЗатраты);
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.Заголовок = НСтр("ru = 'Причина отмены'");
	НовыйЭлемент.ПутьКДанным = "Объект.МатериальныеЗатраты.проф_ПричинаОтмены";
	
	НовыйЭлемент = Элементы.Добавить("МатериальныеЗатратыОтменил", Тип("ПолеФормы"), Элементы.МатериальныеЗатраты);
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.Заголовок = НСтр("ru = 'Отменил'");
	НовыйЭлемент.ПутьКДанным = "Объект.МатериальныеЗатраты.проф_Отменил";
	
	проф_УстановитьУсловноеОформление();
	//-- Проф-ИТ, #515, Соловьев А.А., 26.02.2024
	
	//++ Проф-ИТ, #523, Соловьев А.А., 13.03.2024
	МассивДобавляемыхРеквизитов = Новый Массив;
	
	НовыйРеквизит = Новый РеквизитФормы("проф_ДокументДляИсправления", 
		Новый ОписаниеТипов("ДокументСсылка.ВнутреннееПотреблениеТоваров"));
	МассивДобавляемыхРеквизитов.Добавить(НовыйРеквизит);
	
	ИзменитьРеквизиты(МассивДобавляемыхРеквизитов);
	//-- Проф-ИТ, #523, Соловьев А.А., 13.03.2024
	
КонецПроцедуры

&НаКлиенте
Процедура проф_ПослеЗаписиПосле(ПараметрыЗаписи)
	
	//++ Проф-ИТ, #413, Соловьев А.А., 22.12.2023
	РеквизитДинамическийСписок = ЭтаФорма["ДокументыСостоянияПоказателиНаработки"];
	РеквизитДинамическийСписок.Параметры.УстановитьЗначениеПараметра("ЗавершитьРемонтныеРаботы", Объект.ЗавершитьРемонтныеРаботы);
	Элементы.ДокументыСостоянияПоказателиНаработки.Обновить();
	//-- Проф-ИТ, #413, Соловьев А.А., 22.12.2023
	
	//++ Проф-ИТ, #523, Соловьев А.А., 13.03.2024
	Если Объект.Проведен Тогда 
		КоличествоДокументовВП = ДокументыВнутреннееПотребление.Количество();
		Если КоличествоДокументовВП > 0 Тогда
			
			Если КоличествоДокументовВП = 1 Тогда 
				ПриЗакрытииФормыВыбораВнутреннегоПотребления(ДокументыВнутреннееПотребление[0].ВнутреннееПотребление, Неопределено);
			Иначе
				
				ДокументыВП = МассивДокументовВП();
				
				НастройкиКомпоновки = Новый НастройкиКомпоновкиДанных;
				ЭлементОтбора = НастройкиКомпоновки.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных")); 
				ЭлементОтбора.ЛевоеЗначение  = Новый ПолеКомпоновкиДанных("Ссылка");
				ЭлементОтбора.ВидСравнения   = ВидСравненияКомпоновкиДанных.ВСписке;
				ЭлементОтбора.Использование  = Истина;
				ЭлементОтбора.ПравоеЗначение = ДокументыВП;
				
				ПараметрыФормы = Новый Структура;
				ПараметрыФормы.Вставить("ФиксированныеНастройки", НастройкиКомпоновки);
				ПараметрыФормы.Вставить("РежимВыбора", Истина);
				ПараметрыФормы.Вставить("МножественныйВыбор", Ложь);
				
				ОписаниеОповещения = Новый ОписаниеОповещения("ПриЗакрытииФормыВыбораВнутреннегоПотребления", ЭтаФорма, "Подбор");
				
				ОткрытьФорму("Документ.ВнутреннееПотреблениеТоваров.ФормаВыбора", ПараметрыФормы,
					ЭтаФорма, , , , ОписаниеОповещения);
				
			КонецЕсли;
			
		КонецЕсли;
	КонецЕсли;
	//-- Проф-ИТ, #523, Соловьев А.А., 13.03.2024
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
&ИзменениеИКонтроль("ДеревоРемонтныхРаботВыполненоПриИзменении")
Процедура проф_ДеревоРемонтныхРаботВыполненоПриИзменении(Элемент)

	ТекСтрокаДерева = Элементы.ДеревоРемонтныхРабот.ТекущиеДанные;

	ТекСтрокаДерева.ПроцентВыполненияРабот = ?(ТекСтрокаДерева.Выполнено, 100, 0);
	СтруктураСтроки = Новый Структура("ID, РемонтыОборудования_ID, Родитель_ID", ТекСтрокаДерева.ID, ТекСтрокаДерева.РемонтыОборудования_ID, ТекСтрокаДерева.Родитель_ID);
	СтрокаРодДерева = ТекСтрокаДерева.ПолучитьРодителя();
	Если Не СтрокаРодДерева = Неопределено Тогда
		СтруктураРодСтроки = Новый Структура("ID, РемонтыОборудования_ID, Родитель_ID", ТекСтрокаДерева.ID, ТекСтрокаДерева.РемонтыОборудования_ID, ТекСтрокаДерева.Родитель_ID);
	Иначе
		СтруктураРодСтроки = Неопределено;
	КонецЕсли;

	Если НЕ ТекСтрокаДерева = Неопределено Тогда 
		МассивСтрок = Объект.РемонтныеРаботы.НайтиСтроки(СтруктураСтроки);
		Если МассивСтрок.Количество() Тогда
			МассивСтрок[0].Выполнено = ТекСтрокаДерева.Выполнено;
		КонецЕсли;
	КонецЕсли;

	ЗаполнитьПроцентВыполненияРемонтныхРаботВСтрокахДерева(СтруктураСтроки, СтруктураРодСтроки);

	ОбновитьПроцентВыполненияВСтрокахТЧ(ДеревоРемонтныхРабот.ПолучитьЭлементы()[0]);

#Удаление //++ Проф-ИТ, #290, Горетовская М.С., 04.10.2023
	Если Не ТекСтрокаДерева.Выполнено Тогда

		Объект.ЗавершитьРемонтныеРаботы = Ложь;
		Если НЕ Элементы.РемонтыОборудования.ТекущиеДанные = Неопределено Тогда
			МасСтр = Объект.РемонтыОборудования.НайтиСтроки(Новый Структура("ID", Элементы.РемонтыОборудования.ТекущиеДанные.ID));
			Если МасСтр.Количество() > 0 Тогда
				МасСтр[0].ЗавершитьРемонт = Ложь;
			КонецЕсли;
		КонецЕсли;

	КонецЕсли;
#КонецУдаления //-- Проф-ИТ, #290, Горетовская М.С., 04.10.2023

	Для Каждого Строка Из ДеревоРемонтныхРабот.ПолучитьЭлементы() Цикл
		РазвернутьДеревоРемонтныхРабот(Строка);
	КонецЦикла;
	#Вставка
	//++ Проф-ИТ, #350, Соловьев А.А., 16.11.2023
	Если Не ТекСтрокаДерева.Выполнено Тогда 
		СтруктураСтроки = Новый Структура("ID, РемонтыОборудования_ID",
			ТекСтрокаДерева.ID, ТекСтрокаДерева.РемонтыОборудования_ID);
		СтрокиМатЗатрат = Объект.МатериальныеЗатраты.НайтиСтроки(СтруктураСтроки);
		Для Каждого СтрокаМатЗатрат Из СтрокиМатЗатрат Цикл
			СтрокаМатЗатрат.проф_Отменено = Истина;
		КонецЦикла;
	КонецЕсли;
	//-- Проф-ИТ, #350, Соловьев А.А., 16.11.2023
	#КонецВставки
КонецПроцедуры

&НаКлиенте
Процедура проф_ЗавершитьРемонтныеРаботыПриИзмененииПосле(Элемент)
	
	//++ Проф-ИТ, #326, Соловьев А.А., 02.11.2023
	Если Объект.ЗавершитьРемонтныеРаботы Тогда 
		//++ Проф-ИТ, #468, Соловьев А.А., 05.02.2024
		//Объект["проф_ЭксплуатацияВозможна"] = "";
		//-- Проф-ИТ, #468, Соловьев А.А., 05.02.2024
	КонецЕсли;
	//-- Проф-ИТ, #326, Соловьев А.А., 02.11.2023
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыДеревоРемонтныхРабот

&НаКлиенте
Процедура проф_ДеревоРемонтныхРаботВыполненоПриИзмененииПосле(Элемент)
	//++ Проф-ИТ, #326, Соловьев А.А., 31.10.2023
	Элементы["проф_ЭксплуатацияВозможна"].ТолькоПросмотр = ЗапретРедактированияРеквизитаЭксплуатацияВозможна(
																	ДеревоРемонтныхРабот.ПолучитьЭлементы()[0]);
	//-- Проф-ИТ, #326, Соловьев А.А., 31.10.2023
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыРемонтыОборудования

&НаКлиенте
Процедура проф_РемонтыОборудованияПриАктивизацииСтрокиПосле(Элемент)
	//++ Проф-ИТ, #326, Соловьев А.А., 31.10.2023
	СтрокиРемонтныхРабот = ДеревоРемонтныхРабот.ПолучитьЭлементы();
	Если СтрокиРемонтныхРабот.Количество() > 0 Тогда 
		Элементы["проф_ЭксплуатацияВозможна"].ТолькоПросмотр = ЗапретРедактированияРеквизитаЭксплуатацияВозможна(
																							СтрокиРемонтныхРабот[0]);
	КонецЕсли;
	//-- Проф-ИТ, #326, Соловьев А.А., 31.10.2023
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыМатериальныеЗатраты

//++ Проф-ИТ, #350, Соловьев А.А., 16.11.2023

&НаКлиенте
Процедура проф_МатериальныеЗатратыОтмененоПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.МатериальныеЗатраты.ТекущиеДанные;
	
	//++ Проф-ИТ, #515, Соловьев А.А., 27.02.2024
	//Если ТекущиеДанные.проф_Отменено И Объект.РемонтныеРаботы.Количество() Тогда 
	//	СтруктураПоиска = Новый Структура("ID, РемонтыОборудования_ID",
	//						ТекущиеДанные.ID, ТекущиеДанные.РемонтыОборудования_ID);
	//	СтрокиДерева = ДеревоРемонтныхРабот.ПолучитьЭлементы();
	//	ОбновитьВыполненоВДереве(СтрокиДерева, СтруктураПоиска);
	//	ОбновитьПроцентВыполненияВСтрокахТЧ(СтрокиДерева[0]);
	//	
	//	Для Каждого Строка Из ДеревоРемонтныхРабот.ПолучитьЭлементы() Цикл
	//		РазвернутьДеревоРемонтныхРабот(Строка);
	//	КонецЦикла;
	//КонецЕсли;
	
	ТекущиеДанные.проф_Отменил = ТекущиеДанныеОтменил(ТекущиеДанные.проф_Отменено);
	Если Не ТекущиеДанные.проф_Отменено Тогда 
		ТекущиеДанные.проф_ПричинаОтмены = ПредопределенноеЗначение("Справочник.проф_ПричиныОтмены.ПустаяСсылка");
	КонецЕсли;
	//-- Проф-ИТ, #515, Соловьев А.А., 27.02.2024
	
КонецПроцедуры
//-- Проф-ИТ, #350, Соловьев А.А., 16.11.2023

#КонецОбласти 

#Область ОбработчикиСобытийЭлементовТаблицыИсполнителиПоРемонтам

&НаКлиенте
Процедура проф_ИсполнителиПоРемонтамИсполнительПриИзмененииПосле(Элемент)
	
	УстановитьДоступностьЭлементовФормы();
	
КонецПроцедуры 

#КонецОбласти 

#Область ОбработчикиСобытийЭлементовТаблицыФормыСерийныеЗапчасти

&НаКлиенте
Процедура проф_СерийныеЗапчастиПриНачалеРедактированияВместо(Элемент, НоваяСтрока, Копирование)
	//++ Проф-ИТ, #480, Соловьев А.А., 07.02.2024
	ТекДанныеРемонт = Элементы.РемонтыОборудования.ТекущиеДанные;
	ТекДанныеЗапчасти = Элементы.СерийныеЗапчасти.ТекущиеДанные;
	
	Если ТекДанныеРемонт <> Неопределено И НоваяСтрока И НЕ Копирование Тогда
		ТекДанныеЗапчасти.Количество = 1;
		ТекДанныеЗапчасти.КоличествоНовое = 1;
		ТекДанныеЗапчасти.СтатусДвиженияСерийныхЗЧ = ПредопределенноеЗначение("Перечисление.торо_ВидыДвиженияСерийныхЗЧ.Списание");
		ТекДанныеЗапчасти.СтатусДвиженияНовойЗЧ = ПредопределенноеЗначение("Перечисление.торо_ВидыДвиженияСерийныхЗЧ.Установка");
	КонецЕсли;
	//-- Проф-ИТ, #480, Соловьев А.А., 07.02.2024
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура проф_УстановитьУсловноеОформление()
	
	//++ Проф-ИТ, #515, Соловьев А.А., 05.03.2024
	Элемент = УсловноеОформление.Элементы.Добавить();
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.МатериальныеЗатратыПричинаОтмены.Имя);
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.МатериальныеЗатраты.проф_Отменено");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Ложь;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("ТолькоПросмотр", Истина);
	
	//
	
	Элемент = УсловноеОформление.Элементы.Добавить();
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.МатериальныеЗатратыПричинаОтмены.Имя);
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.МатериальныеЗатраты.проф_Отменено");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("ТолькоПросмотр", Ложь);
	
	//
	
	Элемент = УсловноеОформление.Элементы.Добавить();
	
	ГруппаОтбора1 = Элемент.Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
	ГруппаОтбора1.ТипГруппы = ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИ;
	
	ОтборЭлемента = ГруппаОтбора1.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Проведен");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;
	
	ОтборЭлемента = ГруппаОтбора1.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.МатериальныеЗатраты.проф_Отменено");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.МатериальныеЗатратыОтменено.Имя);
	Элемент.Оформление.УстановитьЗначениеПараметра("ТолькоПросмотр", Истина);
	//-- Проф-ИТ, #515, Соловьев А.А., 05.03.2024
	
КонецПроцедуры

&НаКлиенте
&ИзменениеИКонтроль("ДобавитьВнутреннееПотреблениеПослеВопроса")
Процедура проф_ДобавитьВнутреннееПотреблениеПослеВопроса()
	
	ТекущийРемонт = Элементы.РемонтыОборудования.ТекущиеДанные;

	Если Не ТекущийРемонт = Неопределено Тогда

		МассивСтрокНоменклатуры = СформироватьМассивНоменклатурыКСписаниюНаСервере(ТекущийРемонт.ID);

		Если МассивСтрокНоменклатуры = Неопределено Тогда

			// Значение неопределено, когда вся имеющаяся в заявке номенклатура уже есть в документе заказ на вн потребл.			
			МассивСтрокНоменклатуры = Новый Массив;

		КонецЕсли;	

		ЗначенияЗаполнения = Новый Структура();
		ЗначенияЗаполнения.Вставить("МассивСтрокНоменклатуры",МассивСтрокНоменклатуры);
		ЗначенияЗаполнения.Вставить("Организация", Объект.Организация);
		ЗначенияЗаполнения.Вставить("Подразделение", Объект.Подразделение);
		ЗначенияЗаполнения.Вставить("Ответственный", Объект.Ответственный);

		#Вставка
		//++ Проф-ИТ, #27, Соловьев А.А., 26.09.2023
		ЗначенияЗаполнения.Вставить("проф_ДокументОснование", ТекущийРемонт.ДокументИсточник);
		//-- Проф-ИТ, #27, Соловьев А.А., 26.09.2023
		//++ Проф-ИТ, #447, Соловьев А.А., 30.01.2024
		ЗначенияЗаполнения.Вставить("СозданиеНаОснованииАктаОВыполненииЭтапаРабот", Истина);
		//-- Проф-ИТ, #447, Соловьев А.А., 30.01.2024
		//++ Проф-ИТ, #451, Соловьев А.А., 31.01.2024
		ЗначенияЗаполнения.Вставить("Дата", Объект.Дата);
		//-- Проф-ИТ, #451, Соловьев А.А., 31.01.2024
		//++ Проф-ИТ, #523, Соловьев А.А., 13.03.2024
		Если ЗначениеЗаполнено(ЭтотОбъект["проф_ДокументДляИсправления"]) Тогда
			ЗначенияЗаполнения.Вставить("Дата", ЗарплатаКадрыКлиентСервер.ДатаСеанса());
			ЗначенияЗаполнения.Вставить("проф_Исправление", Истина);
			ЗначенияЗаполнения.Вставить("проф_ИсправляемыйДокумент", ЭтотОбъект["проф_ДокументДляИсправления"]);
			ЗначенияЗаполнения.Вставить("проф_СторнируемыйДокумент", ЭтотОбъект["проф_ДокументДляИсправления"]);
		КонецЕсли;
		//-- Проф-ИТ, #523, Соловьев А.А., 13.03.2024
		#КонецВставки
		ЗначенияЗаполнения.Вставить("Комментарий" , Строка(Объект.Ссылка) + " " + ТекущийРемонт.ОбъектРемонта + " " + 
		ТекущийРемонт.ВидРемонтныхРабот + " " + ТекущийРемонт.ДатаНачала + " - " + 
		ТекущийРемонт.ДатаОкончания);

		СтруктураПараметров = Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения);

		МассивID = Новый Массив;
		МассивID.Добавить(ТекущийРемонт.ID);
		СтруктураПараметров.Вставить("ID", МассивID);

		СтруктураПередаваемыхВОбработчикПараметров = Новый Структура;
		СтруктураПередаваемыхВОбработчикПараметров.Вставить("ТекущийРемонт_ID",ТекущийРемонт.ID);

		ОписаниеОповещения = Новый ОписаниеОповещения("ДобавитьИзменитьЗаписиВРегистреТоро_ИнтеграцияДокументов", ЭтаФорма, СтруктураПередаваемыхВОбработчикПараметров);
		ОткрытьФорму("Документ.ВнутреннееПотреблениеТоваров.ФормаОбъекта", СтруктураПараметров, ЭтаФорма,,,,ОписаниеОповещения);

	КонецЕсли;

	#Вставка //++ Проф-ИТ, #523, Соловьев А.А., 13.03.2024
	ЭтотОбъект["проф_ДокументДляИсправления"] = Неопределено;
	#КонецВставки //-- Проф-ИТ, #523, Соловьев А.А., 13.03.2024
	
КонецПроцедуры

&НаКлиенте
Функция ЗапретРедактированияРеквизитаЭксплуатацияВозможна(СтрокаДерева)
	//++ Проф-ИТ, #326, Соловьев А.А., 31.10.2023
	ЗапретРедактирования = Истина;
	
	Для Каждого ТекСтрокаДерева Из СтрокаДерева.ПолучитьЭлементы() Цикл
		Если Не ТекСтрокаДерева.Выполнено Тогда 
			Возврат Ложь;
		КонецЕсли;
		ЗапретРедактирования = ЗапретРедактированияРеквизитаЭксплуатацияВозможна(ТекСтрокаДерева);
		Если ЗапретРедактирования = Ложь Тогда 
			Прервать;
		КонецЕсли;
	КонецЦикла;
	
	Возврат ЗапретРедактирования;
	//-- Проф-ИТ, #326, Соловьев А.А., 31.10.2023
КонецФункции

//++ Проф-ИТ, #329, Соловьев А.А., 14.11.2023

&НаСервере
&ИзменениеИКонтроль("ПометитьУдалениеНаСервере")
Функция проф_ПометитьУдалениеНаСервере(СтруктураПараметров)

	Попытка

		ОбъектНаУдаление = СтруктураПараметров.ДокументЕРПСсылка.ПолучитьОбъект();

		Если ОбъектНаУдаление.Проведен Тогда
			ОбъектНаУдаление.Записать(РежимЗаписиДокумента.ОтменаПроведения);
		КонецЕсли;

		ОбъектНаУдаление.ПометкаУдаления = Истина;
		ОбъектНаУдаление.Записать();

		МенеджерЗаписи = РегистрыСведений.торо_ИнтеграцияДокументов.СоздатьМенеджерЗаписи();
		МенеджерЗаписи.ID           = СтруктураПараметров.ID;
		МенеджерЗаписи.ДокументЕРП  = СтруктураПараметров.ДокументЕРПСсылка;
		МенеджерЗаписи.ДокументТОИР = Объект.Ссылка;
		МенеджерЗаписи.Прочитать();

		Если МенеджерЗаписи.Выбран() Тогда
			МенеджерЗаписи.Удалить();
		КонецЕсли; 

		Если ТипЗнч(ОбъектНаУдаление.Ссылка) = Тип("ДокументСсылка.ВнутреннееПотреблениеТоваров") Тогда 
			ОбновитьДокументыВнутреннегоПотребленияСервер();
		ИначеЕсли ТипЗнч(ОбъектНаУдаление.Ссылка) = Тип("ДокументСсылка.ОприходованиеИзлишковТоваров") Тогда
			ОбновитьДокументыОприходованияИзлишковСервер();
		КонецЕсли;
		#Вставка
		Документы.торо_АктОВыполненииЭтапаРабот.проф_ДвиженияПоРегистру_проф_СоответствиеАктовИВнутреннегоПотребления(
																										Объект.Ссылка);
		#КонецВставки
	Исключение

		Возврат ОписаниеОшибки();

	КонецПопытки; 

	Возврат Истина;

КонецФункции

&НаСервере
&ИзменениеИКонтроль("ДобавитьИзменитьЗаписиВРегистреТоро_ИнтеграцияДокументовСервер")
Процедура проф_ДобавитьИзменитьЗаписиВРегистреТоро_ИнтеграцияДокументовСервер1(Результат, ДополнительныеПараметры)

	Если ТипЗнч(ДополнительныеПараметры) = Тип("Структура") Тогда
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		|	торо_ИнтеграцияДокументов.ДокументЕРП
		|ИЗ
		|	РегистрСведений.торо_ИнтеграцияДокументов КАК торо_ИнтеграцияДокументов
		|ГДЕ
		|	торо_ИнтеграцияДокументов.ID = &ID
		|	И торо_ИнтеграцияДокументов.ДокументТОИР = &Ссылка
		|	И торо_ИнтеграцияДокументов.ДокументЕРП  = &СсылкаНаДокЕРП";

		Запрос.УстановитьПараметр("ID"             , ДополнительныеПараметры.ТекущийРемонт_ID);
		Запрос.УстановитьПараметр("Ссылка"         , Объект.Ссылка);
		Запрос.УстановитьПараметр("СсылкаНаДокЕРП" , ДополнительныеПараметры.Ссылка);

		РезультатЗапроса = Запрос.Выполнить();
		Если РезультатЗапроса.Пустой() Тогда
			ДобавитьЗаписьВРегистрТоро_ИнтеграцияДокументовНаСервере(ДополнительныеПараметры.ТекущийРемонт_ID, Объект.Ссылка, ДополнительныеПараметры.Ссылка);
		КонецЕсли; 

		#Вставка
		Документы.торо_АктОВыполненииЭтапаРабот.проф_ДвиженияПоРегистру_проф_СоответствиеАктовИВнутреннегоПотребления(
																										Объект.Ссылка);
		#КонецВставки
	КонецЕсли; 	
КонецПроцедуры
//-- Проф-ИТ, #329, Соловьев А.А., 14.11.2023

//++ Проф-ИТ, #350, Соловьев А.А., 16.11.2023

&НаКлиенте
Процедура ОбновитьВыполненоВДереве(СтрокиДерева, СтруктураПоиска)
		
	Для Каждого ТекСтрокаДерева Из СтрокиДерева Цикл
		
		ПодчиненныеСтроки = ТекСтрокаДерева.ПолучитьЭлементы();
		Если ПодчиненныеСтроки.Количество() Тогда 
			ОбновитьВыполненоВДереве(ПодчиненныеСтроки, СтруктураПоиска);
			Продолжить;
		КонецЕсли;	
		
		ОбновитьВыполненоВСтрокеДерева(ТекСтрокаДерева, СтруктураПоиска);
				
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьВыполненоВСтрокеДерева(ТекСтрокаДерева, СтруктураПоиска)
	
	Если ТекСтрокаДерева.ID = СтруктураПоиска.ID 
	И ТекСтрокаДерева.РемонтыОборудования_ID = СтруктураПоиска.РемонтыОборудования_ID Тогда 
		
		ТекСтрокаДерева.Выполнено = Ложь;
		
		ТекСтрокаДерева.ПроцентВыполненияРабот = 0;
		
		СтрокаРодДерева = ТекСтрокаДерева.ПолучитьРодителя();
		Если Не СтрокаРодДерева = Неопределено Тогда
			СтруктураРодСтроки = Новый Структура("ID, РемонтыОборудования_ID, Родитель_ID",
				ТекСтрокаДерева.ID, ТекСтрокаДерева.РемонтыОборудования_ID, ТекСтрокаДерева.Родитель_ID);
		Иначе
			СтруктураРодСтроки = Неопределено;
		КонецЕсли;
		
		СтруктураСтроки = Новый Структура("ID, РемонтыОборудования_ID, Родитель_ID",
			ТекСтрокаДерева.ID, ТекСтрокаДерева.РемонтыОборудования_ID, ТекСтрокаДерева.Родитель_ID);
		Если НЕ ТекСтрокаДерева = Неопределено Тогда 
			МассивСтрок = Объект.РемонтныеРаботы.НайтиСтроки(СтруктураСтроки);
			Если МассивСтрок.Количество() Тогда
				МассивСтрок[0].Выполнено = ТекСтрокаДерева.Выполнено;
			КонецЕсли;
		КонецЕсли;
		
		ЗаполнитьПроцентВыполненияРемонтныхРаботВСтрокахДерева(СтруктураСтроки, СтруктураРодСтроки);
		
	КонецЕсли;	
	
КонецПроцедуры	

&НаСервере
&ИзменениеИКонтроль("СформироватьМассивНоменклатурыКСписаниюНаСервере")
Функция проф_СформироватьМассивНоменклатурыКСписаниюНаСервере(ID)

	#Удаление //++ Проф-ИТ, #523, Соловьев А.А., 13.03.2024
	УжеСписаннаяНоменклатура = ПолучитьТаблицуСписаннойНоменклатуры(ID, Тип("ДокументСсылка.ВнутреннееПотреблениеТоваров"));
	#КонецУдаления 
	#Вставка
	Если ЗначениеЗаполнено(ЭтотОбъект["проф_ДокументДляИсправления"]) Тогда 
		УжеСписаннаяНоменклатура = Новый ТаблицаЗначений;
	Иначе
		УжеСписаннаяНоменклатура = ПолучитьТаблицуСписаннойНоменклатуры(ID, Тип("ДокументСсылка.ВнутреннееПотреблениеТоваров"));
	КонецЕсли;
	#КонецВставки //-- Проф-ИТ, #523, Соловьев А.А., 13.03.2024
	
	Если ТипЗнч(ID) = Тип("Массив") Тогда
		#Удаление //++ Проф-ИТ, #350, Соловьев А.А., 21.11.2023
		НоменклатураКЗаказу = Объект.МатериальныеЗатраты.Выгрузить();
		#КонецУдаления
		#Вставка
		СтруктураОтбора = Новый Структура("проф_Отменено", Ложь);
		НоменклатураКЗаказу = Объект.МатериальныеЗатраты.Выгрузить(СтруктураОтбора);
		#КонецВставки //-- Проф-ИТ, #350, Соловьев А.А., 21.11.2023
		Запчасти = Объект.СерийныеЗапчасти.Выгрузить();
	Иначе
		#Удаление //++ Проф-ИТ, #350, Соловьев А.А., 21.11.2023
		НоменклатураКЗаказу = Объект.МатериальныеЗатраты.Выгрузить(Новый Структура("РемонтыОборудования_ID",ID));
		#КонецУдаления
		#Вставка
		СтруктураОтбора = Новый Структура("проф_Отменено, РемонтыОборудования_ID", Ложь, ID);
		НоменклатураКЗаказу = Объект.МатериальныеЗатраты.Выгрузить(СтруктураОтбора);
		#КонецВставки //-- Проф-ИТ, #350, Соловьев А.А., 21.11.2023
		Запчасти = Объект.СерийныеЗапчасти.Выгрузить(Новый Структура("РемонтыОборудования_ID",ID));	
	КонецЕсли;


	НоменклатураКЗаказу.Колонки.Добавить("Серия", Новый ОписаниеТипов("СправочникСсылка.СерииНоменклатуры"));

	Для каждого Строка Из Запчасти Цикл
		НС = НоменклатураКЗаказу.Добавить();
		ЗаполнитьЗначенияСвойств(НС, Строка);
		НС.Номенклатура = Строка.НоменклатураНовая;
		НС.ХарактеристикаНоменклатуры = Строка.ХарактеристикаНоменклатурыНовая;
		НС.Количество = Строка.КоличествоНовое;
		НС.ЕдиницаИзмерения = Строка.ЕдиницаИзмеренияНовая;
		НС.КоличествоЕдиниц = Строка.КоличествоНовое;
		НС.Серия = Строка.СерияНоменклатурыНовая;
		НС.ХарактеристикиИспользуются = Строка.ХарактеристикиИспользуютсяДляНовой;
	КонецЦикла;

	Для каждого Стр ИЗ НоменклатураКЗаказу Цикл 
		Если НЕ ЗначениеЗаполнено(Стр.ЕдиницаИзмерения) Тогда 
			Стр.КоличествоЕдиниц = Стр.Количество*1;
		КонецЕсли;
	КонецЦикла;

	НоменклатураКЗаказу.Свернуть("Номенклатура, ХарактеристикаНоменклатуры, ХарактеристикиИспользуются, Серия", "КоличествоЕдиниц");

	Товары = Новый Массив;
	Услуга = Перечисления.ТипыНоменклатуры.Услуга;
	Работа = Перечисления.ТипыНоменклатуры.Работа;

	Для каждого Строка Из НоменклатураКЗаказу Цикл

		Если Строка.Номенклатура.ТипНоменклатуры = Услуга
			ИЛИ Строка.Номенклатура.ТипНоменклатуры = Работа Тогда
			Продолжить;
		КонецЕсли; 
		КоличествоУжеЗаказаннойНоменклатуры = 0;

		Если УжеСписаннаяНоменклатура.Количество() > 0 Тогда
			НайдСтроки = УжеСписаннаяНоменклатура.НайтиСтроки(Новый Структура("Номенклатура, Характеристика",Строка.Номенклатура,Строка.ХарактеристикаНоменклатуры));


			Если Не НайдСтроки = Неопределено Тогда
				Для каждого НайденнаяСтрока Из НайдСтроки Цикл
					КоличествоУжеЗаказаннойНоменклатуры = КоличествоУжеЗаказаннойНоменклатуры + НайденнаяСтрока.Количество;
				КонецЦикла; 
			КонецЕсли;
		КонецЕсли;

		Если УжеСписаннаяНоменклатура.Количество() = 0
			ИЛИ Строка.КоличествоЕдиниц > КоличествоУжеЗаказаннойНоменклатуры Тогда

			Структура = Новый Структура("Номенклатура, Характеристика, ХарактеристикиИспользуются, Количество, Серия",
			Строка.Номенклатура,
			Строка.ХарактеристикаНоменклатуры,
			Строка.ХарактеристикиИспользуются,
			(Строка.КоличествоЕдиниц - КоличествоУжеЗаказаннойНоменклатуры),
			Строка.Серия);
			Товары.Добавить(Структура);

		КонецЕсли;	

	КонецЦикла;

	Если Товары.Количество() > 0 Тогда
		Возврат Товары;
	Иначе
		Возврат Неопределено;
	КонецЕсли; 

КонецФункции
//-- Проф-ИТ, #350, Соловьев А.А., 16.11.2023

//++ Проф-ИТ, #413, Соловьев А.А., 22.12.2023
Функция ТекстЗапросаДокументыСостоянияПоказателиНаработки()
	
	Возврат
	"ВЫБРАТЬ
	|	МАКСИМУМ(торо_НаработкаОбъектовРемонта.ДатаКон) КАК ДатаКон,
	|	торо_НаработкаОбъектовРемонта.ОбъектРемонта КАК ОбъектРемонта
	|ПОМЕСТИТЬ втДанныеРегистраНаработка
	|ИЗ
	|	РегистрНакопления.торо_НаработкаОбъектовРемонта КАК торо_НаработкаОбъектовРемонта
	|ГДЕ
	|	торо_НаработкаОбъектовРемонта.Регистратор ССЫЛКА Документ.торо_УчетНаработкиОборудования
	|	И &ЗавершитьРемонтныеРаботы
	|	И торо_НаработкаОбъектовРемонта.ОбъектРемонта В
	|			(ВЫБРАТЬ
	|				торо_АктОВыполненииЭтапаРаботРемонтыОборудования.ОбъектРемонта КАК ОбъектРемонта
	|			ИЗ
	|				Документ.торо_АктОВыполненииЭтапаРабот.РемонтыОборудования КАК торо_АктОВыполненииЭтапаРаботРемонтыОборудования
	|			ГДЕ
	|				торо_АктОВыполненииЭтапаРаботРемонтыОборудования.Ссылка = &ДокументОснование
	|			СГРУППИРОВАТЬ ПО
	|				торо_АктОВыполненииЭтапаРаботРемонтыОборудования.ОбъектРемонта)
	|
	|СГРУППИРОВАТЬ ПО
	|	торо_НаработкаОбъектовРемонта.ОбъектРемонта
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	торо_УчетКонтролируемыхПоказателей.Ссылка КАК Ссылка,
	|	""Учет контролируемых показателей"" КАК ТипДокумента,
	|	ВЫБОР
	|		КОГДА торо_УчетКонтролируемыхПоказателей.Проведен
	|			ТОГДА 1
	|		ИНАЧЕ 0
	|	КОНЕЦ КАК ИндексКартинки
	|ИЗ
	|	Документ.торо_УчетКонтролируемыхПоказателей КАК торо_УчетКонтролируемыхПоказателей
	|ГДЕ
	|	&УчетКонтролируемыхПоказателей
	|	И торо_УчетКонтролируемыхПоказателей.ДокументОснование = &ДокументОснование
	|	И НЕ торо_УчетКонтролируемыхПоказателей.ПометкаУдаления
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	торо_УчетНаработкиОборудования.Ссылка,
	|	""Учет наработки оборудования"",
	|	ВЫБОР
	|		КОГДА торо_УчетНаработкиОборудования.Проведен
	|			ТОГДА 1
	|		ИНАЧЕ 0
	|	КОНЕЦ
	|ИЗ
	|	Документ.торо_УчетНаработкиОборудования КАК торо_УчетНаработкиОборудования
	|ГДЕ
	|	&УчетНаработки
	|	И торо_УчетНаработкиОборудования.ДокументОснование = &ДокументОснование
	|	И НЕ торо_УчетНаработкиОборудования.ПометкаУдаления
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	торо_СостоянияОбъектовРемонта.Ссылка,
	|	""Состояния объектов ремонта"",
	|	ВЫБОР
	|		КОГДА торо_СостоянияОбъектовРемонта.Проведен
	|			ТОГДА 1
	|		ИНАЧЕ 0
	|	КОНЕЦ
	|ИЗ
	|	Документ.торо_СостоянияОбъектовРемонта КАК торо_СостоянияОбъектовРемонта
	|ГДЕ
	|	&УчетСостояний
	|	И торо_СостоянияОбъектовРемонта.ДокументОснование = &ДокументОснование
	|	И НЕ торо_СостоянияОбъектовРемонта.ПометкаУдаления
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	торо_НаработкаОбъектовРемонта.Регистратор,
	|	""Учет наработки оборудования"",
	|	ВЫБОР
	|		КОГДА торо_УчетНаработкиОборудования.Проведен
	|			ТОГДА 1
	|		ИНАЧЕ 0
	|	КОНЕЦ
	|ИЗ
	|	втДанныеРегистраНаработка КАК втДанныеРегистраНаработка
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрНакопления.торо_НаработкаОбъектовРемонта КАК торо_НаработкаОбъектовРемонта
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.торо_УчетНаработкиОборудования КАК торо_УчетНаработкиОборудования
	|			ПО (торо_УчетНаработкиОборудования.Ссылка = торо_НаработкаОбъектовРемонта.Регистратор)
	|		ПО втДанныеРегистраНаработка.ОбъектРемонта = торо_НаработкаОбъектовРемонта.ОбъектРемонта
	|			И втДанныеРегистраНаработка.ДатаКон = торо_НаработкаОбъектовРемонта.ДатаКон
	|ГДЕ
	|	торо_НаработкаОбъектовРемонта.Регистратор ССЫЛКА Документ.торо_УчетНаработкиОборудования";

КонецФункции
//-- Проф-ИТ, #413, Соловьев А.А., 22.12.2023

&НаСервере
Процедура УстановитьДоступностьЭлементовФормы() //++ Проф-ИТ, #190, Сергеев Д.Н., 22.12.2023
	
	Если Объект.ИсполнителиПоРемонтам.Количество() > 0 
		И ТипЗнч(Объект.ИсполнителиПоРемонтам[0].Исполнитель) = Тип("СправочникСсылка.Контрагенты") Тогда
		
		Элементы["проф_ГруппаДатаВхДокумента"].Видимость = Истина;  
	Иначе
		Элементы["проф_ГруппаДатаВхДокумента"].Видимость = Ложь; 	
	КонецЕсли;
	
	Если НЕ Объект.проф_КорректироовкиЗапрещены Тогда
	    Возврат;	
	КонецЕсли; 
	
	ТолькоПросмотр = Истина;

КонецПроцедуры //-- Проф-ИТ, #190, Сергеев Д.Н., 22.12.2023

&НаКлиенте
&ИзменениеИКонтроль("СводныйДокументВнутреннееПотреблениеПослеВопроса")
Процедура проф_СводныйДокументВнутреннееПотреблениеПослеВопроса()

	СтруктураПередаваемыхВОбработчикПараметров = Новый Структура;

	МассивСтрокНоменклатуры = Новый Массив;	
	МассивID = Новый Массив;

	СтрокаКомментария = "";

	Для Каждого ТекущийРемонт Из Объект.РемонтыОборудования Цикл
		СтрокаКомментария = СтрокаКомментария + " " + Строка(Объект.Ссылка) + " " + ТекущийРемонт.ОбъектРемонта + " " + 
		ТекущийРемонт.ВидРемонтныхРабот + " " + ТекущийРемонт.ДатаНачала + " - " + 
		ТекущийРемонт.ДатаОкончания;

		МассивID.Добавить(ТекущийРемонт.ID);
	КонецЦикла;

	МассивСтрокНоменклатуры = СформироватьМассивНоменклатурыКСписаниюНаСервере(МассивID);
	Если МассивСтрокНоменклатуры = Неопределено Тогда

		// Значение неопределено, когда вся имеющаяся в заявке номенклатура уже есть в документе заказ на вн потребл.			
		МассивСтрокНоменклатуры = Новый Массив;
	КонецЕсли;

	СтруктураПередаваемыхВОбработчикПараметров.Вставить("МассивID", МассивID);

	ЗначенияЗаполнения = Новый Структура;
	ЗначенияЗаполнения.Вставить("Организация", Объект.Организация);
	ЗначенияЗаполнения.Вставить("Подразделение", Объект.Подразделение);
	ЗначенияЗаполнения.Вставить("Ответственный", Объект.Ответственный);
	ЗначенияЗаполнения.Вставить("КлючНазначенияИспользования", "СОЗДАНИЕ_ПОТРЕБЛЕНИЯ_ИЗ_ТОИР");
	ЗначенияЗаполнения.Вставить("МассивСтрокНоменклатуры",МассивСтрокНоменклатуры);
	ЗначенияЗаполнения.Вставить("Комментарий" , СтрокаКомментария);
	#Вставка
	//++ Проф-ИТ, #447, Соловьев А.А., 30.01.2024
	ЗначенияЗаполнения.Вставить("СозданиеНаОснованииАктаОВыполненииЭтапаРабот", Истина);
	//-- Проф-ИТ, #447, Соловьев А.А., 30.01.2024
	//++ Проф-ИТ, #451, Соловьев А.А., 05.02.2024
	ЗначенияЗаполнения.Вставить("Дата", Объект.Дата);
	//-- Проф-ИТ, #451, Соловьев А.А., 05.02.2024
	#КонецВставки
	СтруктураПараметров = Новый Структура("ЗначенияЗаполнения", ЗначенияЗаполнения);
	СтруктураПараметров.Вставить("ID", МассивID);

	ОписаниеОповещения = Новый ОписаниеОповещения("ДобавитьИзменитьЗаписиВРегистреТоро_ИнтеграцияДокументовСводный",ЭтаФорма,СтруктураПередаваемыхВОбработчикПараметров);
	ОткрытьФорму("Документ.ВнутреннееПотреблениеТоваров.Форма.ФормаДокумента", СтруктураПараметров, ЭтаФорма,,,,ОписаниеОповещения);

КонецПроцедуры

&НаСервере
&ИзменениеИКонтроль("ДобавитьЗапчастьИзПодбора")
Процедура проф_ДобавитьЗапчастьИзПодбора(Адрес, СтруктураСтроки, ИмяТЧ, ИмяРеквизита)

	РемонтыОборудования_ID = СтруктураСтроки.РемонтыОборудования_ID;

	Тз = ПолучитьИзВременногоХранилища(Адрес);

	СтруктураДействий = Новый Структура;
	Для каждого текСтрока из Тз Цикл

		СтруктураПоиска = Новый Структура("РемонтыОборудования_ID, Номенклатура, ХарактеристикаНоменклатуры, СерияНоменклатуры", 
		РемонтыОборудования_ID, текСтрока.Номенклатура, текСтрока.Характеристика, текСтрока.Серия);

		НайС = Объект[ИмяТЧ].НайтиСтроки(СтруктураПоиска);
		КоэфУпаковкиВыбр = ?(ЗначениеЗаполнено(текСтрока.Упаковка), текСтрока.Упаковка.Коэффициент, 1);
		Если НайС.Количество() = 0 Тогда
			нс = Объект[ИмяТЧ].Добавить();
			ЗаполнитьЗначенияСвойств(нс, текСтрока);
			нс[ИмяРеквизита] = текСтрока.Номенклатура;

			нс.ЕдиницаИзмерения = текСтрока.Упаковка;
			нс.Количество = текСтрока.КоличествоУпаковок * ?(ЗначениеЗаполнено(нс.Единицаизмерения), 1, КоэфУпаковкиВыбр);
			нс.СерияНоменклатуры = текСтрока.Серия;
			нс.РемонтыОборудования_ID = РемонтыОборудования_ID;
			нс.ID = РемонтыОборудования_ID;
			нс.ХарактеристикаНоменклатуры = текСтрока.Характеристика;
			#Удаление
			нс.СтатусДвиженияСерийныхЗЧ = Перечисления.торо_ВидыДвиженияСерийныхЗЧ.ВозвратЗамена;
			#КонецУдаления
			#Вставка
			//++ Проф-ИТ, #480, Соловьев А.А., 07.02.2024
			нс.СтатусДвиженияСерийныхЗЧ = Перечисления.торо_ВидыДвиженияСерийныхЗЧ.Списание;
			//-- Проф-ИТ, #480, Соловьев А.А., 07.02.2024
			#КонецВставки
			нс.СтатусДвиженияНовойЗЧ = Перечисления.торо_ВидыДвиженияСерийныхЗЧ.Установка;
			нс.КоличествоНовое = 1;
		Иначе

			нс = НайС[0];

			КоэфУпаковкиСтар = ?(ЗначениеЗаполнено(нс.ЕдиницаИзмерения), нс.ЕдиницаИзмерения.Коэффициент, 1);
			КоличествоЕдиниц = (нс.Количество*КоэфУпаковкиСтар + текСтрока.КоличествоУпаковок * КоэфУпаковкиВыбр);
			нс.Количество = КоличествоЕдиниц / КоэфУпаковкиВыбр;
			нс.ЕдиницаИзмерения = текСтрока.Упаковка;

		КонецЕсли;

	КонецЦикла;

КонецПроцедуры

//++ Проф-ИТ, #515, Соловьев А.А., 27.02.2024
&НаСервереБезКонтекста
Функция ТекущиеДанныеОтменил(Отменено)
	
	Возврат ?(Отменено, ПараметрыСеанса.ТекущийПользователь, Справочники.Пользователи.ПустаяСсылка());
	
КонецФункции
//-- Проф-ИТ, #515, Соловьев А.А., 27.02.2024

//++ Проф-ИТ, #523, Соловьев А.А., 13.03.2024
&НаКлиенте
Процедура ПриЗакрытииФормыВыбораВнутреннегоПотребления(Значение, ДополнительныеПараметры) Экспорт
	
	Если Значение = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ВнутреннееПотребление = Значение;
	
	Если Не Объект.Ссылка.Пустая() И НужноСоздатьИсправление(ВнутреннееПотребление) Тогда 
		
		ДополнительныеПараметры = Новый Структура("ВнутреннееПотребление", ВнутреннееПотребление);
		ОписаниеОповещения = Новый ОписаниеОповещения("НужноСоздатьИсправлениеВопрос", ЭтотОбъект, ДополнительныеПараметры);
		ТекстВопроса = НСтр("ru = 'Данные в Акте не соответствуют данным в документе Внутреннее потребление товаров. Ввести документ Исправление?'");
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, РежимДиалогаВопрос.ОКОтмена);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура НужноСоздатьИсправлениеВопрос(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = КодВозвратаДиалога.ОК Тогда
		
		ЭтотОбъект["проф_ДокументДляИсправления"] = ДополнительныеПараметры.ВнутреннееПотребление;
		проф_ДобавитьВнутреннееПотреблениеПослеВопроса();
		
	КонецЕсли; 
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция НужноСоздатьИсправление(ВнутреннееПотребление)
	
	ВнутреннееПотреблениеОбъект = ВнутреннееПотребление.ПолучитьОбъект();
	
	ДокументНаходитсяВЗакрытомПериоде = ДатыЗапретаИзменения.ИзменениеЗапрещено(ВнутреннееПотреблениеОбъект);
	
	Возврат ДокументНаходитсяВЗакрытомПериоде;
	
КонецФункции

&НаСервере
Функция МассивДокументовВП()
	
	Возврат ДокументыВнутреннееПотребление.Выгрузить(, "ВнутреннееПотребление").ВыгрузитьКолонку("ВнутреннееПотребление");
	
КонецФункции
//-- Проф-ИТ, #523, Соловьев А.А., 13.03.2024

#КонецОбласти
