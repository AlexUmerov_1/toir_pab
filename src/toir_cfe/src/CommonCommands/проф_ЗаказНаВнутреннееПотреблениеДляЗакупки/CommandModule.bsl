
#Область ОбработчикиСобытийФормы
	
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	//++ Проф-ИТ, #73, Соловьев А.А., 01.09.2023
	ПараметрыФормы = Новый Структура("проф_ЗаказДляЗакупки", Истина);
	ОткрытьФорму("Документ.ЗаказНаВнутреннееПотребление.ФормаСписка",
		//++ Проф-ИТ, #73, Карпов Д. Ю., 25.09.2023. Выводится не приятная ситуация, когда пользователи открывают типовую форму
		//Затем при вызове нашей команды вновь перекидывает на типовую форму, в связи с этим не корректно создаётся документ
		//ПараметрыФормы, ПараметрыВыполненияКоманды.Источник, ПараметрыВыполненияКоманды.Уникальность,
		ПараметрыФормы, ПараметрыВыполненияКоманды.Источник, "проф_ЗаказНаВнутреннееПотреблениеДляЗакупки",
		//-- Проф-ИТ, #73, Карпов Д. Ю., 25.09.2023
		ПараметрыВыполненияКоманды.Окно, ПараметрыВыполненияКоманды.НавигационнаяСсылка);
	//-- Проф-ИТ, #73, Соловьев А.А., 01.09.2023	
КонецПроцедуры

#КонецОбласти