
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура проф_ПриСозданииНаСервереВместо(Отказ, СтандартнаяОбработка)
	
	//++ Проф-ИТ, #351, Соловьев А.А., 17.11.2023
	ДобавляемыеРеквизиты = Новый Массив;
	НовыйРеквизит = Новый РеквизитФормы("Аналог", Новый ОписаниеТипов("СправочникСсылка.Номенклатура")); 
	ДобавляемыеРеквизиты.Добавить(НовыйРеквизит);
	НовыйРеквизит = Новый РеквизитФормы("ХарактеристикаАналога", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры")); 
	ДобавляемыеРеквизиты.Добавить(НовыйРеквизит);
	ИзменитьРеквизиты(ДобавляемыеРеквизиты);
	//-- Проф-ИТ, #351, Соловьев А.А., 17.11.2023
	
	//++ Проф-ИТ, #174, Соловьев А.А., 27.10.2023
	ЗаменитьЗапросыДинамическихСписков(Параметры);
	
	УстановитьУсловноеОформление();
	
	Номенклатура 				= Параметры.Номенклатура;
	Упаковка					= Параметры.ЕдиницаИзмерения;
	КоличествоУпаковок			= Параметры.Количество;
	Количество					= Параметры.КоличествоЕдиниц;
	ИсходноеКоличество			= КоличествоУпаковок;
	ИсходнаяУпаковка			= Упаковка;
	ИсходнаяЕдиницаИзмерения	= ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Номенклатура, "ЕдиницаИзмерения");
	ИсходноеКоличествоЕд		= Количество;
	СтраховойЗапас 				= (Параметры.Свойство("СтраховойЗапас"));
	
	НаборУпаковок = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Номенклатура, "ЕдиницаИзмерения");
	
	Элементы.Упаковка.Видимость			= ЗначениеЗаполнено(НаборУпаковок);
	Элементы.ЕдиницаИзмерения.Видимость = Не ЗначениеЗаполнено(НаборУпаковок);
	
	ПараметрыВыбораУпаковки = Новый Структура("Номенклатура", Номенклатура);
	
	ДанныеВыбора = ПолучитьДанныеВыбора(Тип("СправочникСсылка.УпаковкиНоменклатуры"), ПараметрыВыбораУпаковки);
	
	СписокВыбора = Элементы.Упаковка.СписокВыбора;
	
	Для Каждого ЗначениеВыбора из ДанныеВыбора Цикл
		СписокВыбора.Добавить(ЗначениеВыбора.Значение, ЗначениеВыбора.Представление);
	КонецЦикла;
	
	Элементы.МатериалыХарактеристика.Видимость = ПолучитьФункциональнуюОпцию(
													"торо_ИспользоватьХарактеристикиНоменклатуры");
	
	ОткрытиеФормы = Истина;
	//-- Проф-ИТ, #174, Соловьев А.А., 27.10.2023
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

//++ Проф-ИТ, #174, Соловьев А.А., 27.10.2023

&НаСервере
Процедура ЗаменитьЗапросыДинамическихСписков(Параметры)
	
	ТекущаяДата = ТекущаяДатаСеанса();
	
	ID							= Параметры.ID;
	ID_Ремонт					= Параметры.РемонтыОборудования_ID;
	Номенклатура				= Параметры.Номенклатура;
	Характеристика				= Параметры.ХарактеристикаНоменклатуры;
	ОбъектРемонта				= Параметры.ОбъектРемонта;
	Подразделение				= Параметры.ПодразделениеИсполнитель;
	ВидРемонта					= Параметры.ВидРемонта;
	ТехКарта					= Параметры.ТехКарта;
	СтраховойЗапас 				= (Параметры.Свойство("СтраховойЗапас"));
	Количество					= Параметры.КоличествоЕдиниц;
	
	Если СтраховойЗапас Тогда 
		Склад					= Параметры.Склад;
	Иначе
		Склад					= Справочники.Склады.ПустаяСсылка();
	КонецЕсли;
	
	Если СтраховойЗапас И Не ЗначениеЗаполнено(Склад) Тогда 
		ТекстСообщения = НСтр("ru = 'Не выбран склад для отображения остатков'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
	КонецЕсли;
	
	ШаблонЗаголовка = Нстр("ru = 'Аналоги материала ""%1"" (%2 %3)'");
	Заголовок = СтрШаблон(ШаблонЗаголовка, СокрЛП(Номенклатура), КоличествоУпаковок,
		?(ЗначениеЗаполнено(Упаковка), СокрЛП(Упаковка), ИсходнаяЕдиницаИзмерения));
	
	ПустаяСсылкаАналоги = Документы.торо_УстановкаАналоговНоменклатурыДляРемонтов.ПустаяСсылка();
	ДатаРемонта = ?(Параметры.ДатаРемонта = Неопределено, ТекущаяДата, Параметры.ДатаРемонта);;
	
	Если ТипЗнч(ОбъектРемонта) = Тип("СправочникСсылка.торо_ОбъектыРемонта") Тогда
		РеквизитыОР = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ОбъектРемонта, "Организация, Направление");
		ОрганизацияОР = РеквизитыОР.Организация;
		НаправлениеОР = РеквизитыОР.Направление;
	Иначе
		ОрганизацияОР = Справочники.Организации.ПустаяСсылка();
		НаправлениеОР = Справочники.торо_НаправленияОбъектовРемонтныхРабот.ПустаяСсылка();
	КонецЕсли;
	
	СтрПодразделенияСлужбыТОИР = "ПодразделенияСлужбыТОИР";
	ПодразделенияСлужбыТОИР = Справочники.проф_НастройкиСистемы.ПолучитьСпЗначНастройкиСистемы(
		"Подразделения", СтрПодразделенияСлужбыТОИР);
	
	СтруктураСвойствСписка = ОбщегоНазначения.СтруктураСвойствДинамическогоСписка();
	СтруктураСвойствСписка.ТекстЗапроса = ТекстЗапросаСписок();
	ОбщегоНазначения.УстановитьСвойстваДинамическогоСписка(Элементы.Список, СтруктураСвойствСписка);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "Дата",						Дата(1, 1, 1));
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ДатаРемонта",				ДатаРемонта);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "Материал",					Номенклатура);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ХарактеристикаМатериала",	Характеристика);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "КоличествоПоЗаказу",		Количество);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ОбъектРемонта", 			ОбъектРемонта);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "НаправлениеОбъектаРемонта", НаправлениеОР);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "Организация", 				ОрганизацияОР);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ПодразделениеИсполнитель",	Подразделение);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ТехКарта", 					ТехКарта);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Список, "ВидРемонта", 				ВидРемонта);
	
	СтруктураСвойствСписка.ТекстЗапроса = ТекстЗапросаМатериалы();
	ОбщегоНазначения.УстановитьСвойстваДинамическогоСписка(Элементы.Материалы, СтруктураСвойствСписка);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы,
																СтрПодразделенияСлужбыТОИР, ПодразделенияСлужбыТОИР);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "Номенклатура",	Номенклатура);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "Характеристика", Характеристика);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "ТекущаяДата",	ТекущаяДата);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "Склад",			Склад);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "Дата",			ТекущаяДата);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "Ссылка",			ПустаяСсылкаАналоги);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "Кратность",		1);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "СтраховойЗапас", СтраховойЗапас);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "Материал", 		Номенклатура);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "ХарактеристикаМатериала", Характеристика);
	
	СтруктураСвойствСписка.ТекстЗапроса = ТекстЗапросаАналоги();
	ОбщегоНазначения.УстановитьСвойстваДинамическогоСписка(Элементы.Аналоги, СтруктураСвойствСписка);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги,
															 СтрПодразделенияСлужбыТОИР, ПодразделенияСлужбыТОИР);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "Номенклатура",		Номенклатура);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "Характеристика",	Характеристика);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "ТекущаяДата",		ТекущаяДата);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "Склад",			Склад);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "Дата",				ТекущаяДата);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "Ссылка",			ПустаяСсылкаАналоги);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "Кратность",		1);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "СтраховойЗапас",	СтраховойЗапас);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "Аналог", 			ЭтотОбъект["Аналог"]);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "ХарактеристикаАналога", ЭтотОбъект["ХарактеристикаАналога"]);
	
КонецПроцедуры

&НаСервере
Функция ТекстЗапросаМатериалы()
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	торо_АналогиНоменклатурыДляРемонтов.Регистратор КАК Ссылка,
	|	торо_АналогиНоменклатурыДляРемонтов.Материал КАК Номенклатура,
	|	торо_АналогиНоменклатурыДляРемонтов.ХарактеристикаМатериала КАК Характеристика,
	|	торо_АналогиНоменклатурыДляРемонтов.КоличествоМатериала КАК Количество,
	|	торо_АналогиНоменклатурыДляРемонтов.УпаковкаМатериала КАК Упаковка,
	|	ВЫБОР
	|		КОГДА ЕСТЬNULL(УпаковкиНоменклатуры.Коэффициент, 0) = 0
	|			ТОГДА 1
	|		ИНАЧЕ ЕСТЬNULL(УпаковкиНоменклатуры.Коэффициент, 0)
	|	КОНЕЦ КАК Коэффициент,
	|	СправочникНоменклатура.ИспользованиеХарактеристик В (ЗНАЧЕНИЕ(Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.ОбщиеДляВидаНоменклатуры), ЗНАЧЕНИЕ(Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.ИндивидуальныеДляНоменклатуры)) КАК ХарактеристикиИспользуются
	|ПОМЕСТИТЬ втУстановкаАналоговМатериалы
	|ИЗ
	|	РегистрСведений.торо_АналогиНоменклатурыДляРемонтов КАК торо_АналогиНоменклатурыДляРемонтов
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.УпаковкиНоменклатуры КАК УпаковкиНоменклатуры
	|		ПО торо_АналогиНоменклатурыДляРемонтов.УпаковкаМатериала = УпаковкиНоменклатуры.Ссылка
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СправочникНоменклатура
	|		ПО торо_АналогиНоменклатурыДляРемонтов.Материал = СправочникНоменклатура.Ссылка
	|ГДЕ
	|	торо_АналогиНоменклатурыДляРемонтов.Регистратор = &Ссылка
	|	И торо_АналогиНоменклатурыДляРемонтов.Материал = &Материал
	|	И торо_АналогиНоменклатурыДляРемонтов.ХарактеристикаМатериала = &ХарактеристикаМатериала
	|	И торо_АналогиНоменклатурыДляРемонтов.Аналог = &Аналог
	|	И торо_АналогиНоменклатурыДляРемонтов.ХарактеристикаАналога = &ХарактеристикаАналога
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	ТоварыНаСкладахОстатки.Номенклатура КАК Номенклатура,
	|	ТоварыНаСкладахОстатки.Характеристика КАК Характеристика,
	|	ТоварыНаСкладахОстатки.ВНаличииОстаток КАК ВНаличии
	|ПОМЕСТИТЬ втТоварыНаСкладахПредв
	|ИЗ
	|	РегистрНакопления.проф_ТоварыНаСкладах.Остатки(
	|			&ТекущаяДата,
	|			&СтраховойЗапас
	|			И Склад В (&Склад)
	|				И (Номенклатура, Характеристика) В
	|					(ВЫБРАТЬ
	|						втУстановкаАналоговМатериалы.Номенклатура КАК Номенклатура,
	|						втУстановкаАналоговМатериалы.Характеристика КАК Характеристика
	|					ИЗ
	|						втУстановкаАналоговМатериалы КАК втУстановкаАналоговМатериалы)
	|				И Назначение = ЗНАЧЕНИЕ(Справочник.проф_Назначения.ПустаяСсылка)) КАК ТоварыНаСкладахОстатки
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	проф_ТоварыНаСкладахОстатки.Номенклатура,
	|	проф_ТоварыНаСкладахОстатки.Характеристика,
	|	проф_ТоварыНаСкладахОстатки.ВНаличииОстаток
	|ИЗ
	|	РегистрНакопления.проф_ТоварыНаСкладах.Остатки(
	|			&ТекущаяДата,
	|			&СтраховойЗапас
	|			И Склад В (&Склад)
	|				И (Номенклатура, Характеристика) В
	|					(ВЫБРАТЬ
	|						втУстановкаАналоговМатериалы.Номенклатура КАК Номенклатура,
	|						втУстановкаАналоговМатериалы.Характеристика КАК Характеристика
	|					ИЗ
	|						втУстановкаАналоговМатериалы КАК втУстановкаАналоговМатериалы)) КАК проф_ТоварыНаСкладахОстатки
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.проф_Назначения КАК проф_Назначения
	|		ПО проф_ТоварыНаСкладахОстатки.Назначение = проф_Назначения.Ссылка
	|			И (проф_Назначения.Подразделение В (&ПодразделенияСлужбыТОИР))
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	проф_ТоварыНаСкладахОстатки.Номенклатура,
	|	проф_ТоварыНаСкладахОстатки.Характеристика,
	|	проф_ТоварыНаСкладахОстатки.ВНаличииОстаток
	|ИЗ
	|	РегистрНакопления.проф_ТоварыНаСкладах.Остатки(
	|			&ТекущаяДата,
	|			НЕ &СтраховойЗапас
	|				И (Номенклатура, Характеристика) В
	|					(ВЫБРАТЬ
	|						втУстановкаАналоговМатериалы.Номенклатура КАК Номенклатура,
	|						втУстановкаАналоговМатериалы.Характеристика КАК Характеристика
	|					ИЗ
	|						втУстановкаАналоговМатериалы КАК втУстановкаАналоговМатериалы)) КАК проф_ТоварыНаСкладахОстатки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втТоварыНаСкладахПредв.Номенклатура КАК Номенклатура,
	|	втТоварыНаСкладахПредв.Характеристика КАК Характеристика,
	|	СУММА(втТоварыНаСкладахПредв.ВНаличии) КАК ВНаличии
	|ПОМЕСТИТЬ ВсеОстатки
	|ИЗ
	|	втТоварыНаСкладахПредв КАК втТоварыНаСкладахПредв
	|
	|СГРУППИРОВАТЬ ПО
	|	втТоварыНаСкладахПредв.Номенклатура,
	|	втТоварыНаСкладахПредв.Характеристика
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втУстановкаАналоговМатериалы.Ссылка КАК Ссылка,
	|	втУстановкаАналоговМатериалы.Номенклатура КАК Номенклатура,
	|	втУстановкаАналоговМатериалы.Характеристика КАК Характеристика,
	|	втУстановкаАналоговМатериалы.Количество * &Кратность КАК Количество,
	|	ВЫРАЗИТЬ(втУстановкаАналоговМатериалы.Количество * &Кратность / втУстановкаАналоговМатериалы.Коэффициент КАК ЧИСЛО(15, 3)) КАК КоличествоУпаковок,
	|	втУстановкаАналоговМатериалы.Упаковка КАК Упаковка,
	|	втУстановкаАналоговМатериалы.ХарактеристикиИспользуются КАК ХарактеристикиИспользуются,
	|	ВЫРАЗИТЬ(ЕСТЬNULL(ВсеОстатки.ВНаличии, 0) / втУстановкаАналоговМатериалы.Коэффициент КАК ЧИСЛО(15, 3)) КАК ВНаличииОстаток
	|ИЗ
	|	втУстановкаАналоговМатериалы КАК втУстановкаАналоговМатериалы
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВсеОстатки КАК ВсеОстатки
	|		ПО втУстановкаАналоговМатериалы.Номенклатура = ВсеОстатки.Номенклатура
	|			И втУстановкаАналоговМатериалы.Характеристика = ВсеОстатки.Характеристика";
		
	Возврат ТекстЗапроса;
	
КонецФункции

&НаСервере
Функция ТекстЗапросаАналоги()
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	торо_АналогиНоменклатурыДляРемонтов.Регистратор КАК Ссылка,
	|	торо_АналогиНоменклатурыДляРемонтов.Аналог КАК Номенклатура,
	|	торо_АналогиНоменклатурыДляРемонтов.ХарактеристикаАналога КАК Характеристика,
	|	торо_АналогиНоменклатурыДляРемонтов.КоличествоАналога КАК Количество,
	|	торо_АналогиНоменклатурыДляРемонтов.УпаковкаАналога КАК Упаковка,
	|	ВЫБОР
	|		КОГДА ЕСТЬNULL(УпаковкиНоменклатуры.Коэффициент, 0) = 0
	|			ТОГДА 1
	|		ИНАЧЕ ЕСТЬNULL(УпаковкиНоменклатуры.Коэффициент, 0)
	|	КОНЕЦ КАК Коэффициент,
	|	СправочникНоменклатура.ИспользованиеХарактеристик В (ЗНАЧЕНИЕ(Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.ОбщиеДляВидаНоменклатуры), ЗНАЧЕНИЕ(Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.ИндивидуальныеДляНоменклатуры)) КАК ХарактеристикиИспользуются
	|ПОМЕСТИТЬ втУстановкаАналоговАналоги
	|ИЗ
	|	РегистрСведений.торо_АналогиНоменклатурыДляРемонтов КАК торо_АналогиНоменклатурыДляРемонтов
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.УпаковкиНоменклатуры КАК УпаковкиНоменклатуры
	|		ПО торо_АналогиНоменклатурыДляРемонтов.УпаковкаАналога = УпаковкиНоменклатуры.Ссылка
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СправочникНоменклатура
	|		ПО торо_АналогиНоменклатурыДляРемонтов.Аналог = СправочникНоменклатура.Ссылка
	|ГДЕ
	|	торо_АналогиНоменклатурыДляРемонтов.Регистратор = &Ссылка
	|	И торо_АналогиНоменклатурыДляРемонтов.Аналог = &Аналог
	|	И торо_АналогиНоменклатурыДляРемонтов.ХарактеристикаАналога = &ХарактеристикаАналога
	|	И торо_АналогиНоменклатурыДляРемонтов.Материал = &Материал
	|	И торо_АналогиНоменклатурыДляРемонтов.ХарактеристикаМатериала = &ХарактеристикаМатериала
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	ТоварыНаСкладахОстатки.Номенклатура КАК Номенклатура,
	|	ТоварыНаСкладахОстатки.Характеристика КАК Характеристика,
	|	ТоварыНаСкладахОстатки.ВНаличииОстаток КАК ВНаличии
	|ПОМЕСТИТЬ втТоварыНаСкладахПредв
	|ИЗ
	|	РегистрНакопления.проф_ТоварыНаСкладах.Остатки(
	|			&ТекущаяДата,
	|			&СтраховойЗапас
	|				И Склад В (&Склад)
	|				И (Номенклатура, Характеристика) В
	|					(ВЫБРАТЬ
	|						втУстановкаАналоговАналоги.Номенклатура КАК Номенклатура,
	|						втУстановкаАналоговАналоги.Характеристика КАК Характеристика
	|					ИЗ
	|						втУстановкаАналоговАналоги КАК втУстановкаАналоговАналоги)
	|				И Назначение = ЗНАЧЕНИЕ(Справочник.проф_Назначения.ПустаяСсылка)) КАК ТоварыНаСкладахОстатки
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	проф_ТоварыНаСкладахОстатки.Номенклатура,
	|	проф_ТоварыНаСкладахОстатки.Характеристика,
	|	проф_ТоварыНаСкладахОстатки.ВНаличииОстаток
	|ИЗ
	|	РегистрНакопления.проф_ТоварыНаСкладах.Остатки(
	|			&ТекущаяДата,
	|			&СтраховойЗапас
	|				И Склад В (&Склад)
	|				И (Номенклатура, Характеристика) В
	|					(ВЫБРАТЬ
	|						втУстановкаАналоговАналоги.Номенклатура КАК Номенклатура,
	|						втУстановкаАналоговАналоги.Характеристика КАК Характеристика
	|					ИЗ
	|						втУстановкаАналоговАналоги КАК втУстановкаАналоговАналоги)) КАК проф_ТоварыНаСкладахОстатки
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.проф_Назначения КАК проф_Назначения
	|		ПО проф_ТоварыНаСкладахОстатки.Назначение = проф_Назначения.Ссылка
	|			И (проф_Назначения.Подразделение В (&ПодразделенияСлужбыТОИР))
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	проф_ТоварыНаСкладахОстатки.Номенклатура,
	|	проф_ТоварыНаСкладахОстатки.Характеристика,
	|	проф_ТоварыНаСкладахОстатки.ВНаличииОстаток
	|ИЗ
	|	РегистрНакопления.проф_ТоварыНаСкладах.Остатки(
	|			&ТекущаяДата,
	|			НЕ &СтраховойЗапас
	|				И (Номенклатура, Характеристика) В
	|					(ВЫБРАТЬ
	|						втУстановкаАналоговАналоги.Номенклатура КАК Номенклатура,
	|						втУстановкаАналоговАналоги.Характеристика КАК Характеристика
	|					ИЗ
	|						втУстановкаАналоговАналоги КАК втУстановкаАналоговАналоги)) КАК проф_ТоварыНаСкладахОстатки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втТоварыНаСкладахПредв.Номенклатура КАК Номенклатура,
	|	втТоварыНаСкладахПредв.Характеристика КАК Характеристика,
	|	СУММА(втТоварыНаСкладахПредв.ВНаличии) КАК ВНаличии
	|ПОМЕСТИТЬ ВсеОстатки
	|ИЗ
	|	втТоварыНаСкладахПредв КАК втТоварыНаСкладахПредв
	|
	|СГРУППИРОВАТЬ ПО
	|	втТоварыНаСкладахПредв.Номенклатура,
	|	втТоварыНаСкладахПредв.Характеристика
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втУстановкаАналоговАналоги.Ссылка КАК Ссылка,
	|	втУстановкаАналоговАналоги.Номенклатура КАК Номенклатура,
	|	втУстановкаАналоговАналоги.Характеристика КАК Характеристика,
	|	втУстановкаАналоговАналоги.Количество * &Кратность КАК Количество,
	|	ВЫРАЗИТЬ(втУстановкаАналоговАналоги.Количество * &Кратность / втУстановкаАналоговАналоги.Коэффициент КАК ЧИСЛО(15, 3)) КАК КоличествоУпаковок,
	|	втУстановкаАналоговАналоги.Упаковка КАК Упаковка,
	|	втУстановкаАналоговАналоги.ХарактеристикиИспользуются КАК ХарактеристикиИспользуются,
	|	ВЫРАЗИТЬ(ЕСТЬNULL(ВсеОстатки.ВНаличии, 0) / втУстановкаАналоговАналоги.Коэффициент КАК ЧИСЛО(15, 3)) КАК ВНаличииОстаток
	|ИЗ
	|	втУстановкаАналоговАналоги КАК втУстановкаАналоговАналоги
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВсеОстатки КАК ВсеОстатки
	|		ПО втУстановкаАналоговАналоги.Номенклатура = ВсеОстатки.Номенклатура
	|			И втУстановкаАналоговАналоги.Характеристика = ВсеОстатки.Характеристика";
	
	Возврат ТекстЗапроса;
	
КонецФункции

//-- Проф-ИТ, #174, Соловьев А.А., 27.10.2023

//++ Проф-ИТ, #351, Соловьев А.А., 15.11.2023

&НаСервере
Функция ТекстЗапросаСписок()
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.Ссылка КАК Ссылка,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.ВерсияДанных КАК ВерсияДанных,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.ПометкаУдаления КАК ПометкаУдаления,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.Номер КАК Номер,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.Дата КАК Дата,
	|	ВЫБОР
	|		КОГДА ТИПЗНАЧЕНИЯ(&ОбъектРемонта) = ТИП(Справочник.торо_СписокОбъектовРегламентногоМероприятия)
	|			ТОГДА торо_УстановкаАналоговНоменклатурыДляРемонтов.СписокОбъектовРМ
	|		ИНАЧЕ торо_УстановкаАналоговНоменклатурыДляРемонтов.ОбъектРемонта
	|	КОНЕЦ КАК ОбъектРемонта,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.Проведен КАК Проведен,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.ВидРемонта КАК ВидРемонта,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.ТехКарта КАК ТехКарта,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.НаправлениеОбъектаРемонта КАК НаправлениеОбъектаРемонта,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.ДатаНачалаДействия КАК ДатаНачалаДействия,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.ДатаОкончанияДействия КАК ДатаОкончанияДействия,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.Организация КАК Организация,
	|	ВЫРАЗИТЬ(торо_УстановкаАналоговНоменклатурыДляРемонтов.УказаниеПоПрименению КАК СТРОКА(100)) КАК УказаниеПоПрименению,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.Ответственный КАК Ответственный,
	|	торо_УстановкаАналоговНоменклатурыДляРемонтов.Подразделение КАК Подразделение,
	|	ИСТИНА КАК ПростаяЗамена,
	|	АналогиВПроизводстве.Регистратор КАК Регистратор,
	|	АналогиВПроизводстве.НомерСтроки КАК НомерСтроки,
	|	АналогиВПроизводстве.Аналог КАК Аналог,
	|	АналогиВПроизводстве.ХарактеристикаАналога КАК ХарактеристикаАналога,
	|	АналогиВПроизводстве.УпаковкаАналога КАК УпаковкаАналога,
	|	АналогиВПроизводстве.КоличествоАналога * (ВЫРАЗИТЬ(&КоличествоПоЗаказу / АналогиВПроизводстве.КоличествоМатериала - 0.5 КАК ЧИСЛО(15, 0))) КАК КоличествоАналога,
	|	АналогиВПроизводстве.КоличествоУпаковокАналога * (ВЫРАЗИТЬ(&КоличествоПоЗаказу / АналогиВПроизводстве.КоличествоМатериала - 0.5 КАК ЧИСЛО(15, 0))) КАК КоличествоУпаковокАналога,
	|	АналогиВПроизводстве.КоличествоМатериала КАК КоличествоМатериалаПоДокументу,
	|	ВЫБОР
	|		КОГДА &КоличествоПоЗаказу = АналогиВПроизводстве.КоличествоМатериала * (ВЫРАЗИТЬ(&КоличествоПоЗаказу / АналогиВПроизводстве.КоличествоМатериала - 0.5 КАК ЧИСЛО(15, 0)))
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ КАК КоличестваНеДостаточноДляЗамены,
	|	АналогиВПроизводстве.УпаковкаМатериала КАК УпаковкаМатериала,
	|	АналогиВПроизводстве.КоличествоУпаковокМатериала КАК КоличествоУпаковокМатериала,
	|	АналогиВПроизводстве.Материал.ЕдиницаИзмерения КАК МатериалЕдиницаИзмерения,
	|	АналогиВПроизводстве.Аналог.ЕдиницаИзмерения КАК АналогЕдиницаИзмерения
	|ИЗ
	|	РегистрСведений.торо_АналогиНоменклатурыДляРемонтов КАК АналогиВПроизводстве
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.торо_УстановкаАналоговНоменклатурыДляРемонтов КАК торо_УстановкаАналоговНоменклатурыДляРемонтов
	|		ПО АналогиВПроизводстве.Регистратор = торо_УстановкаАналоговНоменклатурыДляРемонтов.Ссылка
	|ГДЕ
	|	АналогиВПроизводстве.Материал = &Материал
	|	И АналогиВПроизводстве.ХарактеристикаМатериала = &ХарактеристикаМатериала
	|	И АналогиВПроизводстве.Период <= &ДатаРемонта
	|	И ВЫБОР
	|			КОГДА АналогиВПроизводстве.ПериодЗавершения = ДАТАВРЕМЯ(1, 1, 1)
	|				ТОГДА ИСТИНА
	|			ИНАЧЕ &ДатаРемонта <= АналогиВПроизводстве.ПериодЗавершения
	|		КОНЕЦ
	|	И АналогиВПроизводстве.ОбъектРемонта В (&ОбъектРемонта, ЗНАЧЕНИЕ(Справочник.торо_ОбъектыРемонта.ПустаяСсылка), ЗНАЧЕНИЕ(Справочник.торо_СписокОбъектовРегламентногоМероприятия.ПустаяСсылка))
	|	И АналогиВПроизводстве.НаправлениеОбъектаРемонта В (&НаправлениеОбъектаРемонта, ЗНАЧЕНИЕ(Справочник.торо_НаправленияОбъектовРемонтныхРабот.ПустаяСсылка))
	|	И АналогиВПроизводстве.Организация В (&Организация, ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка))
	|	И АналогиВПроизводстве.Подразделение В (&ПодразделениеИсполнитель, ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка))
	|	И АналогиВПроизводстве.ТехКарта В (&ТехКарта, ЗНАЧЕНИЕ(Справочник.торо_ИдентификаторыТехКарт.ПустаяСсылка))
	|	И АналогиВПроизводстве.ВидРемонта В (&ВидРемонта, ЗНАЧЕНИЕ(Справочник.торо_ВидыРемонтов.ПустаяСсылка))";
	
	Возврат ТекстЗапроса;
	
КонецФункции

&НаКлиенте
&Вместо("ОбработчикАктивизацииСтрокиСписок")
Процедура проф_ОбработчикАктивизацииСтрокиСписок()
		
	ТекущиеДанные = Элементы.Список.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Кратность = КратностьЗамены(Количество, ТекущиеДанные.КоличествоМатериалаПоДокументу);
	
	Если ТекСсылка <> ТекущиеДанные.Ссылка или ТекКратность <> Кратность Тогда
		
		ТекСсылка = ТекущиеДанные.Ссылка;
		ТекКратность = Кратность;
		Аналог = ТекущиеДанные.Аналог;
		ХарактеристикаАналога = ТекущиеДанные.ХарактеристикаАналога;
		
		УстановитьПараметрыДинамическихСписковНаСервере();
		
		ОбновитьПредставлениеОбластиДействия(ТекущиеДанные);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
&После("УстановитьПараметрыДинамическихСписковНаСервере")
Процедура проф_УстановитьПараметрыДинамическихСписковНаСервере(КоличествоПоЗаказу)
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "Материал", Номенклатура);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "ХарактеристикаМатериала", Характеристика);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "Аналог", ЭтотОбъект["Аналог"]);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Материалы, "ХарактеристикаАналога", ЭтотОбъект["ХарактеристикаАналога"]);
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "Аналог", ЭтотОбъект["Аналог"]);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "ХарактеристикаАналога", ЭтотОбъект["ХарактеристикаАналога"]);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "Материал", Номенклатура);
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(Аналоги, "ХарактеристикаМатериала", Характеристика);
	
КонецПроцедуры

&НаСервере
&Вместо("ЗначениеВыбораНаСервере")
Функция проф_ЗначениеВыбораНаСервере(Ссылка, КоличествоПоДокументу, Идентификатор)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	&ID КАК ID,
	|	&РемонтыОборудования_ID КАК РемонтыОборудования_ID,
	|	торо_АналогиНоменклатурыДляРемонтов.Материал КАК Материал,
	|	торо_АналогиНоменклатурыДляРемонтов.ХарактеристикаМатериала КАК ХарактеристикаМатериала,
	|	торо_АналогиНоменклатурыДляРемонтов.Аналог КАК Аналог,
	|	торо_АналогиНоменклатурыДляРемонтов.ХарактеристикаАналога КАК ХарактеристикаАналога,
	|	торо_АналогиНоменклатурыДляРемонтов.УпаковкаМатериала КАК УпаковкаМатериала,
	|	торо_АналогиНоменклатурыДляРемонтов.КоличествоМатериала КАК КоличествоМатериала,
	|	торо_АналогиНоменклатурыДляРемонтов.УпаковкаАналога КАК УпаковкаАналога,
	|	торо_АналогиНоменклатурыДляРемонтов.КоличествоАналога КАК КоличествоАналога
	|ПОМЕСТИТЬ втДанныеРегистраАналоги
	|ИЗ
	|	РегистрСведений.торо_АналогиНоменклатурыДляРемонтов КАК торо_АналогиНоменклатурыДляРемонтов
	|ГДЕ
	|	торо_АналогиНоменклатурыДляРемонтов.Регистратор = &Ссылка
	|	И торо_АналогиНоменклатурыДляРемонтов.Материал = &Материал
	|	И торо_АналогиНоменклатурыДляРемонтов.Аналог = &Аналог
	|	И торо_АналогиНоменклатурыДляРемонтов.ХарактеристикаМатериала = &ХарактеристикаМатериала
	|	И торо_АналогиНоменклатурыДляРемонтов.ХарактеристикаАналога = &ХарактеристикаАналога
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Материалы.ID КАК ID,
	|	Материалы.РемонтыОборудования_ID КАК РемонтыОборудования_ID,
	|	Материалы.Материал КАК Номенклатура,
	|	Материалы.ХарактеристикаМатериала КАК ХарактеристикаНоменклатуры,
	|	Материалы.КоличествоМатериала * &Кратность КАК Количество,
	|	ВЫРАЗИТЬ(Материалы.КоличествоМатериала * &Кратность / ЕСТЬNULL(УпаковкиНоменклатуры.Коэффициент, 1) КАК ЧИСЛО(15, 3)) КАК КоличествоУпаковок,
	|	Материалы.УпаковкаМатериала КАК Упаковка
	|ИЗ
	|	втДанныеРегистраАналоги КАК Материалы
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.УпаковкиНоменклатуры КАК УпаковкиНоменклатуры
	|		ПО Материалы.УпаковкаМатериала = УпаковкиНоменклатуры.Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Аналоги.ID КАК ID,
	|	Аналоги.РемонтыОборудования_ID КАК РемонтыОборудования_ID,
	|	Аналоги.Аналог КАК Номенклатура,
	|	Аналоги.ХарактеристикаАналога КАК ХарактеристикаНоменклатуры,
	|	Аналоги.КоличествоАналога * &Кратность КАК КоличествоЕдиниц,
	|	ВЫРАЗИТЬ(Аналоги.КоличествоАналога * &Кратность / ЕСТЬNULL(УпаковкиНоменклатуры.Коэффициент, 1) КАК ЧИСЛО(15, 3)) КАК Количество,
	|	Аналоги.УпаковкаАналога КАК ЕдиницаИзмерения,
	|	СправочникНоменклатура.ИспользованиеХарактеристик В (ЗНАЧЕНИЕ(Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.ОбщиеДляВидаНоменклатуры), ЗНАЧЕНИЕ(Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.ИндивидуальныеДляНоменклатуры)) КАК ХарактеристикиИспользуются
	|ИЗ
	|	втДанныеРегистраАналоги КАК Аналоги
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СправочникНоменклатура
	|		ПО Аналоги.Аналог = СправочникНоменклатура.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.УпаковкиНоменклатуры КАК УпаковкиНоменклатуры
	|		ПО Аналоги.УпаковкаАналога = УпаковкиНоменклатуры.Ссылка";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Запрос.УстановитьПараметр("Материал", ЭтотОбъект["Номенклатура"]);
	Запрос.УстановитьПараметр("ХарактеристикаМатериала", ЭтотОбъект["Характеристика"]);
	Запрос.УстановитьПараметр("Аналог", ЭтотОбъект["Аналог"]);
	Запрос.УстановитьПараметр("ХарактеристикаАналога", ЭтотОбъект["ХарактеристикаАналога"]);
	Запрос.УстановитьПараметр("Кратность", КратностьЗамены(Количество, КоличествоПоДокументу));
	Запрос.УстановитьПараметр("ID", ЭтотОбъект["ID"]);
	Запрос.УстановитьПараметр("РемонтыОборудования_ID", ЭтотОбъект["ID_Ремонт"]);
	
	Результат = Запрос.ВыполнитьПакет();
	
	ЗначениеВыбора = Новый Структура("Материалы, Аналоги");
	
	ЗначениеВыбора.Материалы = Результат[1].Выгрузить();
	ЗначениеВыбора.Аналоги = Результат[2].Выгрузить();
	
	Возврат ПоместитьВоВременноеХранилище(ЗначениеВыбора, Идентификатор);
	
КонецФункции

//-- Проф-ИТ, #351, Соловьев А.А., 15.11.2023

#КонецОбласти
