
//++ Проф-ИТ, #4, Башинская А., 13.10.2023	

#Область ОбработчикиСобытийФормы

&НаКлиенте
Процедура ФайлВыгрузкиНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ДиалогВыбора           = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.Открытие);
	ДиалогВыбора.Заголовок = "Выберите файл";
	Фильтр				   = "Табличный документ(*.xls; *.xlsx)|*.xls; *.xlsx"; 
	ДиалогВыбора.Фильтр    = Фильтр;	 
	Оповещение = Новый ОписаниеОповещения("ЗавершениеОткрытияФайла", ЭтотОбъект);
	ДиалогВыбора.Показать(Оповещение);

КонецПроцедуры 

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
КонецПроцедуры
 
#КонецОбласти

#Область ПрограммныйИнтерфейс

&НаКлиенте
Процедура ЗавершениеОткрытияФайла(ВыбранныеФайлы, ДополнительныеПараметры) Экспорт
	
	Если ВыбранныеФайлы = Неопределено Тогда
		Возврат;
	Иначе
		ФайлВыгрузки = ВыбранныеФайлы[0];  
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти  
 
#Область ОбработчикиКомандФормы
 
&НаКлиенте
Процедура Загрузить(Команда)
	
	ОчиститьСообщения();
	
	Если ПустаяСтрока(ФайлВыгрузки) Тогда
		
		Сообщение = Новый СообщениеПользователю;
		Сообщение.Текст = "Выберите файл!";
		Сообщение.Сообщить();
		Возврат; 
		
	КонецЕсли;	
	
	ЗагрузитьДанныеВТаблицу();
	
КонецПроцедуры

#КонецОбласти 
 
#Область СлужебныеПроцедурыИФункции 
 
&НаКлиенте
Процедура ЗагрузитьДанныеВТаблицу()
	 
	Адрес = ПоместитьВоВременноеХранилище(Новый ДвоичныеДанные(ФайлВыгрузки));
	ЗагрузитьТабличныйДокументИзФайла(Адрес, ФайлВыгрузки);
	 
КонецПроцедуры
 
&НаСервере
Процедура ЗагрузитьТабличныйДокументИзФайла(Знач Адрес, Знач ИмяФайла)
 
	Расширение = Прав(ИмяФайла, 4);
	Расширение = СтрЗаменить(Расширение, ".", "");

	ФайлПриемник = ПолучитьИмяВременногоФайла(Расширение);
	ДанныеХранилища = ПолучитьИзВременногоХранилища(Адрес);
	ДанныеХранилища.Записать(ФайлПриемник);

	ТабличныйДокумент.Очистить();
	ТабличныйДокумент.Прочитать(ФайлПриемник);
	
	// Удаляем временный файл
	Попытка
		УдалитьФайлы(ФайлПриемник);
	Исключение
		ОписаниеОшибки = ОбработкаОшибок.ПодробноеПредставлениеОшибки(ИнформацияОбОшибке());
		ЗаписьЖурналаРегистрации(НСтр("ru = 'Мой механизм.Действие'"),
			УровеньЖурналаРегистрации.Ошибка, , , ОписаниеОшибки);
	КонецПопытки;	

	ТЗ = ПреобразоватьТабличныйДокументВТаблицуЗначений(ТабличныйДокумент);
	
	ЗагрузкаПрервана = Ложь;
	ПроверитьСоставТаблицыПронумеровать(ТЗ, РезультатЗагрузки, ЗагрузкаПрервана);     
	Если ЗагрузкаПрервана Тогда
		Возврат;
	Иначе             
		ВыборкаДанные = ДополнитьДанныеИзБД(ТЗ, РезультатЗагрузки);
		СоздатьДокументы(ВыборкаДанные, РезультатЗагрузки);
	КонецЕсли;
	 
КонецПроцедуры
 
&НаСервере
Процедура ПроверитьСоставТаблицыПронумеровать(ТаблицаДанных, РезультатЗарузки, ЗагрузкаПрервана) 
	
	РезультатЗарузки = РезультатЗарузки + "ПРОВЕРКА СТРУКТУРЫ ФАЙЛА." + Символы.ПС;

	СообщениеРезультатЗарузки = "";
	Если ТаблицаДанных.Колонки.Найти("Номенклатура") = Неопределено Тогда
		СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + "Не найдена колонка ""Номенклатура"";" + Символы.ПС; 
	КонецЕсли;
	Если ТаблицаДанных.Колонки.Найти("КодНоменклатурыИзERP") = Неопределено Тогда
		СообщениеРезультатЗарузки = СообщениеРезультатЗарузки
			+ "Не найдена колонка ""Код номенклатуры из ERP"";" + Символы.ПС;
	КонецЕсли;
	Если ТаблицаДанных.Колонки.Найти("Характеристика") = Неопределено Тогда
		СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + "Не найдена колонка ""Характеристика"";" + Символы.ПС; 
	КонецЕсли;
	Если ТаблицаДанных.Колонки.Найти("Количество") = Неопределено Тогда
		СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + "Не найдена колонка ""Количество"";" + Символы.ПС; 
	КонецЕсли;	
	Если ТаблицаДанных.Колонки.Найти("НоменклатураАналог") = Неопределено Тогда
		СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + "Не найдена колонка ""НоменклатураАналог"";" + Символы.ПС; 
	КонецЕсли;
	Если ТаблицаДанных.Колонки.Найти("КодНоменклатуры_аналогаИзERP") = Неопределено Тогда
		СообщениеРезультатЗарузки = СообщениеРезультатЗарузки
			+ "Не найдена колонка ""Код номенклатуры-аналога из ERP"";" + Символы.ПС;
	КонецЕсли;
	Если ТаблицаДанных.Колонки.Найти("ХарактеристикаАналог") = Неопределено Тогда
		СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + "Не найдена колонка ""ХарактеристикаАналог"";" + Символы.ПС;
	КонецЕсли;
	Если ТаблицаДанных.Колонки.Найти("КоличествоАналог") = Неопределено Тогда
		СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + "Не найдена колонка ""КоличествоАналог"";" + Символы.ПС; 
	КонецЕсли;
	Если ТаблицаДанных.Колонки.Найти("УказаниеПоПрименению") = Неопределено Тогда
		СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + "Не найдена колонка ""Указание по применению"";" + Символы.ПС; 
	КонецЕсли;
	
	Если ЗначениеЗаполнено(СообщениеРезультатЗарузки) Тогда   
		РезультатЗарузки = СообщениеРезультатЗарузки + "ЗАГРУЗКА ПРЕРВАНА." + Символы.ПС; 
		ЗагрузкаПрервана = Истина;
		Возврат;
	КонецЕсли;
	
	// Удалим пустые строки или строки без заполненного объекта ремонта
	сч = ТаблицаДанных.Количество() - 1;
	Пока сч >= 0 Цикл
		Стр = ТаблицаДанных[сч];
		Если СокрЛП(Стр.КодНоменклатурыИзERP) = "" Тогда 
			ТаблицаДанных.Удалить(Стр);
		КонецЕсли;		
		сч = сч - 1;
	КонецЦикла;

	// Пронумеруем и нормализуем
	ТаблицаДанных.Колонки.Добавить("НомерСтроки", Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(3, 0)));
	
	сч = 1;
	Для Каждого Стр Из ТаблицаДанных Цикл 
		Стр.Номенклатура 				= СокрЛП(Стр.Номенклатура);
		Стр.КодНоменклатурыИзERP 		= СокрЛП(Стр.КодНоменклатурыИзERP);
		Стр.Характеристика 				= СокрЛП(Стр.Характеристика);
		Стр.Количество 					= СокрЛП(Стр.Количество);
		Стр.НоменклатураАналог   		= СокрЛП(Стр.НоменклатураАналог);
		Стр.КодНоменклатуры_аналогаИзERP= СокрЛП(Стр.КодНоменклатуры_аналогаИзERP);      
		Стр.ХарактеристикаАналог 	  	= СокрЛП(Стр.ХарактеристикаАналог);
		Стр.КоличествоАналог 			= СокрЛП(Стр.КоличествоАналог);
		Стр.УказаниеПоПрименению		= СокрЛП(Стр.УказаниеПоПрименению);
		
		Стр.НомерСтроки = сч;
		сч = сч + 1;
	КонецЦикла;  
	
КонецПроцедуры
 
&НаСервере
Функция ДополнитьДанныеИзБД(ТЗ, СообщениеРезультатЗарузки)
	
	СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + "АНАЛИЗ ДАННЫХ ФАЙЛА." + Символы.ПС;

	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
				|	ВнешнийИсточник.НомерСтроки КАК НомерСтроки,
				|	ВнешнийИсточник.Номенклатура КАК НоменклатураСтр,
				|	ВнешнийИсточник.КодНоменклатурыИзERP КАК КодНоменклатурыИзERP,
				|	ВнешнийИсточник.Характеристика КАК Характеристика,
				|	ВнешнийИсточник.Количество КАК Количество,
				|	ВнешнийИсточник.НоменклатураАналог КАК Номенклатура_аналог,
				|	ВнешнийИсточник.КодНоменклатуры_аналогаИзERP КАК КодНоменклатурыИзERP_аналог,  
				|	ВнешнийИсточник.КоличествоАналог КАК Количество_аналог,
				|	ВнешнийИсточник.УказаниеПоПрименению КАК УказаниеПоПрименению_аналог,	             
				|	ВнешнийИсточник.ХарактеристикаАналог КАК Характеристика_аналог
				|ПОМЕСТИТЬ ВнешнийИсточник
				|ИЗ
				|	&ТЗ КАК ВнешнийИсточник
				|;
				| 
				|////////////////////////////////////////////////////////////////////////////////	              
				|ВЫБРАТЬ
				|	ВнешнийИсточник.НомерСтроки КАК НомерСтроки,
				|	ВнешнийИсточник.НоменклатураСтр КАК НоменклатураСтр,
				|	ВнешнийИсточник.КодНоменклатурыИзERP КАК КодНоменклатурыИзERP,
				|	ВнешнийИсточник.Характеристика КАК ХарактеристикаСтр,   
				|	ВнешнийИсточник.УказаниеПоПрименению_аналог КАК УказаниеПоПрименению_аналог,
				|	ВнешнийИсточник.Характеристика_аналог КАК Характеристика_аналог,     				  
				|	ВнешнийИсточник.Количество КАК Количество,     
				|	ВнешнийИсточник.Номенклатура_аналог КАК Номенклатура_аналог,
				|	ВнешнийИсточник.КодНоменклатурыИзERP_аналог КАК КодНоменклатурыИзERP_аналог,
				|	ВнешнийИсточник.Количество_аналог КАК Количество_аналог,	              
				|	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ ЕСТЬNULL(Номенклатура.Ссылка, НЕОПРЕДЕЛЕНО)) КАК НоменклатураКолВо,
				|	МАКСИМУМ(ЕСТЬNULL(Номенклатура.Ссылка, НЕОПРЕДЕЛЕНО)) КАК Номенклатура,    
				|	МАКСИМУМ(ЕСТЬNULL(Номенклатура.ИспользованиеХарактеристик, Ложь)) КАК ИспользованиеХарактеристик,
				|	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ ЕСТЬNULL(Номенклатура2.Ссылка, НЕОПРЕДЕЛЕНО)) КАК НоменклатураАналогКолВо,
				|	МАКСИМУМ(ЕСТЬNULL(Номенклатура2.Ссылка, НЕОПРЕДЕЛЕНО)) КАК НоменклатураАналог,
				|	МАКСИМУМ(ЕСТЬNULL(Номенклатура2.ИспользованиеХарактеристик, Ложь)) КАК ИспользованиеХарактеристикАналог,
				|	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ ЕСТЬNULL(ХарактеристикиНоменклатуры.Ссылка, НЕОПРЕДЕЛЕНО)) КАК ХарактеристикаКолВо,
				|	МАКСИМУМ(ЕСТЬNULL(ХарактеристикиНоменклатуры.Ссылка, НЕОПРЕДЕЛЕНО)) КАК Характеристика,
				|	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ ЕСТЬNULL(ХарактеристикиНоменклатуры2.Ссылка, НЕОПРЕДЕЛЕНО)) КАК ХарактеристикаАналогКолВо,
				|	МАКСИМУМ(ЕСТЬNULL(ХарактеристикиНоменклатуры2.Ссылка, НЕОПРЕДЕЛЕНО)) КАК ХарактеристикаАналог,
				|	ЕСТЬNULL(Номенклатура.Ссылка, ИСТИНА) КАК Пропустить, 
				|	ЕСТЬNULL(Номенклатура2.Ссылка, ИСТИНА) КАК Пропустить2 						              
				|ИЗ
				|	ВнешнийИсточник КАК ВнешнийИсточник
				|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК Номенклатура
				|		ПО ВнешнийИсточник.НоменклатураСтр = Номенклатура.Наименование
				|			И ВнешнийИсточник.КодНоменклатурыИзERP = Номенклатура.Код
				|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК Номенклатура2
				|		ПО ВнешнийИсточник.Номенклатура_аналог = Номенклатура2.Наименование  
				|			И ВнешнийИсточник.КодНоменклатурыИзERP_аналог = Номенклатура2.Код
				|		ЛЕВОЕ СОЕДИНЕНИЕ 	Справочник.ХарактеристикиНоменклатуры КАК ХарактеристикиНоменклатуры
				|		ПО ВнешнийИсточник.Характеристика = ХарактеристикиНоменклатуры.Наименование
				|		ЛЕВОЕ СОЕДИНЕНИЕ 	Справочник.ХарактеристикиНоменклатуры КАК ХарактеристикиНоменклатуры2
				|		ПО ВнешнийИсточник.Характеристика_аналог = ХарактеристикиНоменклатуры2.Наименование
				|
				|СГРУППИРОВАТЬ ПО
				|	ВнешнийИсточник.НомерСтроки,  
				|	ЕСТЬNULL(Номенклатура.Ссылка, ИСТИНА),
				|	ЕСТЬNULL(Номенклатура2.Ссылка, ИСТИНА),	
				|	ВнешнийИсточник.НоменклатураСтр,
				|	ВнешнийИсточник.КодНоменклатурыИзERP,
				|	ВнешнийИсточник.Характеристика,   
				|	ВнешнийИсточник.УказаниеПоПрименению_аналог,
				|	ВнешнийИсточник.Характеристика_аналог,     				  
				|	ВнешнийИсточник.Количество,     
				|	ВнешнийИсточник.Номенклатура_аналог,
				|	ВнешнийИсточник.КодНоменклатурыИзERP_аналог,
				|	ВнешнийИсточник.Количество_аналог;";

	Запрос.УстановитьПараметр("ТЗ", ТЗ);
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		Пока Выборка.Следующий() Цикл		
			ПроверитьЗаполненностьДанных(Выборка, СообщениеРезультатЗарузки);						
		КонецЦикла;	 
		
	КонецЦикла;
	
	Выборка.Сбросить();
	
	Возврат Выборка;	
	
КонецФункции
 
&НаСервере
Процедура ПроверитьЗаполненностьДанных(Выборка, СообщениеРезультатЗарузки)

	СтрНайденоБолееОдногоЗначения = "%1В строке №%2 найдено более одного значения";
	СтрНеНайденоЗначение = "%1В строке №%2 не найдено значение";

	Если Выборка.НоменклатураКолВо > 1 Тогда				
		СообщениеРезультатЗарузки = СтрШаблон(СтрНайденоБолееОдногоЗначения + " для поля шаблона ""Номенклатура"".%3",
			СообщениеРезультатЗарузки, Выборка.НомерСтроки, Символы.ПС);
	КонецЕсли;  
	
	Если Выборка.Номенклатура = Неопределено Тогда				
		ТекстСообщения = СтрНеНайденоЗначение
			+ " обязательного поля шаблона ""Номенклатура"". Данные по строке не загружены.%3";
		СообщениеРезультатЗарузки = СтрШаблон(ТекстСообщения, СообщениеРезультатЗарузки, Выборка.НомерСтроки, Символы.ПС);
	КонецЕсли; 
	
	Если Выборка.НоменклатураАналогКолВо > 1 Тогда				
		СообщениеРезультатЗарузки = СтрШаблон(СтрНайденоБолееОдногоЗначения + " для поля шаблона ""Номенклатура аналог"".%3",
			СообщениеРезультатЗарузки, Выборка.НомерСтроки, Символы.ПС);
	КонецЕсли;  
	
	Если Выборка.НоменклатураАналог = Неопределено Тогда				
		СообщениеРезультатЗарузки = СтрШаблон(СтрНеНайденоЗначение + " обязательного поля шаблона ""Номенклатура аналог"".%3",
			СообщениеРезультатЗарузки, Выборка.НомерСтроки, Символы.ПС);
	КонецЕсли; 
	
	Если Выборка.ХарактеристикаКолВо > 1 Тогда				
		СообщениеРезультатЗарузки = СтрШаблон(СтрНайденоБолееОдногоЗначения + " для поля шаблона ""Характеристика"".%3",
			СообщениеРезультатЗарузки, Выборка.НомерСтроки, Символы.ПС);
	КонецЕсли;  
	
	Если Выборка.Характеристика = Неопределено
	И Выборка.Номенклатура <> Неопределено 
	И Выборка.ИспользованиеХарактеристик <> Перечисления.ВариантыИспользованияХарактеристикНоменклатуры.НеИспользовать Тогда				
		СообщениеРезультатЗарузки = СтрШаблон(СтрНеНайденоЗначение + " поля шаблона ""Характеристика"".%3",
			СообщениеРезультатЗарузки, Выборка.НомерСтроки, Символы.ПС);
	КонецЕсли; 
	
	Если Выборка.ХарактеристикаАналогКолВо > 1 Тогда				
		ТекстСообщения = СтрНайденоБолееОдногоЗначения + " для поля шаблона ""Характеристика аналог"".%3";
		СообщениеРезультатЗарузки = СтрШаблон(ТекстСообщения, СообщениеРезультатЗарузки, Выборка.НомерСтроки, Символы.ПС);
	КонецЕсли;  
	
	Если Выборка.ХарактеристикаАналог = Неопределено
	И Выборка.НоменклатураАналог <> Неопределено 
	И Выборка.ИспользованиеХарактеристикАналог <> Перечисления.ВариантыИспользованияХарактеристикНоменклатуры.НеИспользовать Тогда				
		СообщениеРезультатЗарузки = СтрШаблон(СтрНеНайденоЗначение +
		" поля шаблона ""Характеристика аналог"".%3",СообщениеРезультатЗарузки, Выборка.НомерСтроки, Символы.ПС); 
	КонецЕсли; 
		
	СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + Символы.ПС;	
	
КонецПроцедуры	
 
&НаСервере
Процедура СоздатьДокументы(ВыборкаДанные, СообщениеРезультатЗарузки) 
	
	СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + "СОЗДАНИЕ ДОКУМЕНТОВ." + Символы.ПС;
	
	Пока ВыборкаДанные.Следующий() Цикл
		
		Если ВыборкаДанные.Пропустить = Истина Или ВыборкаДанные.Пропустить2 = Истина Тогда
			Продолжить;
		КонецЕсли;
		
		Документ = Документы.торо_УстановкаАналоговНоменклатурыДляРемонтов.СоздатьДокумент();
		Документ.Дата = ТекущаяДатаСеанса();    
		Документ.ДатаНачалаДействия = ТекущаяДатаСеанса();  
		Документ.УстановитьНовыйНомер();
		Документ.проф_Статус = Перечисления.проф_СтатусыРазрешенийНаЗаменуМатериалов.Утверждено;   
		Документ.Ответственный = Пользователи.ТекущийПользователь();
		Документ.Автор = Пользователи.ТекущийПользователь();		
		
		НстрМатериалы = Документ.Материалы.Добавить();
		НстрМатериалы.Номенклатура = ВыборкаДанные.Номенклатура;	 
		НстрМатериалы.Характеристика = ВыборкаДанные.Характеристика;	 
		НстрМатериалы.Количество = ВыборкаДанные.Количество;	           
		НстрМатериалы.КоличествоУпаковок = ВыборкаДанные.Количество;	 
			
		НстрАналоги = Документ.Аналоги.Добавить();  
		НстрАналоги.Номенклатура = ВыборкаДанные.НоменклатураАналог;
		НстрАналоги.Характеристика = ВыборкаДанные.ХарактеристикаАналог;
		НстрАналоги.Количество = ВыборкаДанные.Количество_аналог;           
		НстрАналоги.КоличествоУпаковок = ВыборкаДанные.Количество_аналог;
		
		Если ЗначениеЗаполнено(ВыборкаДанные.УказаниеПоПрименению_аналог) Тогда 
			Документ.УказаниеПоПрименению = ВыборкаДанные.УказаниеПоПрименению_аналог;
		Иначе
			Документ.УказаниеПоПрименению = "Загружено обработкой из Excel";
		КонецЕсли;	
										
		Попытка
			Документ.Записать(РежимЗаписиДокумента.Проведение);
		Исключение
			СообщениеРезультатЗарузки = СтрШаблон("%1Произошла ошибка при записи документа по строке №%2." +
			" Данные по строке не загружены.%3", СообщениеРезультатЗарузки, ВыборкаДанные.НомерСтроки, Символы.ПС);    
			Продолжить;
		КонецПопытки;
		
		Если НстрМатериалы.Номенклатура = НстрАналоги.Номенклатура Тогда
			СообщениеРезультатЗарузки = СтрШаблон("%1В документе:%2 оригинал и аналог одинаковые!%3" ,
													СообщениеРезультатЗарузки, Документ.Ссылка, Символы.ПС);    
		КонецЕсли;
		Если НстрМатериалы.Номенклатура.ПометкаУдаления Или НстрАналоги.Номенклатура.ПометкаУдаления Тогда
			СообщениеРезультатЗарузки = СтрШаблон("%1В документе:%2 оригинал/аналог помечен на удаление!%3" ,
													СообщениеРезультатЗарузки, Документ.Ссылка, Символы.ПС);    
		КонецЕсли;
		
		СообщениеРезультатЗарузки = СтрШаблон("%1По строке №%2 создан документ %3.%4", 
													СообщениеРезультатЗарузки, ВыборкаДанные.НомерСтроки, Документ.Ссылка, Символы.ПС);    
			
	КонецЦикла;   
	
	СообщениеРезультатЗарузки = СообщениеРезультатЗарузки + Символы.ПС + "ЗАГРУЗКА ЗАВЕРШЕНА." + Символы.ПС;
	
КонецПроцедуры
 
&НаСервере
Функция ПреобразоватьТабличныйДокументВТаблицуЗначений(ТабДокумент)
 
	ПоследняяСтрока = ТабДокумент.ВысотаТаблицы;
	ПоследняяКолонка = ТабДокумент.ШиринаТаблицы;

	ОбластьЯчеек = ТабДокумент.Область(1, 1, ПоследняяСтрока, ПоследняяКолонка);

	// Создаем описание источника данных на основании области ячеек табличного документа.
	ИсточникДанных = Новый ОписаниеИсточникаДанных(ОбластьЯчеек);

	// Создаем объект для интеллектуального построения отчетов,
	// указываем источник данных и выполняем построение отчета.
	ПостроительОтчета = Новый ПостроительОтчета;
	ПостроительОтчета.ИсточникДанных = ИсточникДанных;
	ПостроительОтчета.Выполнить();

	// Результат выгружаем в таблицу значений.
	ТабЗначений = ПостроительОтчета.Результат.Выгрузить();

	Возврат ТабЗначений;
 
КонецФункции  
 
#КонецОбласти  

//-- Проф-ИТ, #4, Башинская А., 13.10.2023	