
//++ Проф-ИТ, #150, Соловьев А.А., 18.12.2023

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ТекущаяДата = ТекущаяДатаСеанса();
	ДатаЗаписи = НачалоДня(ТекущаяДата) - 1;
	НачалоПериода = НачалоМесяца(ДобавитьМесяц(ТекущаяДата, -12));
	КонецПериода = КонецМесяца(ДобавитьМесяц(ТекущаяДата, -1));
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы
	
&НаКлиенте
Процедура Рассчитать(Команда)
	
	Если Не ПроверитьЗаполнение() Тогда 
		Возврат;
	КонецЕсли;
	
	РассчитатьНаСервере();
	
	Состояние(НСтр("ru = 'Выполнено'"));
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьПериод(Команда)
	
	ОбщегоНазначенияУТКлиент.РедактироватьПериод(ЭтотОбъект, 
		Новый Структура("ДатаНачала, ДатаОкончания", "НачалоПериода", "КонецПериода"));
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура РассчитатьНаСервере()
	
	Попытка
		проф_ПараметрыНаработкиОбъектовРемонтаВызовСервера.проф_РасчетПараметровНаработкиОРСтратегический(НачалоПериода, КонецПериода, ДатаЗаписи, Истина);
	Исключение
		ТекстСообщения = СтрШаблон(НСтр("ru = 'Не удалось произвести расчет по причине: %1'"), ОписаниеОшибки());
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
	КонецПопытки;

КонецПроцедуры

#КонецОбласти

//-- Проф-ИТ, #150, Соловьев А.А., 18.12.2023