
#Область ОбработчикиСобытий

&НаСервере
Процедура проф_ПриСозданииНаСервереВместо(Отказ, СтандартнаяОбработка)
	
	//++ Проф-ИТ, #295, Соловьев А.А., 16.10.2023
	Если НЕ ЗначениеЗаполнено(Параметры.Склады) Тогда 
		Возврат;
	КонецЕсли;
	
	ДобавитьРеквизитыЭлементыФормы();
	
	ЭтотОбъект["ОсталосьОбеспечить"] = Параметры.ОсталосьОбеспечить;
	
	СкладОтправитель = Параметры.СкладОтправитель;
	МинОстаток = Параметры.МинОстаток;
	Для Каждого Склад Из Параметры.Склады Цикл 
		
		Если Склад.Ключ = СкладОтправитель Тогда 
			ТекстСообщения = НСтр("ru = 'Склад-получатель не должен быть равен складу-отправителю'");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
			Продолжить;
		КонецЕсли;
		
		Для Каждого СтрокаНазначения Из ЭтотОбъект["ТаблицаНазначения"] Цикл
			СтрокаТЧ = Склады.Добавить();
			СтрокаТЧ.Склад = Склад.Ключ;
			СтрокаТЧ.Количество = Мин(Склад.Значение, ОстатокСклада);
			СтрокаТЧ.Назначение = СтрокаНазначения.Назначение;
			СтрокаТЧ.ДоступноеКоличество = СтрокаНазначения.Количество;
		КонецЦикла;
		
	КонецЦикла;
	
	Если Склады.Количество() = 0 Тогда 
		Отказ = Истина;
	КонецЕсли;
	//-- Проф-ИТ, #295, Соловьев А.А., 16.10.2023

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

//++ Проф-ИТ, #295, Соловьев А.А., 10.10.2023

&НаСервере
Процедура ДобавитьРеквизитыЭлементыФормы()
		
	СтрТаблицаНазначения = "ТаблицаНазначения";
	СтрНазначение = "Назначение";
	ТипДанныхЧисло		= Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15, 3));
	ТипДанныхНазначение = Новый ОписаниеТипов("СправочникСсылка.проф_Назначения");
	
	ДобавляемыеРеквизиты = Новый Массив;
	ТипыРеквизита = Новый Массив;
	ТипыРеквизита.Добавить(Тип("ТаблицаЗначений"));
	ОписаниеТиповДляРеквизита = Новый ОписаниеТипов(ТипыРеквизита); 
	ТаблицаНазначения = Новый РеквизитФормы(СтрТаблицаНазначения, ОписаниеТиповДляРеквизита);
	ДобавляемыеРеквизиты.Добавить(ТаблицаНазначения);
	
	НовыйРеквизит = Новый РеквизитФормы(СтрНазначение, ТипДанныхНазначение, СтрТаблицаНазначения);
	ДобавляемыеРеквизиты.Добавить(НовыйРеквизит);
	НовыйРеквизит = Новый РеквизитФормы("Количество", ТипДанныхЧисло, СтрТаблицаНазначения);
	ДобавляемыеРеквизиты.Добавить(НовыйРеквизит);
	
	НовыйРеквизит = Новый РеквизитФормы(СтрНазначение, ТипДанныхНазначение, "Склады"); 
	ДобавляемыеРеквизиты.Добавить(НовыйРеквизит);
	НовыйРеквизит = Новый РеквизитФормы("ДоступноеКоличество", ТипДанныхЧисло, "Склады");
	ДобавляемыеРеквизиты.Добавить(НовыйРеквизит);
	
	НовыйРеквизит = Новый РеквизитФормы("ОсталосьОбеспечить", ТипДанныхЧисло); 
	ДобавляемыеРеквизиты.Добавить(НовыйРеквизит);
	
	ИзменитьРеквизиты(ДобавляемыеРеквизиты);
	
	проф_ГруппаШапка						= Элементы.Добавить("проф_ГруппаОтборВерх", Тип("ГруппаФормы"), ЭтотОбъект);
	проф_ГруппаШапка.Вид					= ВидГруппыФормы.ОбычнаяГруппа;
	проф_ГруппаШапка.Отображение			= ОтображениеОбычнойГруппы.Нет;
	проф_ГруппаШапка.ОтображатьЗаголовок	= Ложь; 
	проф_ГруппаШапка.Группировка			= ГруппировкаПодчиненныхЭлементовФормы.ГоризонтальнаяВсегда;
	
	Элементы.Переместить(проф_ГруппаШапка, ЭтотОбъект, Элементы.СкладОтправитель);
	Элементы.Переместить(Элементы.СкладОтправитель, проф_ГруппаШапка);
	
	ЭлементОсталосьОбеспечить = Элементы.Добавить("ОсталосьОбеспечить", Тип("ПолеФормы"), проф_ГруппаШапка);
	ЭлементОсталосьОбеспечить.Вид = ВидПоляФормы.ПолеВвода;
	ЭлементОсталосьОбеспечить.Заголовок = НСтр("ru = 'Ост. обеспечить'");
	ЭлементОсталосьОбеспечить.ПутьКДанным = "ОсталосьОбеспечить";
	ЭлементОсталосьОбеспечить.ТолькоПросмотр = Истина;
	
	ЭтотОбъект[СтрТаблицаНазначения].Загрузить(ПолучитьИзВременногоХранилища(Параметры.АдресТаблицыОстатковНаСкладе));
	
	ЭлементНазначение = Элементы.Добавить("СкладыНазначение", Тип("ПолеФормы"), Элементы.Склады);
	ЭлементНазначение.Вид = ВидПоляФормы.ПолеВвода;
	ЭлементНазначение.Заголовок = НСтр("ru = 'Назначение'");
	ЭлементНазначение.ПутьКДанным = "Склады.Назначение";
	ЭлементНазначение.РежимВыбораИзСписка = Истина;
	ЭлементНазначение.КнопкаСоздания = Ложь;
	ЭлементНазначение.ТолькоПросмотр = Истина;
	
	Для Каждого СтрокаНазначение Из ЭтотОбъект[СтрТаблицаНазначения] Цикл 
		ЭлементНазначение.СписокВыбора.Добавить(СтрокаНазначение.Назначение);
	КонецЦикла;
	
	ЭлементДоступноеКоличество = Элементы.Добавить("СкладыДоступноеКоличество", Тип("ПолеФормы"), Элементы.Склады);
	ЭлементДоступноеКоличество.Вид = ВидПоляФормы.ПолеВвода;
	ЭлементДоступноеКоличество.Заголовок = НСтр("ru = 'Доступное количество'");
	ЭлементДоступноеКоличество.ПутьКДанным = "Склады.ДоступноеКоличество";
	ЭлементДоступноеКоличество.ТолькоПросмотр = Истина;
	
	Элементы.Переместить(ЭлементНазначение, Элементы.Склады, Элементы.СкладыКоличество);
	Элементы.Переместить(ЭлементДоступноеКоличество, Элементы.Склады, Элементы.СкладыКоличество);
	
КонецПроцедуры

//-- Проф-ИТ, #295, Соловьев А.А., 10.10.2023

&НаКлиенте
Процедура проф_СкладыПередОкончаниемРедактированияВместо(Элемент, НоваяСтрока, ОтменаРедактирования, Отказ)
	//Вставить содержимое обработчика
КонецПроцедуры

&НаКлиенте
Процедура проф_СкладыВыборВместо(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	//Вставить содержимое обработчика
КонецПроцедуры

&НаКлиенте
&Вместо("ВыбратьДанные")
Процедура проф_ВыбратьДанные()
	
	//++ Проф-ИТ, #295, Соловьев А.А., 16.10.2023
	ОчиститьСообщения();
	РезультатПроверки = ПроверитьДанныеНаСервере();
	
	Если РезультатПроверки = "Вопрос" Тогда 
		ОповещениеОбОтвете = Новый ОписаниеОповещения("ЗавершитьВыбор", ЭтаФорма);
		ПоказатьВопрос(ОповещениеОбОтвете, 
			НСтр("ru = 'При перемещении выбранного количества свободный остаток
			| станет меньше минимально установленного. Продолжить?'"),
			РежимДиалогаВопрос.ДаНет);
	КонецЕсли;		
	
	Если ПустаяСтрока(РезультатПроверки) Тогда 		
		ЗавершитьВыбор(КодВозвратаДиалога.Да, Неопределено);		
	КонецЕсли;	
	//-- Проф-ИТ, #295, Соловьев А.А., 16.10.2023
	
КонецПроцедуры

//++ Проф-ИТ, #295, Соловьев А.А., 10.10.2023

&НаСервере
Функция ПроверитьДанныеНаСервере()
	
	РезультатПроверки = "";
	
	ТаблицаДанных = Склады.Выгрузить(, "Количество");
	ТаблицаДанных.Свернуть(, "Количество");
	
	Если ТаблицаДанных.Количество() = 0 Тогда 
		ТекстСообщения = НСтр("ru = 'Ошибка, не выбран склад для перемещения'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
	ИначеЕсли ТаблицаДанных[0].Количество > ЭтотОбъект["ОсталосьОбеспечить"] Тогда 
		ТекстСообщения = НСтр("ru = 'Общее количество не может превышать <%1>
			| - количество, необходимое к обеспечению'");
		ТекстСообщения = СтрШаблон(ТекстСообщения, ЭтотОбъект["ОсталосьОбеспечить"]);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "ОсталосьОбеспечить");
	КонецЕсли;
	
	Если Не ПустаяСтрока(ТекстСообщения) Тогда 
		Возврат "Отказ";
	КонецЕсли;
	
	НомерСтроки = 1;
	
	Для Каждого СтрокаДанных Из Склады Цикл
		
		Если СтрокаДанных.Количество > СтрокаДанных.ДоступноеКоличество Тогда
			
			Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("Склады", НомерСтроки, "ДоступноеКоличество");
			
			ТекстСообщенияШаблон = НСтр("ru = 'Количество не может превышать <%1>
				| - свободный остаток на складе-отправителе по назначению %2'");
			ТекстСообщения = СтрШаблон(ТекстСообщенияШаблон, СтрокаДанных.ДоступноеКоличество, СтрокаДанных.Назначение);
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, , Поле);
			
			РезультатПроверки = "Отказ";
			Прервать;
		КонецЕсли;
		
		Если МинОстаток > 0 И СтрокаДанных.Количество > 0
		И (ОстатокСклада - СтрокаДанных.Количество) < МинОстаток Тогда
			РезультатПроверки = "Вопрос";
		КонецЕсли;
		
		НомерСтроки = НомерСтроки + 1;
		
	КонецЦикла;
	
	Возврат РезультатПроверки;	
	
КонецФункции

//-- Проф-ИТ, #295, Соловьев А.А., 10.10.2023

&НаКлиенте
&ИзменениеИКонтроль("ЗавершитьВыбор")
Процедура проф_ЗавершитьВыбор(Результат, ДопПараметры)
	Если Результат = КодВозвратаДиалога.Да Тогда
		ТекСтрока = Элементы.Склады.ТекущиеДанные;
		#Удаление
		СтруктураВозврата = Новый Структура("СкладПолучатель, Количество, СкладОтправитель", 
			ТекСтрока.Склад, ТекСтрока.Количество, СкладОтправитель);
		Закрыть(СтруктураВозврата);	
		#КонецУдаления
		#Вставка
		//++ Проф-ИТ, #295, Соловьев А.А., 10.10.2023
		АдресТаблицы = ЗавершитьВыборНаСервере();
		Если АдресТаблицы = Неопределено Тогда 
			Возврат;
		КонецЕсли;	
		Закрыть(АдресТаблицы);
		//-- Проф-ИТ, #295, Соловьев А.А., 10.10.2023
		#КонецВставки
	КонецЕсли;
КонецПроцедуры

//++ Проф-ИТ, #295, Соловьев А.А., 10.10.2023

&НаСервере
Функция ЗавершитьВыборНаСервере()
	
	ОписаниеТиповСклады = Новый ОписаниеТипов("СправочникСсылка.Склады");
	ТаблицаДанных = Новый ТаблицаЗначений;
	ТаблицаДанных.Колонки.Добавить("СкладОтправитель", ОписаниеТиповСклады);
	ТаблицаДанных.Колонки.Добавить("СкладПолучатель", ОписаниеТиповСклады);
	ТаблицаДанных.Колонки.Добавить("НазначениеИсходное", Новый ОписаниеТипов("СправочникСсылка.проф_Назначения"));
	ТаблицаДанных.Колонки.Добавить("Количество", Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15, 3)));
	
	Для Каждого СтрокаСклад Из Склады Цикл
		Если СтрокаСклад.Количество > 0 Тогда 
			НоваяСтрока = ТаблицаДанных.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаСклад);
			НоваяСтрока.НазначениеИсходное = СтрокаСклад.Назначение;
			НоваяСтрока.СкладПолучатель = СтрокаСклад.Склад;
		КонецЕсли;	
	КонецЦикла;
	
	Если ТаблицаДанных.Количество() = 0 Тогда 
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Заполните количество'"), , "Склады[0].Количество");
		Возврат Неопределено;
	КонецЕсли;	
	
	ТаблицаДанных.ЗаполнитьЗначения(СкладОтправитель, "СкладОтправитель");
	АдресТаблицы = ПоместитьВоВременноеХранилище(ТаблицаДанных);
	
	Возврат АдресТаблицы;
	
КонецФункции

//-- Проф-ИТ, #295, Соловьев А.А., 10.10.2023

#КонецОбласти