
//++ Проф-ИТ, #53, Башинская А., 13.10.2023	

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("ОповещениеОРезультатах") И Параметры.ОповещениеОРезультатах Тогда
		Элементы.Загрузить.Доступность 	  = Ложь;
		Элементы.Результат.Видимость 	  = Истина;
		Элементы.ФайлВыгрузки.Доступность = Ложь;     
			
		Результат = Параметры.СообщениеРезультат; 
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ФайлВыгрузкиНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ДиалогВыбора           = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.Открытие);
	ДиалогВыбора.Заголовок = "Выберите файл";
	Фильтр				   = "Табличный документ(*.xls; *.xlsx)|*.xls; *.xlsx"; 
	ДиалогВыбора.Фильтр    = Фильтр;	 
	Оповещение = Новый ОписаниеОповещения("ЗавершениеОткрытияФайла", ЭтотОбъект);
	ДиалогВыбора.Показать(Оповещение);
	
КонецПроцедуры 

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)    
	
	Если Не Параметры.Свойство("ОповещениеОРезультатах") Тогда
		ОповеститьОВыборе(АдресВоВременномХранилище);  
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ПрограммныйИнтерфейс

&НаКлиенте
Процедура ЗавершениеОткрытияФайла(ВыбранныеФайлы, ДополнительныеПараметры) Экспорт
	
	Если ВыбранныеФайлы = Неопределено Тогда
		Возврат;
	Иначе
		ФайлВыгрузки = ВыбранныеФайлы[0];  
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти  

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Загрузить(Команда)
	
	ОчиститьСообщения();
	
	Если ПустаяСтрока(ФайлВыгрузки) Тогда
		
		Сообщение = Новый СообщениеПользователю;
		Сообщение.Текст = "Выберите файл!";
		Сообщение.Сообщить();
		Возврат; 
		
	КонецЕсли;	
	
	ЗагрузитьДанныеВТаблицу();
	
КонецПроцедуры

#КонецОбласти 

#Область СлужебныеПроцедурыИФункции 

&НаКлиенте
Процедура ЗагрузитьДанныеВТаблицу()
	
	Адрес = ПоместитьВоВременноеХранилище(Новый ДвоичныеДанные(ФайлВыгрузки));
	ЗагрузитьТабличныйДокументИзФайла(Адрес, ФайлВыгрузки);
	
	Закрыть();
	
КонецПроцедуры

&НаСервере
Процедура ЗагрузитьТабличныйДокументИзФайла(Знач Адрес, Знач ИмяФайла)

    Расширение = Прав(ИмяФайла, 4);
    Расширение = СтрЗаменить(Расширение, ".", "");

    ФайлПриемник = ПолучитьИмяВременногоФайла(Расширение);
    ДанныеХранилища = ПолучитьИзВременногоХранилища(Адрес);
    ДанныеХранилища.Записать(ФайлПриемник);

    ТабличныйДокумент.Очистить();
    ТабличныйДокумент.Прочитать(ФайлПриемник);
    ТЗ = ПреобразоватьТабличныйДокументВТаблицуЗначений(ТабличныйДокумент);
	
	АдресВоВременномХранилище = ПоместитьВоВременноеХранилище(ТЗ, АдресВоВременномХранилище);
	
// Удаляем временный файл
	Попытка
		УдалитьФайлы(ФайлПриемник);
	Исключение
		ЗаписьЖурналаРегистрации(НСтр("ru = 'Загрузка начальных данных по ОР. Ошибка удаления временных файлов.'"),
								УровеньЖурналаРегистрации.Ошибка, , ,
								ОбработкаОшибок.ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
	КонецПопытки;

КонецПроцедуры

&НаСервере
Функция ПреобразоватьТабличныйДокументВТаблицуЗначений(ТабДокумент)

    ПоследняяСтрока = ТабДокумент.ВысотаТаблицы;
    ПоследняяКолонка = ТабДокумент.ШиринаТаблицы;

    ОбластьЯчеек = ТабДокумент.Область(1, 1, ПоследняяСтрока, ПоследняяКолонка);

    // Создаем описание источника данных на основании области ячеек табличного документа.
    ИсточникДанных = Новый ОписаниеИсточникаДанных(ОбластьЯчеек);

    // Создаем объект для интеллектуального построения отчетов,
    // указываем источник данных и выполняем построение отчета.
    ПостроительОтчета = Новый ПостроительОтчета;
    ПостроительОтчета.ИсточникДанных = ИсточникДанных;
    ПостроительОтчета.Выполнить();

    // Результат выгружаем в таблицу значений.
    ТабЗначений = ПостроительОтчета.Результат.Выгрузить();

    Возврат ТабЗначений;

КонецФункции  

#КонецОбласти

//-- Проф-ИТ, #53, Башинская А., 13.10.2023	