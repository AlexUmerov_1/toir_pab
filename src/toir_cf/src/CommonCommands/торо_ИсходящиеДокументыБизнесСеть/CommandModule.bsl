
#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	Отбор = Новый Структура;
	Отбор.Вставить("ВидДокумента", ПредопределенноеЗначение("Перечисление.ТипыДокументовЭДО.ЗаказТовара"));
	Если ЗначениеЗаполнено(ПараметрКоманды) Тогда
		Отбор.Вставить("Контрагент", торо_ОбщегоНазначенияВызовСервера.ЗначениеРеквизитаОбъекта(ПараметрКоманды, "Контрагент"));
	КонецЕсли;
	
	БизнесСетьСлужебныйКлиент.ОткрытьСписокДокументовОбмена("Исходящие", Отбор);
	
КонецПроцедуры

#КонецОбласти
