#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Параметры.Свойство("ЗаказНаВП", ЗаказНаВП);
	Параметры.Свойство("КодСтроки", КодСтроки);
	Параметры.Свойство("МожноУдалятьСтроки", МожноУдалятьСтроки);
	
	Если Параметры.Свойство("АдресВХТаблицыСтрок") Тогда
		ТаблицаСтрокВХ = ПолучитьИзВременногоХранилища(Параметры.АдресВХТаблицыСтрок);
		ТаблицаСтрок.Загрузить(ТаблицаСтрокВХ);
	Иначе
		Если Не ЗначениеЗаполнено(ЗаказНаВП) Тогда
			Отказ = Истина;
			Возврат;
		КонецЕсли;
		
		НоваяСтрокаТЗ = ТаблицаСтрок.Добавить();
		НоваяСтрокаТЗ.ЗаказНаВП = ЗаказНаВП;
		НоваяСтрокаТЗ.КодСтроки = КодСтроки;
	КонецЕсли;
	
	Параметры.Свойство("КомментарийКУтверждениюПоДокументу", КомментарийКУтверждениюПоДокументу);
	Параметры.Свойство("ИзДокумента", ИзДокумента);
	
	ЗаполнитьТаблицуКомментариев();
	
	Если Не ИзДокумента Тогда
	    Элементы.ТаблицаКомментариевДобавитьКомментарий.Видимость = Ложь;
	КонецЕсли;
	
	Элементы.ТаблицаКомментариев.ОтборСтрок = Новый ФиксированнаяСтруктура("Удален", Ложь);
	
	Если МожноУдалятьСтроки Тогда
		Элементы.ТаблицаКомментариевЗаказНаВП.Видимость = Истина;
	КонецЕсли;
	
	ИспользоватьХарактеристики = Константы.торо_ИспользоватьХарактеристикиНоменклатуры.Получить();
	Если Не ИспользоватьХарактеристики Тогда
		Элементы.ТаблицаКомментариевХарактеристика.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ДобавитьКомментарий(Команда)
	
	НоваяСтрока = ТаблицаКомментариев.Добавить();
	НоваяСтрока.Пользователь = ПользователиКлиент.ТекущийПользователь();
	НоваяСтрока.МожноРедактировать = Истина;
	
	Элементы.ТаблицаКомментариевДобавитьКомментарий.Доступность = Ложь;
	
КонецПроцедуры

&НаКлиенте
Процедура Сохранить(Команда)

	Если Не ПроверитьЗаполнениеНаСервере() Тогда
		Возврат;
	КонецЕсли;
	
	АдресВХУдаленныхСтрок = "";
	СохранитьНаСервере(АдресВХУдаленныхСтрок);
	
	Если ЗначениеЗаполнено(КомментарийКУтверждениюПоДокументу) Тогда
	    Оповестить("ВводКомментарияКУтверждениюПоДокументу", КомментарийКУтверждениюПоДокументу);
	ИначеЕсли ЗначениеЗаполнено(АдресВХУдаленныхСтрок) Тогда
		Оповестить("УдаленыКомментарииКУтверждениюСтрокПоДокументу", АдресВХУдаленныхСтрок);
	КонецЕсли;
	
	Закрыть();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыТаблицаКомментариев

&НаКлиенте
Процедура ТаблицаКомментариевПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	Отказ = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаКомментариевПередНачаломИзменения(Элемент, Отказ)
	
	ТекущиеДанные = Элементы.ТаблицаКомментариев.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Или Не ТекущиеДанные.МожноРедактировать Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаКомментариевПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	ТекущиеДанные = Элементы.ТаблицаКомментариев.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;

	Если Не ЗначениеЗаполнено(ТекущиеДанные.Период) Тогда
	    КомментарийКУтверждениюПоДокументу = ТекущиеДанные.Комментарий;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаКомментариевПередУдалением(Элемент, Отказ)
	
	ТекущиеДанные = Элементы.ТаблицаКомментариев.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
	Если ТекущиеДанные.КодСтрокиНоменклатуры = 0 И Не ЗначениеЗаполнено(ТекущиеДанные.Период) И ТекущиеДанные.МожноРедактировать Тогда
		Элементы.ТаблицаКомментариевДобавитьКомментарий.Доступность = Истина;
		КомментарийКУтверждениюПоДокументу = "";
		Возврат;
	КонецЕсли;
	
	Отказ = Истина;
	
	Если МожноУдалятьСтроки И ТекущиеДанные.МожноРедактировать Тогда
		ТекущиеДанные.Удален = Истина;
		ТекущиеДанные.Изменен = Истина;
		Возврат;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаКомментариевКомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ТекущиеДанные = Элементы.ТаблицаКомментариев.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ОповещениеОЗакрытии = Новый ОписаниеОповещения("КомментарийЗавершениеВвода", ЭтотОбъект, Неопределено);
	
	ПоказатьВводСтроки(ОповещениеОЗакрытии, ТекущиеДанные.Комментарий, "Комментарий к утверждению",, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура ТаблицаКомментариевКомментарийПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.ТаблицаКомментариев.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные.Изменен = Истина;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЗаполнитьТаблицуКомментариев()
	
	Запрос = Новый Запрос();
	Запрос.Текст = "ВЫБРАТЬ
	               |	ТаблицаСтрок.ЗаказНаВП КАК ЗаказНаВП,
	               |	ТаблицаСтрок.КодСтроки КАК КодСтроки
	               |ПОМЕСТИТЬ ВТ_ТаблицаСтрок
	               |ИЗ
	               |	&ТаблицаСтрок КАК ТаблицаСтрок
	               |
	               |ИНДЕКСИРОВАТЬ ПО
	               |	ЗаказНаВП,
	               |	КодСтроки
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	торо_КомментарииКУтверждениюЗаказовНаВПСрезПоследних.ЗаказНаВнутреннееПотребление КАК ЗаказНаВнутреннееПотребление,
	               |	торо_КомментарииКУтверждениюЗаказовНаВПСрезПоследних.Пользователь КАК Пользователь,
	               |	торо_КомментарииКУтверждениюЗаказовНаВПСрезПоследних.КодСтрокиНоменклатуры КАК КодСтрокиНоменклатуры,
	               |	торо_КомментарииКУтверждениюЗаказовНаВПСрезПоследних.Период КАК Период
	               |ПОМЕСТИТЬ ВТ_АвторыКомментариевСрезПоследних
	               |ИЗ
	               |	РегистрСведений.торо_КомментарииКУтверждениюЗаказовНаВП.СрезПоследних(, &ОтборПоТаблицеСтрок) КАК торо_КомментарииКУтверждениюЗаказовНаВПСрезПоследних
	               |
	               |ИНДЕКСИРОВАТЬ ПО
	               |	Период,
	               |	ЗаказНаВнутреннееПотребление,
	               |	КодСтрокиНоменклатуры
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	ВТ_АвторыКомментариевСрезПоследних.ЗаказНаВнутреннееПотребление КАК ЗаказНаВнутреннееПотребление,
	               |	ВТ_АвторыКомментариевСрезПоследних.КодСтрокиНоменклатуры КАК КодСтрокиНоменклатуры,
	               |	МАКСИМУМ(ВТ_АвторыКомментариевСрезПоследних.Период) КАК Период
	               |ПОМЕСТИТЬ ВТ_ПериодыПоследнихАвтороКомментариев
	               |ИЗ
	               |	ВТ_АвторыКомментариевСрезПоследних КАК ВТ_АвторыКомментариевСрезПоследних
	               |
	               |СГРУППИРОВАТЬ ПО
	               |	ВТ_АвторыКомментариевСрезПоследних.ЗаказНаВнутреннееПотребление,
	               |	ВТ_АвторыКомментариевСрезПоследних.КодСтрокиНоменклатуры
	               |
	               |ИНДЕКСИРОВАТЬ ПО
	               |	Период,
	               |	ЗаказНаВнутреннееПотребление,
	               |	КодСтрокиНоменклатуры
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	ВТ_АвторыКомментариевСрезПоследних.ЗаказНаВнутреннееПотребление КАК ЗаказНаВнутреннееПотребление,
	               |	ВТ_АвторыКомментариевСрезПоследних.КодСтрокиНоменклатуры КАК КодСтрокиНоменклатуры,
	               |	ВТ_АвторыКомментариевСрезПоследних.Период КАК Период,
	               |	ВТ_АвторыКомментариевСрезПоследних.Пользователь КАК Пользователь
	               |ПОМЕСТИТЬ ВТ_ПоследниеАвторыКомментариев
	               |ИЗ
	               |	ВТ_АвторыКомментариевСрезПоследних КАК ВТ_АвторыКомментариевСрезПоследних
	               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТ_ПериодыПоследнихАвтороКомментариев КАК ВТ_ПериодыПоследнихАвтороКомментариев
	               |		ПО ВТ_АвторыКомментариевСрезПоследних.Период = ВТ_ПериодыПоследнихАвтороКомментариев.Период
	               |			И ВТ_АвторыКомментариевСрезПоследних.ЗаказНаВнутреннееПотребление = ВТ_ПериодыПоследнихАвтороКомментариев.ЗаказНаВнутреннееПотребление
	               |			И ВТ_АвторыКомментариевСрезПоследних.КодСтрокиНоменклатуры = ВТ_ПериодыПоследнихАвтороКомментариев.КодСтрокиНоменклатуры
	               |
	               |ИНДЕКСИРОВАТЬ ПО
	               |	Период,
	               |	Пользователь,
	               |	ЗаказНаВнутреннееПотребление,
	               |	КодСтрокиНоменклатуры
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	торо_КомментарииКУтверждениюЗаказовНаВП.Период КАК Период,
	               |	торо_КомментарииКУтверждениюЗаказовНаВП.ЗаказНаВнутреннееПотребление КАК ЗаказНаВП,
	               |	торо_КомментарииКУтверждениюЗаказовНаВП.Пользователь КАК Пользователь,
	               |	торо_КомментарииКУтверждениюЗаказовНаВП.КодСтрокиНоменклатуры КАК КодСтрокиНоменклатуры,
	               |	торо_КомментарииКУтверждениюЗаказовНаВП.Комментарий КАК Комментарий,
	               |	ЗаказНаВнутреннееПотреблениеТовары.Номенклатура КАК Номенклатура,
	               |	ЗаказНаВнутреннееПотреблениеТовары.Характеристика КАК Характеристика,
	               |	ВЫБОР
	               |		КОГДА ЗаказНаВнутреннееПотреблениеТовары.Номенклатура.ИспользованиеХарактеристик = ЗНАЧЕНИЕ(Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.НеИспользовать)
	               |			ТОГДА ЛОЖЬ
	               |		ИНАЧЕ ИСТИНА
	               |	КОНЕЦ КАК ХарактеристикиИспользуются,
	               |	ВЫБОР
	               |		КОГДА ВТ_ПоследниеАвторыКомментариев.Пользователь ЕСТЬ NULL
	               |			ТОГДА ЛОЖЬ
	               |		КОГДА торо_КомментарииКУтверждениюЗаказовНаВП.Пользователь = &ТекущийПользователь
	               |			ТОГДА ИСТИНА
	               |		ИНАЧЕ ЛОЖЬ
	               |	КОНЕЦ КАК МожноРедактировать
	               |ИЗ
	               |	РегистрСведений.торо_КомментарииКУтверждениюЗаказовНаВП КАК торо_КомментарииКУтверждениюЗаказовНаВП
	               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТ_ТаблицаСтрок КАК ВТ_ТаблицаСтрок
	               |		ПО торо_КомментарииКУтверждениюЗаказовНаВП.ЗаказНаВнутреннееПотребление = ВТ_ТаблицаСтрок.ЗаказНаВП
	               |			И (&СоединениеПоКодуСтроки)
	               |		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ЗаказНаВнутреннееПотребление.Товары КАК ЗаказНаВнутреннееПотреблениеТовары
	               |		ПО торо_КомментарииКУтверждениюЗаказовНаВП.ЗаказНаВнутреннееПотребление = ЗаказНаВнутреннееПотреблениеТовары.Ссылка
	               |			И торо_КомментарииКУтверждениюЗаказовНаВП.КодСтрокиНоменклатуры = ЗаказНаВнутреннееПотреблениеТовары.КодСтроки
	               |		ЛЕВОЕ СОЕДИНЕНИЕ ВТ_ПоследниеАвторыКомментариев КАК ВТ_ПоследниеАвторыКомментариев
	               |		ПО торо_КомментарииКУтверждениюЗаказовНаВП.Период = ВТ_ПоследниеАвторыКомментариев.Период
	               |			И торо_КомментарииКУтверждениюЗаказовНаВП.Пользователь = ВТ_ПоследниеАвторыКомментариев.Пользователь
	               |			И торо_КомментарииКУтверждениюЗаказовНаВП.ЗаказНаВнутреннееПотребление = ВТ_ПоследниеАвторыКомментариев.ЗаказНаВнутреннееПотребление
	               |			И торо_КомментарииКУтверждениюЗаказовНаВП.КодСтрокиНоменклатуры = ВТ_ПоследниеАвторыКомментариев.КодСтрокиНоменклатуры
	               |ГДЕ
	               |	(НЕ ЗаказНаВнутреннееПотреблениеТовары.КодСтроки ЕСТЬ NULL
	               |			ИЛИ торо_КомментарииКУтверждениюЗаказовНаВП.КодСтрокиНоменклатуры = 0)
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	ЗаказНаВП,
	               |	Период";
	
	Запрос.УстановитьПараметр("ТекущийПользователь", Пользователи.ТекущийПользователь());
	Запрос.УстановитьПараметр("ТаблицаСтрок", ТаблицаСтрок.Выгрузить());
	
	Если ИзДокумента Тогда
		Запрос.УстановитьПараметр("СоединениеПоКодуСтроки", Истина);
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&ОтборПоТаблицеСтрок", "(ЗаказНаВнутреннееПотребление) В
															               |	(ВЫБРАТЬ
															               |		ВТ_ТаблицаСтрок.ЗаказНаВП КАК ЗаказНаВП
															               |	ИЗ
															               |		ВТ_ТаблицаСтрок КАК ВТ_ТаблицаСтрок)");
	Иначе
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&СоединениеПоКодуСтроки", "торо_КомментарииКУтверждениюЗаказовНаВП.КодСтрокиНоменклатуры = ВТ_ТаблицаСтрок.КодСтроки");
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&ОтборПоТаблицеСтрок", "(ЗаказНаВнутреннееПотребление, КодСтрокиНоменклатуры) В
															               |	(ВЫБРАТЬ
															               |		ВТ_ТаблицаСтрок.ЗаказНаВП КАК ЗаказНаВП,
															               |		ВТ_ТаблицаСтрок.КодСтроки КАК КодСтроки
															               |	ИЗ
															               |		ВТ_ТаблицаСтрок КАК ВТ_ТаблицаСтрок)");
	КонецЕсли;
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ЕстьКомментарийПоДокументу = Ложь;
	
	ВыборкаКомментариев = РезультатЗапроса.Выбрать();
	Пока ВыборкаКомментариев.Следующий() Цикл
		ЗаполнитьЗначенияСвойств(ТаблицаКомментариев.Добавить(), ВыборкаКомментариев);
		
		Если ВыборкаКомментариев.КодСтрокиНоменклатуры = 0 И ВыборкаКомментариев.МожноРедактировать Тогда
		    ЕстьКомментарийПоДокументу = Истина;
		КонецЕсли;
	КонецЦикла;
	
	Если ЗначениеЗаполнено(КомментарийКУтверждениюПоДокументу) Тогда
	    НоваяСтрока = ТаблицаКомментариев.Добавить();
		НоваяСтрока.Пользователь = Пользователи.ТекущийПользователь();
		НоваяСтрока.МожноРедактировать = Истина;
		НоваяСтрока.Комментарий = КомментарийКУтверждениюПоДокументу;
		
		Элементы.ТаблицаКомментариевДобавитьКомментарий.Доступность = Ложь;
	Иначе
		Элементы.ТаблицаКомментариевДобавитьКомментарий.Доступность = Не ЕстьКомментарийПоДокументу;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КомментарийЗавершениеВвода(ВведенныйТекст, ДополнительныеПараметры) Экспорт
	
	Если ВведенныйТекст = Неопределено Тогда
		Возврат;
	КонецЕсли;	
	
	ТекущиеДанные = Элементы.ТаблицаКомментариев.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные.Комментарий = ВведенныйТекст;
	ТекущиеДанные.Изменен = Истина;
	
КонецПроцедуры

&НаКлиенте
Функция ПроверитьЗаполнениеНаСервере()
	
	ВсеКомментарииЗаполнены = Истина;
	
	Для каждого СтрокаТЧ Из ТаблицаКомментариев Цикл
	    Если Не ЗначениеЗаполнено(СтрокаТЧ.Комментарий) Тогда
		    ВсеКомментарииЗаполнены = Ложь;
			
			ШаблонСообщения = НСтр("ru = 'На дату %1 не заполнен комментарий номенклатуры <%2, %3>'");
			ТекстСообщения = СтрШаблон(ШаблонСообщения, СтрокаТЧ.Период, СтрокаТЧ.Номенклатура, СтрокаТЧ.Характеристика);
			ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения);
		КонецЕсли;
	КонецЦикла;
	
	Возврат ВсеКомментарииЗаполнены;
	
КонецФункции

&НаСервере
Процедура СохранитьНаСервере(АдресВХУдаленныхСтрок = "")
	
	ТаблицаУдаленныхСтрок = Новый ТаблицаЗначений();
	ТаблицаУдаленныхСтрок.Колонки.Добавить("ЗаказНаВП", Новый ОписаниеТипов("ДокументСсылка.ЗаказНаВнутреннееПотребление"));
	ТаблицаУдаленныхСтрок.Колонки.Добавить("КодСтрокиНоменклатуры", Новый ОписаниеТипов("Число"));
	
	Для каждого СтрокаТЧ Из ТаблицаКомментариев Цикл
		Если Не СтрокаТЧ.Изменен Или Не ЗначениеЗаполнено(СтрокаТЧ.Период) Тогда
			Продолжить;
		КонецЕсли;
		
		Если СтрокаТЧ.Удален Тогда
			МенеджерЗаписи = РегистрыСведений.торо_КомментарииКУтверждениюЗаказовНаВП.СоздатьМенеджерЗаписи();
			МенеджерЗаписи.Период = СтрокаТЧ.Период;
			МенеджерЗаписи.ЗаказНаВнутреннееПотребление = СтрокаТЧ.ЗаказНаВП;
			МенеджерЗаписи.Пользователь = СтрокаТЧ.Пользователь;
			МенеджерЗаписи.КодСтрокиНоменклатуры = СтрокаТЧ.КодСтрокиНоменклатуры;
			
			МенеджерЗаписи.Удалить();
			
			ЗаполнитьЗначенияСвойств(ТаблицаУдаленныхСтрок.Добавить(), СтрокаТЧ);
			
			Продолжить;
		КонецЕсли;
		
		НаборЗаписей = РегистрыСведений.торо_КомментарииКУтверждениюЗаказовНаВП.СоздатьНаборЗаписей();
		НаборЗаписей.Отбор.Период.Установить(СтрокаТЧ.Период);
		НаборЗаписей.Отбор.ЗаказНаВнутреннееПотребление.Установить(СтрокаТЧ.ЗаказНаВП);
		НаборЗаписей.Отбор.Пользователь.Установить(СтрокаТЧ.Пользователь);
		НаборЗаписей.Отбор.КодСтрокиНоменклатуры.Установить(СтрокаТЧ.КодСтрокиНоменклатуры);
		
		НоваяСтрокаНЗ = НаборЗаписей.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрокаНЗ, СтрокаТЧ);
		
		НоваяСтрокаНЗ.ЗаказНаВнутреннееПотребление = СтрокаТЧ.ЗаказНаВП;
		
		НаборЗаписей.Записать(Истина);
	КонецЦикла;
	
	Если ТаблицаУдаленныхСтрок.Количество() > 0 Тогда
		Запрос = Новый Запрос();
		Запрос.Текст = "ВЫБРАТЬ
		               |	ТаблицаУдаленныхСтрок.ЗаказНаВП КАК ЗаказНаВП,
		               |	ТаблицаУдаленныхСтрок.КодСтрокиНоменклатуры КАК КодСтрокиНоменклатуры
		               |ПОМЕСТИТЬ ВТ_ТаблицаУдаленныхСтрок
		               |ИЗ
		               |	&ТаблицаУдаленныхСтрок КАК ТаблицаУдаленныхСтрок
		               |
		               |ИНДЕКСИРОВАТЬ ПО
		               |	ЗаказНаВП,
		               |	КодСтрокиНоменклатуры
		               |;
		               |
		               |////////////////////////////////////////////////////////////////////////////////
		               |ВЫБРАТЬ
		               |	ТаблицаКомментариев.ЗаказНаВП КАК ЗаказНаВП,
		               |	ТаблицаКомментариев.КодСтрокиНоменклатуры КАК КодСтрокиНоменклатуры
		               |ПОМЕСТИТЬ ВТ_ТаблицаКомментариев
		               |ИЗ
		               |	&ТаблицаКомментариев КАК ТаблицаКомментариев
					   |ГДЕ
					   |	НЕ ТаблицаКомментариев.Удален
		               |
		               |ИНДЕКСИРОВАТЬ ПО
		               |	ЗаказНаВП,
		               |	КодСтрокиНоменклатуры
		               |;
		               |
		               |////////////////////////////////////////////////////////////////////////////////
		               |ВЫБРАТЬ РАЗЛИЧНЫЕ
		               |	ВТ_ТаблицаУдаленныхСтрок.ЗаказНаВП КАК ЗаказНаВП,
		               |	ВТ_ТаблицаУдаленныхСтрок.КодСтрокиНоменклатуры КАК КодСтрокиНоменклатуры,
		               |	ВЫБОР
					   |		КОГДА ВТ_ТаблицаКомментариев.ЗаказНаВП ЕСТЬ NULL
					   |			ТОГДА ЛОЖЬ
					   |		ИНАЧЕ ИСТИНА
					   |	КОНЕЦ КАК ЕстьКомментарии
		               |ИЗ
		               |	ВТ_ТаблицаУдаленныхСтрок КАК ВТ_ТаблицаУдаленныхСтрок
		               |		ЛЕВОЕ СОЕДИНЕНИЕ ВТ_ТаблицаКомментариев КАК ВТ_ТаблицаКомментариев
		               |		ПО ВТ_ТаблицаУдаленныхСтрок.ЗаказНаВП = ВТ_ТаблицаКомментариев.ЗаказНаВП
		               |			И ВТ_ТаблицаУдаленныхСтрок.КодСтрокиНоменклатуры = ВТ_ТаблицаКомментариев.КодСтрокиНоменклатуры";
					   
		Запрос.УстановитьПараметр("ТаблицаУдаленныхСтрок", ТаблицаУдаленныхСтрок);
		Запрос.УстановитьПараметр("ТаблицаКомментариев", ТаблицаКомментариев.Выгрузить());
		
		РезультатЗапроса = Запрос.Выполнить();
		ТаблицаУдаленныхСтрок = РезультатЗапроса.Выгрузить();
		
		ТаблицаУдаленныхСтрок.Индексы.Добавить("ЗаказНаВП, КодСтрокиНоменклатуры");
		
		АдресВХУдаленныхСтрок = ПоместитьВоВременноеХранилище(ТаблицаУдаленныхСтрок);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти