
#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	ТекстСообщения = НСтр("ru = 'Добавление записей невозможно. Для этого необходимо использовать команду ""Рассчитать ... (фоновое задание)"" документа ППР'");
	ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения,,,,Отказ);

КонецПроцедуры

#КонецОбласти