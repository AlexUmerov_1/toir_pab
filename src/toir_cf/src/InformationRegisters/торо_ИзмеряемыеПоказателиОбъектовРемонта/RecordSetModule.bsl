#Область ОбработчикиСобытий

Процедура ПриЗаписи(Отказ, Замещение)
	
	Если ЗначениеЗаполнено(ЭтотОбъект.Отбор.ОбъектРемонта.Значение) 
		И НЕ ЭтотОбъект.ДополнительныеСвойства.Свойство("НеОбновлятьРегистрНаличиеНормативов") Тогда
		торо_РаботаСНормативамиСервер.ОбновитьСвойствоОР_ЕстьИзмеряемыеПоказатели(ЭтотОбъект.Отбор.ОбъектРемонта.Значение);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
