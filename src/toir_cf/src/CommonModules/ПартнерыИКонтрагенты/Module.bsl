
#Область ПрограммныйИнтерфейс

// Определяет, есть ли в базе контрагент с таким же набором ИНН/КПП
// Параметры:
//		ИНН - Строка - ИНН.
//		КПП - Строка - КПП.
//		ИсключаяСсылку - СправочникСсылка.Контрагенты - ссылка.
//
// Возвращаемое значение:
// 	СправочникСсылка.Конрагенты, Неопределено - контрагент с таким же ИНН и КПП.
Функция ИННКППУжеИспользуетсяВИнформационнойБазе(ИНН,КПП,ИсключаяСсылку = Неопределено) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 1
	|	Контрагенты.Ссылка
	|ИЗ
	|	Справочник.Контрагенты КАК Контрагенты
	|ГДЕ
	|	Контрагенты.ИНН = &ИНН
	|	И Контрагенты.КПП = &КПП
	|	И Контрагенты.Ссылка <> &Ссылка";
	
	Запрос.УстановитьПараметр("ИНН",ИНН);
	Запрос.УстановитьПараметр("КПП",КПП);
	Запрос.УстановитьПараметр("Ссылка",ИсключаяСсылку);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		Возврат Выборка;
	КонецЕсли;
	
	Возврат Неопределено;
	
КонецФункции

#КонецОбласти

