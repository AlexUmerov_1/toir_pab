#Область СлужебныйПрограммныйИнтерфейс

// Обертка над функцией для вызова из модулей, которые могут выполняться во внешнем соединении.
// Требуется, так как на модуле торо_СЛКСервер нельзя установить флаг "внешнее соединение",
// из-за того, что он привилегированный.
//
Функция СерияЗащитыИнициализирована(Знач Серия) Экспорт

	#Если НЕ ВнешнееСоединение Тогда
		Возврат торо_СЛКСервер.ПроверитьЛицензиюСеанса(Серия);
	#Иначе
		Возврат Ложь;
	#КонецЕсли
	
КонецФункции

Функция ОбменВТекущейСерииДоступен(Обмен) Экспорт
	
	#Если НЕ ВнешнееСоединение Тогда
		Возврат торо_СЛКСервер.ОбменВТекущейСерииДоступен(Обмен);
	#Иначе
		Возврат Ложь;
	#КонецЕсли
	
КонецФункции

#КонецОбласти