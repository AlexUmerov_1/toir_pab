#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПЕРЕМЕННЫЕ

Перем БезусловнаяЗапись Экспорт; // Отключает проверки при записи документа
перем СтруктураДанных Экспорт;  // Структура, хранящая данные для работы с уведомлениями.

#Область ОбработчикиСобытий
Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") Тогда
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеЗаполнения);
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("СправочникСсылка.торо_ОбъектыРемонта") Тогда
		ОбъектРемонта = ДанныеЗаполнения;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ОбъектРемонта) Тогда
		Подразделение = ОбъектРемонта.Подразделение;
		Организация = ОбъектРемонта.Организация;
	КонецЕсли;
	
	Если НЕ ЗначениеЗаполнено(ТекСтруктураИерархии) Тогда
		ТекСтруктураИерархии = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить("НастройкиТОиР", "ОсновнаяСтруктураИерархии");
	КонецЕсли;
	
	торо_ЗаполнениеДокументов.ЗаполнитьСтандартныеРеквизитыШапкиПоУмолчанию(ЭтотОбъект);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		Запрос = Новый Запрос;
		Запрос.Текст =
		"ВЫБРАТЬ
		|	ТабПодчиненные.ОбъектРемонта КАК ОбъектРемонта
		|ПОМЕСТИТЬ ВТ_ОР
		|ИЗ
		|	&ТабПодчиненные КАК ТабПодчиненные
		|ГДЕ
		|	ТабПодчиненные.ПринятьКУчету
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних.ОбъектРемонта КАК ОбъектРемонта,
		|	торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних.СтатусОР КАК СтатусОР,
		|	торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних.Регистратор КАК Регистратор
		|ИЗ
		|	РегистрСведений.торо_СтатусыОбъектовРемонтаВУчете.СрезПоследних(
		|			,
		|			ОбъектРемонта В
		|					(ВЫБРАТЬ
		|						ВТ_ОР.ОбъектРемонта КАК ОбъектРемонта
		|					ИЗ
		|						ВТ_ОР КАК ВТ_ОР)
		|				И Регистратор <> &Ссылка) КАК торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних
		|ГДЕ
		|	торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних.СтатусОР = ЗНАЧЕНИЕ(Перечисление.торо_СтатусыОРВУчете.ПринятоКУчету)";
		
		Запрос.УстановитьПараметр("ТабПодчиненные", СписокПодчиненныхПринятыхКУчету);
		Запрос.УстановитьПараметр("Ссылка", Ссылка);
		
		ВыгрузкаИзРезультатаЗапроса = Запрос.Выполнить().Выгрузить();
		
		Для Каждого СтрУжеПринятКУчету Из ВыгрузкаИзРезультатаЗапроса Цикл
			СтрПодчиненныйОР = СписокПодчиненныхПринятыхКУчету.Найти(СтрУжеПринятКУчету.ОбъектРемонта,"ОбъектРемонта");
			СтрПодчиненныйОР.ПринятьКУчету = Ложь;
			
			ШаблонСообщения = НСтр("ru = 'Объект ремонта %1 уже принят к учету документом %2! Пометка принятия к учету для него снята.'");
			ТекстСообщения = СтрШаблон(ШаблонСообщения, СтрУжеПринятКУчету.ОбъектРемонта, СтрУжеПринятКУчету.Регистратор);
		КонецЦикла;
	КонецЕсли;
	
	Если ЭтоНовый() Тогда
		Автор = Пользователи.ТекущийПользователь();
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если ЗначениеЗаполнено(ОбъектРемонта) Тогда
		ДатаВыпуска = ОбъектРемонта.ДатаВыпуска;
		Если ЗначениеЗаполнено(ДатаВводаВЭксплуатацию) И ДатаВводаВЭксплуатацию < ДатаВыпуска Тогда
			ШаблонСообщения = НСтр("ru = 'Для объекта ремонта <%1> дата ввода в эксплуатацию меньше даты выпуска объекта ремонта: %2.'");
			ТекстСообщения = СтрШаблон(ШаблонСообщения, ОбъектРемонта, Формат(ДатаВыпуска,"ДФ=dd.MM.yyyy"));
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения,,,,Отказ);										
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)

	ПроверитьНаличиеБолееПозднихДокументов(Отказ);
	ПроверитьЧтоОРНеПринятКУчету(Отказ);
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	Движения.торо_СтатусыОбъектовРемонтаВУчете.Записывать = Истина;
	НаборЗаписей = Движения.торо_СтатусыОбъектовРемонтаВУчете;

	НС = НаборЗаписей.Добавить();
	НС.ОбъектРемонта     = ОбъектРемонта;
	НС.Период            = ДатаВводаВЭксплуатацию;
	НС.СтатусОР          = Перечисления.торо_СтатусыОРВУчете.ПринятоКУчету;
	
	Для Каждого ПодчиненныйОР Из СписокПодчиненныхПринятыхКУчету Цикл
		Если Не ПодчиненныйОР.ПринятьКУчету Тогда
			Продолжить;
		КонецЕсли;
		НС = НаборЗаписей.Добавить();
		НС.ОбъектРемонта     = ПодчиненныйОР.ОбъектРемонта;
		НС.Период            = ДатаВводаВЭксплуатацию;
		НС.СтатусОР          = Перечисления.торо_СтатусыОРВУчете.ПринятоКУчету;
	КонецЦикла;
	
	Попытка
		
		ОбъектРемонтаОбъект = ОбъектРемонта.ПолучитьОбъект();
		ОбъектРемонтаОбъект.ДатаВводаВЭксплуатацию = ДатаВводаВЭксплуатацию;
		
		Структура = торо_СтатусыОРВУчете.НастройкиСтатуса(Перечисления.торо_СтатусыОРВУчете.ПринятоКУчету);
		Если Структура <> Неопределено Тогда
			ОбъектРемонтаОбъект.НеУчаствуетВПланировании = Структура.ЗначениеПоУмолчанию;
		КонецЕсли;
		ОбъектРемонтаОбъект.Записать();
		
		Для Каждого ПодчиненныйОР Из СписокПодчиненныхПринятыхКУчету Цикл
			Если Не ПодчиненныйОР.ПринятьКУчету Тогда
				Продолжить;
			КонецЕсли;
			ОбъектРемонтаОбъект = ПодчиненныйОР.ОбъектРемонта.ПолучитьОбъект();
			ОбъектРемонтаОбъект.ДатаВводаВЭксплуатацию = ДатаВводаВЭксплуатацию;
			
			Структура = торо_СтатусыОРВУчете.НастройкиСтатуса(Перечисления.торо_СтатусыОРВУчете.ПринятоКУчету);
			Если Структура <> Неопределено Тогда
				ОбъектРемонтаОбъект.НеУчаствуетВПланировании = Структура.ЗначениеПоУмолчанию;
			КонецЕсли;
			ОбъектРемонтаОбъект.Записать();
		КонецЦикла;
	Исключение
		ЗаписьЖурналаРегистрации("Исключение",УровеньЖурналаРегистрации.Ошибка,,Ссылка,ОписаниеОшибки());
		ТекстСообщения = НСтр("ru = 'Запись не выполнена.'");
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения,,,,Отказ);
	КонецПопытки; 
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	ПроверитьНаличиеБолееПозднихДокументов(Отказ);
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	Попытка
		
		ОбъектРемонтаОбъект = ОбъектРемонта.ПолучитьОбъект();
		Структура = торо_СтатусыОРВУчете.НастройкиСтатуса(Перечисления.торо_СтатусыОРВУчете.НеПринятоКУчету);
		Если Структура <> Неопределено Тогда
			ОбъектРемонтаОбъект.НеУчаствуетВПланировании = Структура.ЗначениеПоУмолчанию;
		КонецЕсли;
		ОбъектРемонтаОбъект.Записать();
		
	Исключение
		ЗаписьЖурналаРегистрации("Исключение",УровеньЖурналаРегистрации.Ошибка,,Ссылка,ОписаниеОшибки());
		ТекстСообщения = НСтр("ru = 'Запись не выполнена.'");
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения,,,,Отказ);
	КонецПопытки; 
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ПроверитьНаличиеБолееПозднихДокументов(Отказ)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	торо_СтатусыОбъектовРемонтаВУчете.Период КАК ДатаВводаВЭксплуатацию,
	|	торо_СтатусыОбъектовРемонтаВУчете.Регистратор КАК Регистратор,
	|	торо_СтатусыОбъектовРемонтаВУчете.ОбъектРемонта,
	|	торо_СтатусыОбъектовРемонтаВУчете.СтатусОР
	|ИЗ
	|	РегистрСведений.торо_СтатусыОбъектовРемонтаВУчете КАК торо_СтатусыОбъектовРемонтаВУчете
	|ГДЕ
	|	торо_СтатусыОбъектовРемонтаВУчете.ОбъектРемонта = &ОбъектРемонта
	|	И торо_СтатусыОбъектовРемонтаВУчете.Регистратор <> &Регистратор
	|	И торо_СтатусыОбъектовРемонтаВУчете.Период >= &ДатаВводаВЭксплуатацию";
	
	Запрос.УстановитьПараметр("ОбъектРемонта",          ОбъектРемонта);
	Запрос.УстановитьПараметр("ДатаВводаВЭксплуатацию", ДатаВводаВЭксплуатацию);
	Запрос.УстановитьПараметр("Регистратор",            Ссылка);
	
	Результат = Запрос.Выполнить();
	
	Если Не Результат.Пустой() Тогда
		// Проверка на наличие документов после указанный даты ввода в эксплуатацию.
		ШаблонСообщения = НСтр("ru = 'Для объекта ремонта <%1> имеются документы на ту же или более позднюю дату'");
		ТекстСообщения = СтрШаблон(ШаблонСообщения, ОбъектРемонта);
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения,,,,Отказ);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьЧтоОРНеПринятКУчету(Отказ)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних.ОбъектРемонта КАК ОбъектРемонта,
	|	торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних.СтатусОР КАК СтатусОР
	|ИЗ
	|	РегистрСведений.торо_СтатусыОбъектовРемонтаВУчете.СрезПоследних(&Период, ОбъектРемонта = &ОбъектРемонта) КАК торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних";
	
	Запрос.УстановитьПараметр("Период", ДатаВводаВЭксплуатацию-1);
	Запрос.УстановитьПараметр("ОбъектРемонта", ОбъектРемонта);
	
	ПринятКУчету = Ложь;
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		Если Выборка.СтатусОР = Перечисления.торо_СтатусыОРВУчете.ПринятоКУчету Тогда
			ПринятКУчету = Истина;
		КонецЕсли;
	КонецЕсли;
	
	Если ПринятКУчету Тогда
		ШаблонСообщения = НСтр("ru = 'Объекта ремонта <%1> уже принят к учету на дату %2'");
		ТекстСообщения = СтрШаблон(ШаблонСообщения,	ОбъектРемонта, ДатаВводаВЭксплуатацию);
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения,,,,Отказ);
	КонецЕсли;
	
КонецПроцедуры



#КонецОбласти

#КонецЕсли