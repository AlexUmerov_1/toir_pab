#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ОбработкаКомандыНаСервере(ПараметрКоманды);
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаКомандыНаСервере(ПараметрКоманды)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	торо_СтатусыРасчетаПроектныхЗатратНаРемонты.IDРемонта КАК IDРемонта,
	|	торо_СтатусыРасчетаПроектныхЗатратНаРемонты.ОбъектРемонта КАК ОбъектРемонта,
	|	торо_СтатусыРасчетаПроектныхЗатратНаРемонты.ДокументИсточник КАК ДокументИсточник,
	|	торо_СтатусыРасчетаПроектныхЗатратНаРемонты.ДокументЗатрат КАК ДокументЗатрат,
	|	торо_СтатусыРасчетаПроектныхЗатратНаРемонты.ВидДокументаИсточника КАК ВидДокументаИсточника,
	|	торо_СтатусыРасчетаПроектныхЗатратНаРемонты.ТехКарта КАК ТехКарта,
	|	торо_СтатусыРасчетаПроектныхЗатратНаРемонты.ТиповойДефект КАК ТиповойДефект,
	|	торо_СтатусыРасчетаПроектныхЗатратНаРемонты.ДатаСрезаТехКарты КАК ДатаСрезаТехКарты
	|ИЗ
	|	РегистрСведений.торо_СтатусыРасчетаПроектныхЗатратНаРемонты КАК торо_СтатусыРасчетаПроектныхЗатратНаРемонты
	|ГДЕ
	|	торо_СтатусыРасчетаПроектныхЗатратНаРемонты.ДокументЗатрат В(&ДокументЗатрат)";
	
	Запрос.УстановитьПараметр("ДокументЗатрат", ПараметрКоманды);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		торо_ЗаполнениеДокументов20.ЗаполнитьДокументПроектныеЗатратыНаРемонты(Выборка.ДокументЗатрат, Выборка);
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти