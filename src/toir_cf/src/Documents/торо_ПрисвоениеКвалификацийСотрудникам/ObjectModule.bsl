#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПЕРЕМЕННЫЕ

перем СтруктураДанных Экспорт;  // Структура, хранящая данные для работы с уведомлениями.
Перем БезусловнаяЗапись Экспорт; // Отключает проверки при записи документа

#Область ОбработчикиСобытий
Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	Если КвалификацииСотрудников.Количество() = 0 Тогда
		ТекстСообщения = НСтр("ru = 'В документе не заполнена табличная часть квалификаций сотрудников. Проведение невозможно!'");
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения,,,, Отказ);
		Возврат;
	КонецЕсли;
	
	// Заголовок для сообщений об ошибках проведения.
	Заголовок = Строка(Ссылка);
	
	Если Не Отказ Тогда
		
		Если ВидОперации = Перечисления.торо_ВидыОперацийСКвалификациями.УдалениеКвалификаций Тогда
			
			Для Каждого СтрокаТЧ Из КвалификацииСотрудников Цикл
				
				// регистр торо_КвалификацияРемонтногоПерсонала 
				Движение = Движения.торо_КвалификацияРемонтногоПерсонала.Добавить();
				Движение.Период       = Дата;
				Движение.Сотрудник    = СтрокаТЧ.Сотрудник;
				Движение.Квалификация = СтрокаТЧ.Квалификация;
				Движение.Удаленная 	  = Истина;
				
				// Перевыбор основной квалификации
				Если ЗначениеЗаполнено(СтрокаТЧ.НоваяОсновная) Тогда
					
					Движение = Движения.торо_КвалификацияРемонтногоПерсонала.Добавить();
					Движение.Период       = Дата;
					Движение.Сотрудник    = СтрокаТЧ.Сотрудник;
					Движение.Квалификация = СтрокаТЧ.НоваяОсновная;
					Движение.Основная 	  = Истина;
					
				КонецЕсли;
				
			КонецЦикла;
			
		Иначе
			
			Запрос = Новый Запрос;
			Запрос.УстановитьПараметр("МассивСотрудников", КвалификацииСотрудников.ВыгрузитьКолонку("Сотрудник"));
			Запрос.УстановитьПараметр("Дата", Дата);
			Запрос.Текст = "ВЫБРАТЬ
			               |	торо_КвалификацияРемонтногоПерсонала.Квалификация КАК Квалификация,
			               |	торо_КвалификацияРемонтногоПерсонала.Сотрудник КАК Сотрудник
			               |ИЗ
			               |	РегистрСведений.торо_КвалификацияРемонтногоПерсонала.СрезПоследних(&Дата, ) КАК торо_КвалификацияРемонтногоПерсонала
			               |ГДЕ
			               |	торо_КвалификацияРемонтногоПерсонала.Сотрудник В(&МассивСотрудников)
			               |	И торо_КвалификацияРемонтногоПерсонала.Основная = ИСТИНА
			               |	И торо_КвалификацияРемонтногоПерсонала.Удаленная = ЛОЖЬ";
			
			Результат = Запрос.Выполнить();
			ОсновныеКвалификацииСотрудников = Результат.Выгрузить();
			
			Для Каждого СтрокаТЧ Из КвалификацииСотрудников Цикл
				
				ОтборПоСотруднику = Новый Структура("Сотрудник", СтрокаТЧ.Сотрудник);
				ОКТекСотрудника = ОсновныеКвалификацииСотрудников.НайтиСтроки(ОтборПоСотруднику);
				
				ЭтоЕдинственнаяОК = Ложь;
				Если НЕ СтрокаТЧ.Основная И ОКТекСотрудника.Количество() = 1 Тогда
					Если ОКТекСотрудника[0].Квалификация = СтрокаТЧ.Квалификация Тогда
						ЭтоЕдинственнаяОК = Истина;
					КонецЕсли;
				КонецЕсли;
				
				Если НЕ ОКТекСотрудника.Количество() ИЛИ ЭтоЕдинственнаяОК Тогда
					Отбор = Новый Структура("Сотрудник, Основная", СтрокаТЧ.Сотрудник, Истина);
					СтрокиСотрудника = КвалификацииСотрудников.НайтиСтроки(Отбор);
					Если НЕ СтрокиСотрудника.Количество() Тогда
						СтрокаТЧ.Основная = Истина;
						ТекОсновнаяКвалификация = торо_ПроцедурыУправленияПерсоналом.ПолучитьОсновнуюКвалификацию(СтрокаТЧ.Сотрудник, ЭтотОбъект.Ссылка, ЭтотОбъект.Дата);
						СтрокаТЧ.НоваяОсновная = ТекОсновнаяКвалификация;
					КонецЕсли;
				КонецЕсли;
			
				// регистр торо_КвалификацияРемонтногоПерсонала 
				Движение = Движения.торо_КвалификацияРемонтногоПерсонала.Добавить();
				Движение.Период       = Дата;
				Движение.Сотрудник    = СтрокаТЧ.Сотрудник;
				Движение.Квалификация = СтрокаТЧ.Квалификация;
				Движение.Основная = СтрокаТЧ.Основная;
				
				Если СтрокаТЧ.Основная И ЗначениеЗаполнено(СтрокаТЧ.НоваяОсновная)
					И СтрокаТЧ.Квалификация <> СтрокаТЧ.НоваяОсновная Тогда
					Движение = Движения.торо_КвалификацияРемонтногоПерсонала.Добавить();
					Движение.Период       = Дата;
					Движение.Сотрудник    = СтрокаТЧ.Сотрудник;
					Движение.Квалификация = СтрокаТЧ.НоваяОсновная;
					Движение.Основная = Ложь;
				КонецЕсли;
				
			КонецЦикла;
			
		КонецЕсли;
		
		Движения.торо_КвалификацияРемонтногоПерсонала.Записать();
		
	КонецЕсли;
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	Если ЭтоНовый() Тогда
		Автор = Пользователи.ТекущийПользователь();
	КонецЕсли;
КонецПроцедуры
#КонецОбласти


#КонецЕсли