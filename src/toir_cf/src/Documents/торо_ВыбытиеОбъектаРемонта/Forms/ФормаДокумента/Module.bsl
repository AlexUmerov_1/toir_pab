#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Заголовок формы++
	торо_РаботаСДиалогами.УстановитьЗаголовокФормыДокумента(Объект, ЭтаФорма, "");
	// Заголовок формы--	

	// СтандартныеПодсистемы.Свойства
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("ИмяЭлементаДляРазмещения", "ГруппаДополнительныеРеквизиты");
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтотОбъект, ДополнительныеПараметры);
	// Конец СтандартныеПодсистемы.Свойства

	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	УстановитьУсловноеОформление();
	
	Если НЕ ЗначениеЗаполнено(Объект.Ссылка) Тогда
		Если ЗначениеЗаполнено(Объект.ОбъектРемонта) Тогда
			ЗаполнитьВходилВСоставПоТекущейИерархии();
		КонецЕсли;
		СлужебныеРеквизитыЗаполнитьНаСервере();
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Объект.ОбъектРемонта) Тогда
		ЗаполнитьДатуВводаВЭксплуатацию();
	КонецЕсли;
		
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.УправлениеДоступом

	СлужебныеРеквизитыЗаполнитьНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// СтандартныеПодсистемы.Свойства
    УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

	УстановитьУсловноеОформление();
	Элементы.ИзменитьИерархию.Заголовок = Объект.ТекСтруктураИерархии;
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	// Конец СтандартныеПодсистемы.Свойства

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
	// Заголовок формы++
	торо_РаботаСДиалогами.УстановитьЗаголовокФормыДокумента(Объект, ЭтаФорма, "");
	// Заголовок формы--	

	СлужебныеРеквизитыЗаполнитьНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("СОЗДАН_ДОКУМЕНТ_СНЯТИЕ_С_УЧЕТА",Объект.ОбъектРемонта,ЭтаФорма.ВладелецФормы);
	ПодключаемыеКомандыКлиент.ПослеЗаписи(ЭтотОбъект, Объект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// СтандартныеПодсистемы.Свойства 
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтотОбъект, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
		УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.Свойства

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ОбъектРемонтаПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.ОбъектРемонта) Тогда
		ЗаполнитьДатуВводаВЭксплуатацию();
		ЗаполнитьВходилВСоставПоТекущейИерархии();
	Иначе
		ДатаВводаВЭксплуатацию = Дата(1,1,1);
		Объект.ВходилВСостав = "";
	КонецЕсли; 
	
	ОбновитьПодчиненныеНаКлиенте();

КонецПроцедуры  

&НаКлиенте
Процедура ПодразделениеПриИзменении(Элемент)
	
	СкладДоИзменения = Объект.Склад;
	торо_ЗаполнениеДокументовКлиент.ЗаполнитьСклад(Объект.Подразделение, Объект.Склад);
	
	Если НЕ СкладДоИзменения = Объект.Склад Тогда 
		СкладПриИзменении(Элементы.Склад);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария(
		Элемент.ТекстРедактирования, ЭтотОбъект, "Объект.Комментарий");

КонецПроцедуры

&НаКлиенте
Процедура ОбъектРемонтаОткрытие(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Если ЗначениеЗаполнено(Объект.ОбъектРемонта) Тогда
		ПараметрыФормы = Новый Структура("Ключ, СтруктураИерархии", Объект.ОбъектРемонта, Объект.ТекСтруктураИерархии);
		ОткрытьФорму("Справочник.торо_ОбъектыРемонта.ФормаОбъекта", ПараметрыФормы);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СкладПриИзменении(Элемент)
	
	ОповещениеПользователя = Новый ОписаниеОповещения("ОбработкаВыбораПользователя", ЭтаФорма, Новый Структура("Склад",Объект.Склад));
	ПоказатьВопрос(ОповещениеПользователя,НСтр("ru = 'Заполнить поле <Склад> в табличной части <Номенклатура>?'"),РежимДиалогаВопрос.ДаНет,,,НСтр("ru = 'Указать склад'"));
	
КонецПроцедуры

&НаКлиенте
Процедура ОбъектРемонтаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	СписокДоступныхСтатусов = Новый Массив;
	СписокДоступныхСтатусов.Добавить(ПредопределенноеЗначение("Перечисление.торо_СтатусыОРВУчете.НеПринятоКУчету"));
	СписокДоступныхСтатусов.Добавить(ПредопределенноеЗначение("Перечисление.торо_СтатусыОРВУчете.ПринятоКУчету"));
	ПараметрыОтбора = Новый Структура("СписокСтатусов", СписокДоступныхСтатусов);
	ПараметрыОтбора.Вставить("СтруктураИерархии", Объект.ТекСтруктураИерархии);	
	Если ЗначениеЗаполнено(Объект.ОбъектРемонта) Тогда
		ПараметрыОтбора.Вставить("ТекущаяСтрока", Объект.ОбъектРемонта);
	КонецЕсли;
		
	ОткрытьФорму("Справочник.торо_ОбъектыРемонта.ФормаВыбора", ПараметрыОтбора, Элемент,,
		ВариантОткрытияОкна.ОтдельноеОкно,,, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыНоменклатура

&НаКлиенте
Процедура НоменклатураПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	ТекДанные = Элементы.Номенклатура.ТекущиеДанные;
	Если НоваяСтрока И ТекДанные <> Неопределено И ЗначениеЗаполнено(Объект.Склад) И Не ЗначениеЗаполнено(ТекДанные.Склад) Тогда
		ТекДанные.Склад = Объект.Склад;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура НоменклатураПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Номенклатура.ТекущиеДанные;
	
	ТекущаяСтрока.ХарактеристикиИспользуются = торо_НоменклатураСервер.ПолучитьХарактеристикиИспользуются(ТекущаяСтрока.Номенклатура);
	
	Если Не ТекущаяСтрока.ХарактеристикиИспользуются Тогда
		ТекущаяСтрока.ХарактеристикаНоменклатуры = Неопределено;
	КонецЕсли;       
		
	ТекущаяСтрока.СерииИспользуются = торо_НоменклатураСервер.ПолучитьСерииИспользуются(ТекущаяСтрока.Номенклатура);
	ТекущаяСтрока.Серия = Неопределено;
	
КонецПроцедуры

&НаКлиенте
Процедура НоменклатураОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ТекДанные = Элементы.Номенклатура.ТекущиеДанные;
	Если Не ТекДанные = Неопределено Тогда
		
		СтрокиСВыбраннойНоменклатурой = Объект.СписокНоменклатуры.НайтиСтроки(Новый Структура("Номенклатура, ХарактеристикаНоменклатуры",ВыбранноеЗначение, ТекДанные.ХарактеристикаНоменклатуры));
		Если СтрокиСВыбраннойНоменклатурой.Количество() > 0 Тогда
			СтандартнаяОбработка = Ложь;
			ТекДанные.Номенклатура = ПредопределенноеЗначение("Справочник.Номенклатура.ПустаяСсылка");
			ОбщегоНазначенияКлиент.СообщитьПользователю(НСтр("ru = 'Выбранная номенклатура с характеристикой уже имеется.'"));
			Возврат;
		КонецЕсли;			
		
	КонецЕсли; 

КонецПроцедуры

&НаКлиенте
Процедура НоменклатураХарактеристикаНоменклатурыСоздание(Элемент, СтандартнаяОбработка)
	
	Если Элементы.Номенклатура.ТекущиеДанные <> Неопределено И ЗначениеЗаполнено(Элементы.Номенклатура.ТекущиеДанные.Номенклатура) Тогда	
		Вид = торо_ОбщегоНазначенияВызовСервера.ЗначениеРеквизитаОбъекта(Элементы.Номенклатура.ТекущиеДанные.Номенклатура, "ВидНоменклатуры");
		СтруктураПараметров = Новый Структура("ВидНоменклатуры, Владелец", Вид, Элементы.Номенклатура.ТекущиеДанные.Номенклатура);
		ОткрытьФорму("Справочник.ХарактеристикиНоменклатуры.Форма.ФормаЭлемента", СтруктураПараметров);
		СтандартнаяОбработка = Ложь;
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура НоменклатураХарактеристикаНоменклатурыОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ТекДанные = Элементы.Номенклатура.ТекущиеДанные;
	Если Не ТекДанные = Неопределено Тогда
		
		СтрокиСВыбраннойНоменклатурой = Объект.СписокНоменклатуры.НайтиСтроки(Новый Структура("Номенклатура, ХарактеристикаНоменклатуры", ТекДанные.Номенклатура,ВыбранноеЗначение));
		Если СтрокиСВыбраннойНоменклатурой.Количество() > 0 Тогда
			СтандартнаяОбработка = Ложь;
			ТекДанные.ХарактеристикаНоменклатуры = ПредопределенноеЗначение("Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка");
			ОбщегоНазначенияКлиент.СообщитьПользователю(НСтр("ru = 'Выбранная номенклатура с характеристикой уже имеется.'"));
			Возврат;
		КонецЕсли;			
		
	КонецЕсли; 

КонецПроцедуры

&НаКлиенте
Процедура НоменклатураХарактеристикаНоменклатурыАвтоПодбор(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка)
	
	ТекущиеДанные = Элементы.Номенклатура.ТекущиеДанные;
	Если ТекущиеДанные <> Неопределено Тогда
		
		ТекущаяНоменклатура = ТекущиеДанные.Номенклатура;
		ВидНоменклатуры = "";
		ИспользованиеХарактеристик = "";
		ИспользованиеХарактеристикИВидНоменклатуры(ТекущаяНоменклатура, ВидНоменклатуры, ИспользованиеХарактеристик);
		
		ПараметрыПолученияДанных.Отбор.Очистить();
		Если ИспользованиеХарактеристик = ПредопределенноеЗначение("Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.ОбщиеДляВидаНоменклатуры") Тогда
			ПараметрыПолученияДанных.Отбор.Вставить("Владелец", ВидНоменклатуры);
		Иначе
			ПараметрыПолученияДанных.Отбор.Вставить("Владелец", ТекущиеДанные.Номенклатура);
		КонецЕсли; 
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура НоменклатураХарактеристикаНоменклатурыОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, СтандартнаяОбработка)
	
	ТекущиеДанные = Элементы.Номенклатура.ТекущиеДанные;
	Если ТекущиеДанные <> Неопределено Тогда
		
		ТекущаяНоменклатура = ТекущиеДанные.Номенклатура;
		ВидНоменклатуры = "";
		ИспользованиеХарактеристик = "";
		ИспользованиеХарактеристикИВидНоменклатуры(ТекущаяНоменклатура, ВидНоменклатуры, ИспользованиеХарактеристик);		
		
		ПараметрыПолученияДанных.Отбор.Очистить();
		Если ИспользованиеХарактеристик = ПредопределенноеЗначение("Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.ОбщиеДляВидаНоменклатуры") Тогда
			ПараметрыПолученияДанных.Отбор.Вставить("Владелец", ВидНоменклатуры);
		Иначе
			ПараметрыПолученияДанных.Отбор.Вставить("Владелец", ТекущиеДанные.Номенклатура);
		КонецЕсли; 
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура НоменклатураСерияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)

	СтандартнаяОбработка = Ложь; 
	
	ТекущиеДанные =  Элементы.Номенклатура.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;

	ВидНоменклатуры = торо_ОбщегоНазначенияВызовСервера.ЗначениеРеквизитаОбъекта(ТекущиеДанные.Номенклатура, "ВидНоменклатуры");
	ПараметрыФормы = Новый Структура("Отбор", Новый Структура("ВидНоменклатуры", ВидНоменклатуры));  
	
	ОткрытьФорму("Справочник.СерииНоменклатуры.ФормаВыбора", ПараметрыФормы, Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура НоменклатураСерияСоздание(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь; 
	
	ТекущиеДанные =  Элементы.Номенклатура.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ВидНоменклатуры = торо_ОбщегоНазначенияВызовСервера.ЗначениеРеквизитаОбъекта(ТекущиеДанные.Номенклатура, "ВидНоменклатуры");
	ПараметрыФормы = Новый Структура("Отбор", Новый Структура("ВидНоменклатуры", ВидНоменклатуры)); 
	
	ОткрытьФорму("Справочник.СерииНоменклатуры.Форма.ФормаЭлемента", ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура НоменклатураСерияОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ТекущиеДанные =  Элементы.Номенклатура.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено ИЛИ Не ЗначениеЗаполнено(ВыбранноеЗначение) Тогда
		Возврат;
	КонецЕсли;

	МассивНоменклатур = Новый Массив;
	МассивНоменклатур.Добавить(ТекущиеДанные.Номенклатура);
	МассивНоменклатур.Добавить(ВыбранноеЗначение);
	
	РеквизитыНоменклатур = торо_ОбщегоНазначенияВызовСервера.ЗначениеРеквизитаОбъектов(МассивНоменклатур, "ВидНоменклатуры");
	
	ВидНоменклатуры = РеквизитыНоменклатур.Получить(ТекущиеДанные.Номенклатура);
	ВидНоменклатурыВыбранный = РеквизитыНоменклатур.Получить(ВыбранноеЗначение);
	
	Если Не ВидНоменклатуры = ВидНоменклатурыВыбранный Тогда
		СтандартнаяОбработка = Ложь;
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура НоменклатураСерияАвтоПодбор(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка)
	
	Если Ожидание > 0 Тогда
		ТекущиеДанные = Элементы.Номенклатура.ТекущиеДанные;
		ВидНоменклатуры = торо_ОбщегоНазначенияВызовСервера.ЗначениеРеквизитаОбъекта(ТекущиеДанные.Номенклатура, "ВидНоменклатуры");
		ПараметрыПолученияДанных.Отбор.Вставить("ВидНоменклатуры",ВидНоменклатуры);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура НоменклатураСерияОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, СтандартнаяОбработка)
	
	ТекущиеДанные = Элементы.Номенклатура.ТекущиеДанные;
	ВидНоменклатуры = торо_ОбщегоНазначенияВызовСервера.ЗначениеРеквизитаОбъекта(ТекущиеДанные.Номенклатура, "ВидНоменклатуры");
	ПараметрыПолученияДанных.Отбор.Вставить("ВидНоменклатуры",ВидНоменклатуры);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.НачатьВыполнениеКоманды(ЭтотОбъект, Команда, Объект);
КонецПроцедуры
 
&НаКлиенте
Процедура Подключаемый_ПродолжитьВыполнениеКомандыНаСервере(ПараметрыВыполнения, ДополнительныеПараметры) Экспорт
	ВыполнитьКомандуНаСервере(ПараметрыВыполнения);
КонецПроцедуры
 
&НаСервере
Процедура ВыполнитьКомандуНаСервере(ПараметрыВыполнения)
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, ПараметрыВыполнения, Объект);
КонецПроцедуры
 
&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

// СтандартныеПодсистемы.Свойства
&НаКлиенте
Процедура Подключаемый_СвойстваВыполнитьКоманду(ЭлементИлиКоманда, НавигационнаяСсылка = Неопределено, СтандартнаяОбработка = Неопределено)
	УправлениеСвойствамиКлиент.ВыполнитьКоманду(ЭтотОбъект, ЭлементИлиКоманда, СтандартнаяОбработка);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Свойства

&НаКлиенте
Процедура ИзменитьИерархию(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ОбработкаВыбораИерахии", ЭтаФорма);
	
	ПараметрыОткрытия = Новый Структура;
	ПараметрыОткрытия.Вставить("ТекущаяСтрока", Объект.ТекСтруктураИерархии);

	ОткрытьФорму("Справочник.торо_СтруктурыОР.ФормаВыбора", ПараметрыОткрытия, ЭтаФорма, ЭтаФорма.УникальныйИдентификатор,,,ОписаниеОповещения, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);

КонецПроцедуры

&НаКлиенте
Процедура ОбновитьПодчиненные(Команда)
	ОбновитьПодчиненныеНаКлиенте();
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.Свойства 
&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьЗависимостиДополнительныхРеквизитов()
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПриИзмененииДополнительногоРеквизита(Элемент)
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Свойства

&НаСервере
Процедура СлужебныеРеквизитыЗаполнитьНаСервере()
	
	ПараметрыЗаполненияРеквизитов = Новый Структура;
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьПризнакХарактеристикиИспользуются",
											Новый Структура("Номенклатура", "ХарактеристикиИспользуются"));
	ПараметрыЗаполненияРеквизитов.Вставить("ЗаполнитьПризнакСерииИспользуются",
											Новый Структура("Номенклатура", "СерииИспользуются"));
										
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(Объект.СписокНоменклатуры, ПараметрыЗаполненияРеквизитов);									
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбораИерахии(Результат, Параметры) Экспорт
	Если ЗначениеЗаполнено(Результат) Тогда 
		ЭтаФорма.Модифицированность = Истина;
		Объект.ТекСтруктураИерархии = Результат;
		Элементы.ИзменитьИерархию.Заголовок = Объект.ТекСтруктураИерархии;
		
		Если ЗначениеЗаполнено(Объект.ОбъектРемонта) Тогда
			ЗаполнитьВходилВСоставПоТекущейИерархии();
		Иначе
			Объект.ВходилВСостав = "";
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеОТекущемПоложенииНаСервере()
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	торо_СтруктурыОР.Ссылка КАК Ссылка,
	               |	торо_СтруктурыОР.ИзменяетсяДокументами КАК ИзменяетсяДокументами,
	               |	торо_СтруктурыОР.СтроитсяАвтоматически КАК СтроитсяАвтоматически,
	               |	торо_СтруктурыОР.РеквизитОР КАК РеквизитОР,
	               |	торо_СтруктурыОР.ТипРеквизитаОР КАК ТипРеквизитаОР
	               |ИЗ
	               |	Справочник.торо_СтруктурыОР КАК торо_СтруктурыОР
	               |ГДЕ
	               |	НЕ торо_СтруктурыОР.ЭтоГруппа";
	
	Результат = Запрос.Выполнить();
	Выборка = Результат.Выбрать();
	
	СписокИерархий.Очистить();
	ТаблицаТекущихПоложений.Очистить();
	
	Пока Выборка.Следующий() Цикл
		
		СписокИерархий.Добавить(Выборка.Ссылка);
		
		НС = ТаблицаТекущихПоложений.Добавить();
		НС.СтруктураИерархии = Выборка.Ссылка;
		
		СписокРодителей = Новый ТаблицаЗначений;
		СписокРодителей.Колонки.Добавить("Номер");
		СписокРодителей.Колонки.Добавить("ТекстовоеОписание");
		
		МассивРодителей = Новый Массив;
		торо_РаботаСИерархией20.ЗаполнитьМассивРодителейОбъектаРемонта(Объект.ОбъектРемонта, МассивРодителей, Выборка.Ссылка);
		
		Номер = 1;
		Для каждого Родитель из МассивРодителей Цикл
			НоваяСтрока = СписокРодителей.Добавить();
			НоваяСтрока.ТекстовоеОписание = Строка(Родитель);
			НоваяСтрока.Номер = Номер;
			Номер = Номер + 1;
		КонецЦикла;
		
		СписокРодителей.Сортировать("Номер Убыв");
		
		ТекстовоеОписаниеПоложения = "";
		
		КолТабов = 0;
		
		Для каждого Строка Из СписокРодителей Цикл
			
			КолОтступов = КолТабов;
			СтрокаОтступа = "";
			
			Пока Не КолОтступов = 0 Цикл
				СтрокаОтступа = СтрокаОтступа + "	";
				КолОтступов = КолОтступов - 1;
			КонецЦикла;
			
			Если КолТабов = 0 Тогда
				ТекстовоеОписаниеПоложения = СтрокаОтступа + Строка(Строка.ТекстовоеОписание);
			Иначе
				ТекстовоеОписаниеПоложения = ТекстовоеОписаниеПоложения + Символы.ПС + СтрокаОтступа + Строка(Строка.ТекстовоеОписание);
			КонецЕсли;
			
			КолТабов = КолТабов + 1;
			
		КонецЦикла;
		
		НС.ТекстовоеОписаниеПоложения = ТекстовоеОписаниеПоложения;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьВходилВСоставПоТекущейИерархии()
	
	ЗаполнитьДанныеОТекущемПоложенииНаСервере();
	МассивСтрок = ТаблицаТекущихПоложений.НайтиСтроки(Новый Структура("СтруктураИерархии", Объект.ТекСтруктураИерархии));
	
	Если МассивСтрок.Количество() > 0 Тогда
		Объект.ВходилВСостав = МассивСтрок[0].ТекстовоеОписаниеПоложения;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДатуВводаВЭксплуатацию()
	
	Если ЗначениеЗаполнено(Объект.ОбъектРемонта) Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	МАКСИМУМ(торо_СтатусыОбъектовРемонтаВУчете.Период) КАК ДатаВводаВЭксплуатацию
		|ИЗ
		|	РегистрСведений.торо_СтатусыОбъектовРемонтаВУчете КАК торо_СтатусыОбъектовРемонтаВУчете
		|ГДЕ
		|	торо_СтатусыОбъектовРемонтаВУчете.ОбъектРемонта = &ОбъектРемонта
		|  И торо_СтатусыОбъектовРемонтаВУчете.СтатусОР = ЗНАЧЕНИЕ(Перечисление.торо_СтатусыОРВУчете.ПринятоКУчету)";

		Запрос.УстановитьПараметр("ОбъектРемонта",Объект.ОбъектРемонта);
		Результат = Запрос.Выполнить();
		
		Если Не Результат.Пустой() Тогда
			Выборка = Результат.Выбрать();
			Выборка.Следующий();
			ДатаВводаВЭксплуатацию = Выборка.ДатаВводаВЭксплуатацию;														
		КонецЕсли;
		
	КонецЕсли;														
		
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьСписокПодчиненных(ОР, Ссылка)
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	торо_СтруктурыОР.Ссылка КАК Ссылка
	               |ИЗ
	               |	Справочник.торо_СтруктурыОР КАК торо_СтруктурыОР
	               |ГДЕ
	               |	НЕ торо_СтруктурыОР.ЭтоГруппа";
	
	Результат = Запрос.Выполнить();
	Выборка = Результат.Выбрать();
	
	МассивВсехПодчиненных = Новый Массив();
	Пока Выборка.Следующий() Цикл
		МассивПодчиненных = торо_РаботаСИерархией20.ПолучитьМассивПодчиненныхОбъектов(ОР, Выборка.Ссылка);
		ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивВсехПодчиненных, МассивПодчиненных);
	КонецЦикла;
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	|	торо_ОбъектыРемонта.Ссылка КАК ОбъектРемонта,
	|	торо_ОбъектыРемонта.ЭтоГруппа КАК ЭтоГруппа
	|ПОМЕСТИТЬ ВТ_ОР
	|ИЗ
	|	Справочник.торо_ОбъектыРемонта КАК торо_ОбъектыРемонта
	|ГДЕ
	|	торо_ОбъектыРемонта.Ссылка В(&МассивОР)
	|	И НЕ торо_ОбъектыРемонта.ЭтоГруппа
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОбъектРемонта
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних.ОбъектРемонта КАК ОбъектРемонта,
	|	торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних.СтатусОР КАК СтатусОР
	|ПОМЕСТИТЬ ВТ_СтатусыОР
	|ИЗ
	|	РегистрСведений.торо_СтатусыОбъектовРемонтаВУчете.СрезПоследних(
	|			,
	|			ОбъектРемонта В
	|					(ВЫБРАТЬ
	|						ВТ_ОР.ОбъектРемонта КАК ОбъектРемонта
	|					ИЗ
	|						ВТ_ОР КАК ВТ_ОР)
	|				И Регистратор <> &Ссылка) КАК торо_СтатусыОбъектовРемонтаВУчетеСрезПоследних
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ОбъектРемонта
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТ_ОР.ОбъектРемонта КАК ОбъектРемонта,
	|	ВТ_ОР.ЭтоГруппа КАК ЭтоГруппа,
	|	ВЫБОР
	|		КОГДА ВТ_СтатусыОР.СтатусОР = ЗНАЧЕНИЕ(Перечисление.торо_СтатусыОРВУчете.ПринятоКУчету)
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ КАК НеПринятКУчету
	|ИЗ
	|	ВТ_ОР КАК ВТ_ОР
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВТ_СтатусыОР КАК ВТ_СтатусыОР
	|		ПО ВТ_ОР.ОбъектРемонта = ВТ_СтатусыОР.ОбъектРемонта";
	
	Запрос.УстановитьПараметр("МассивОР", МассивВсехПодчиненных);
	Запрос.УстановитьПараметр("ПринятКУчету", Перечисления.торо_СтатусыОРВУчете.ПринятоКУчету);
	Запрос.УстановитьПараметр("Ссылка", Ссылка);

	Результат = Запрос.Выполнить().Выгрузить();
	
	МассивРезультата = Новый Массив;
	Для каждого Элемент Из Результат Цикл
		МассивРезультата.Добавить(Новый Структура("ОР,НеПринятКУчету",Элемент.ОбъектРемонта, Элемент.НеПринятКУчету));
	КонецЦикла;
		
	Возврат МассивРезультата;
	
КонецФункции

&НаКлиенте
Процедура ОбновитьПодчиненныеНаКлиенте()
	Объект.СписокПодчиненныхСнятыхСУчета.Очистить();
	Если ЗначениеЗаполнено(Объект.ОбъектРемонта) Тогда
		СписокПодчиненных = ПолучитьСписокПодчиненных(Объект.ОбъектРемонта, Объект.Ссылка);
		Для каждого Элемент Из СписокПодчиненных Цикл
			НС = Объект.СписокПодчиненныхСнятыхСУчета.Добавить();
			НС.ОбъектРемонта = Элемент.ОР;
			НС.СнятьСУчета = Не Элемент.НеПринятКУчету;
		КонецЦикла;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВыделитьВсеПодчиненные(Команда)
	Для Каждого Элемент Из Объект.СписокПодчиненныхСнятыхСУчета Цикл
		Элемент.СнятьСУчета = Истина;
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура СнятьВыделениеВсеПодчиненные(Команда)
	Для Каждого Элемент Из Объект.СписокПодчиненныхСнятыхСУчета Цикл
		Элемент.СнятьСУчета = Ложь;
	КонецЦикла;
КонецПроцедуры

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();
	
	// Характеристики.
	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.НоменклатураХарактеристикаНоменклатуры.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.СписокНоменклатуры.ХарактеристикиИспользуются");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Ложь;

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", WebЦвета.НейтральноСерый);
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = '<характеристики не используются>'"));
	Элемент.Оформление.УстановитьЗначениеПараметра("ОтметкаНезаполненного", Ложь);
	Элемент.Оформление.УстановитьЗначениеПараметра("Доступность", Ложь);
	
	// Серии.	
	Элемент = УсловноеОформление.Элементы.Добавить();
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.НоменклатураСерия.Имя);
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.СписокНоменклатуры.СерииИспользуются");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Ложь;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", WebЦвета.НейтральноСерый);
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = '<серии не используются>'"));
	Элемент.Оформление.УстановитьЗначениеПараметра("ОтметкаНезаполненного", Ложь);
	Элемент.Оформление.УстановитьЗначениеПараметра("Доступность", Ложь);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьТЧНаСервере(Склад)
	Для каждого Строка Из Объект.СписокНоменклатуры Цикл
		Строка.Склад = Склад;
	КонецЦикла; 	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбораПользователя(Результат, ДопПараметры) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		ЗаполнитьТЧНаСервере(ДопПараметры.Склад);
	КонецЕсли; 
		
КонецПроцедуры

&НаСервере
Процедура ИспользованиеХарактеристикИВидНоменклатуры(ТекущаяНоменклатура, ВидНоменклатуры, ИспользованиеХарактеристик)
	
	ВидНоменклатуры = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ТекущаяНоменклатура, "ВидНоменклатуры");
	ИспользованиеХарактеристик = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ВидНоменклатуры, "ИспользованиеХарактеристик");	
	
КонецПроцедуры

#КонецОбласти

