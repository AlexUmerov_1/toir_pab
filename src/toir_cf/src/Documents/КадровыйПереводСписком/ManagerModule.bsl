#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	// Приказ о переводе
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.МенеджерПечати = "Обработка.торо_ПечатьКадровыхПриказов";
	КомандаПечати.Идентификатор = "ПФ_MXL_Т5а";
	КомандаПечати.Представление = НСтр("ru = 'Приказ о переводе (T-5a)'");
	КомандаПечати.ДополнительныеПараметры.Вставить("ТребуетсяЧтениеБезОграничений", Истина);
	КомандаПечати.ПроверкаПроведенияПередПечатью = ОбщегоНазначенияВызовСервера.ХранилищеОбщихНастроекЗагрузить(
			"НастройкиТОиР",
			"ПечатьДокументовБезПредварительногоПросмотра",
			Ложь);
	
КонецПроцедуры

Функция СписокПодходящихСорудников(ДатаНачала, ДатаОкончания, Организация, Текст) Экспорт 
	
	ПараметрыПолученияСотрудниковОрганизаций = Новый Структура(
		"КадровыеДанные, НачалоПериода, ОкончаниеПериода, Организация, ОтбиратьПоГоловнойОрганизации, Подразделение, РаботникиПоДоговорамГПХ, РаботникиПоТрудовымДоговорам, СписокФизическихЛиц",
		"", ДатаНачала, ДатаОкончания, Организация, Ложь, Неопределено, Неопределено, Истина, Неопределено);
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	Сотрудники.Ссылка КАК Ссылка
	               |ИЗ
	               |	Справочник.Сотрудники КАК Сотрудники
	               |ГДЕ
	               |	Сотрудники.Ссылка В(&Сотрудники)
	               |	И Сотрудники.Наименование ПОДОБНО (&Текст + ""%"")";
	
	Запрос.УстановитьПараметр("Сотрудники", КадровыйУчет.СотрудникиОрганизации(Истина, ПараметрыПолученияСотрудниковОрганизаций));
	Запрос.УстановитьПараметр("Текст", Текст);
	
	резЗапроса = Запрос.Выполнить();
	
	Если резЗапроса.Пустой() Тогда
		Возврат Новый Массив;
	КонецЕсли;
	
	Возврат резЗапроса.Выгрузить().ВыгрузитьКолонку("Ссылка");
	
КонецФункции

#КонецОбласти

#КонецЕсли