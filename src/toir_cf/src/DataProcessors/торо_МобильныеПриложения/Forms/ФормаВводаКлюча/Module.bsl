#Область ОбработчикиКомандФормы
&НаКлиенте
Процедура ВвестиКлюч(Команда)
	
	Введен = ВвестиКлючНаСервере();
	
	Если Введен Тогда
		
		Оповестить("ВведенКлюч");
		ПоказатьВопрос(Новый ОписаниеОповещения(), НСтр("ru = 'Ключ введен успешно'"), РежимДиалогаВопрос.ОК);
		
	КонецЕсли;
	
КонецПроцедуры
#КонецОбласти

#Область СлужебныеПроцедурыИФункции
&НаСервере
Функция ВвестиКлючНаСервере()
	
	Возврат Обработки.торо_МобильныеПриложения.ВвестиКлюч_Session(Ключ);
	
КонецФункции
#КонецОбласти
