
&НаКлиенте
Перем ОбновитьИнтерфейс;

&НаКлиенте
Перем ВыполняетсяЗакрытие;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ТекущаяДатаСеанса = ТекущаяДатаСеанса();
	ТекущаяОрганизация = Справочники.Организации.ОрганизацияПоУмолчанию();
	
	Элементы.СтраницыДекорацийНавигации.ОтображениеСтраниц = ОтображениеСтраницФормы.Нет;
	Элементы.СтраницыШагов.ОтображениеСтраниц = ОтображениеСтраницФормы.Нет;
	Элементы.СтраницыКомандныхПанелей.ОтображениеСтраниц = ОтображениеСтраницФормы.Нет;
	Элементы.СтраницыСовпадениеПаролей.ОтображениеСтраниц = ОтображениеСтраницФормы.Нет;
	Элементы.СтраницыОшибкаЭлектронногоАдреса.ОтображениеСтраниц = ОтображениеСтраницФормы.Нет;
	
	ИнициализироватьНачальныеНастройки();
	ИнициализироватьКонтактнуюИнформацию();
	ИнициализироватьКонстантыТОиР();
	
	ПрочитатьДанныеОрганизации();
	
	РеквизитыОрганизации = Новый Структура("
		|ЮридическоеФизическоеЛицо, 
		|ИНН, 
		|КПП, 
		|Префикс");
	ЗаполнитьЗначенияСвойств(РеквизитыОрганизации, ЭтаФорма);
	
	Если ЗначениеЗаполнено(Справочники.Организации.ОрганизацияПоУмолчанию()) Тогда
		ОбновитьСтрокуРеквизитовОрганизации(ЭтаФорма,РеквизитыОрганизации);
	КонецЕсли;
	
	УстановитьНазваниеКомандыРеквизитовОрганизации(ЭтаФорма);
	
	ДобавитьСтраницуПерехода(СтраницыПереходов, Элементы.СтраницаОбщаяИнформация, Ложь,, "НаименованиеПользователя");
	ДобавитьСтраницуПерехода(СтраницыПереходов, Элементы.СтраницаВариантНастройки, ,, "ОткрытьПомощникПереносаДанных");
	ДобавитьСтраницуПерехода(СтраницыПереходов, Элементы.СтраницаСведенияОрганизации,,, "НаименованиеПолное");
	ДобавитьСтраницуПерехода(СтраницыПереходов, Элементы.СтраницаКонтакты,,, "");
	ДобавитьСтраницуПерехода(СтраницыПереходов, Элементы.СтраницаТОиР,,, "НаборКонстантВалютаУправленческогоУчета");
	ДобавитьСтраницуПерехода(СтраницыПереходов, Элементы.СтраницаПользователиИПрава,,, "");
	ДобавитьСтраницуПерехода(СтраницыПереходов, Элементы.СтраницаНастройкаИнтерфейса,,, "");
	ДобавитьСтраницуПерехода(СтраницыПереходов, Элементы.СтраницаЗавершение, Ложь,, "НадписьЗавершение");
	
	УстановитьЦветЗаголовкаПоляОшибки();
	УстановитьНазваниеКомандыРеквизитовОрганизации(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если НЕ ВыполняетсяЗакрытие Тогда
		
		Если Не ДанныеЗаписаны Тогда 
			Отказ = Истина;
			Если НЕ ЗавершениеРаботы Тогда
				ОткрытьФорму("Обработка.торо_ПомощникНастройкиПараметровБазовойВерсии.Форма.ДиалогОтмены", , ЭтаФорма,,,, Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект), РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
			КонецЕсли;
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат Тогда
		ВыполняетсяЗакрытие = Истина;
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура НаименованиеПользователяПриИзменении(Элемент)
	ПользовательИнфБазыИмя =
		ПользователиСлужебныйКлиентСервер.ПолучитьКраткоеИмяПользователяИБ(НаименованиеПользователя);
		
	// Заполним реквизиты организации
	Наименование = НаименованиеПользователя;
	НаименованиеПолное = НаименованиеПользователя;
	НаименованиеСокращенное = СокращенноеФИО(НаименованиеПолное);
	
	Если Не ПустаяСтрока(НаименованиеПользователя) Тогда
		Элементы.НадписьФИО.ЦветТекста = ЦветЗаголовкаАвто;
	КонецЕсли;
	Если Не ПустаяСтрока(ПользовательИнфБазыИмя) Тогда
		Элементы.ПользовательИнфБазыИмя.ЦветТекстаЗаголовка = ЦветЗаголовкаАвто;
	КонецЕсли;
	
	ПроверитьЗаполнениеКлиент();
КонецПроцедуры

&НаКлиенте
Процедура ПользовательИнфБазыИмяПриИзменении(Элемент)
	ПроверитьЗаполнениеКлиент();
КонецПроцедуры

&НаКлиенте
Процедура ПодтверждениеПароляПриИзменении(Элемент)
	ПриИзмененииПароля();
КонецПроцедуры

&НаКлиенте
Процедура НаименованиеПолноеПриИзменении(Элемент)
	
	Если ЮридическоеФизическоеЛицо = 1 Тогда
		НаименованиеСокращенное = СокращенноеФИО(Наименование);
	Иначе
		НаименованиеСокращенное = Наименование;
	КонецЕсли;
		
	ПроверитьЗаполнениеКлиент();
	
КонецПроцедуры

&НаКлиенте
Процедура ИндивидуальныйПредпринимательПриИзменении(Элемент)
	Если ЮридическоеФизическоеЛицо = 1 Тогда
		ЮрФизЛицо = ПредопределенноеЗначение("Перечисление.ЮридическоеФизическоеЛицо.ФизическоеЛицо");
	Иначе
		ЮрФизЛицо = ПредопределенноеЗначение("Перечисление.ЮридическоеФизическоеЛицо.ЮридическоеЛицо");
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура НастройкаИнтерфейса(Команда)
	
	ОткрытьФорму("Обработка.торо_ПанельАдминистрированияТОиР.Форма.Форма", Новый Структура("КлючНазначенияИспользования", "ПомощникСтраницаИнтрефейсИвводДокументов"), ЭтаФорма, ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ИзменитьРеквизитыОрганизации(Команда)
	
	РеквизитыОрганизации = Новый Структура("
		|ЮридическоеФизическоеЛицо, 
		|ИНН, 
		|КПП, 
		|Префикс");
	ЗаполнитьЗначенияСвойств(РеквизитыОрганизации, ЭтаФорма);
	
	ОткрытьФорму("Обработка.торо_ПомощникНастройкиПараметровБазовойВерсии.Форма.РеквизитыОрганизации", РеквизитыОрганизации, ЭтаФорма,,,, Новый ОписаниеОповещения("ИзменитьРеквизитыОрганизацииЗавершение", ЭтотОбъект), РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура ИзменитьРеквизитыОрганизацииЗавершение(ИзмененныеРеквизиты, ДополнительныеПараметры) Экспорт
    
    Если Не ИзмененныеРеквизиты = Неопределено Тогда
        // Заполним значения реквизитов
        Для Каждого ИзмененныйРеквизит Из ИзмененныеРеквизиты Цикл 
            ЭтаФорма[ИзмененныйРеквизит.Ключ] = ИзмененныйРеквизит.Значение;
        КонецЦикла;
        ОбновитьСтрокуРеквизитовОрганизации(ЭтаФорма, ИзмененныеРеквизиты);
    КонецЕсли;
    
    УстановитьНазваниеКомандыРеквизитовОрганизации(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура Далее(Команда)
	ОсуществитьПереходВперед();
КонецПроцедуры
 
&НаКлиенте
Процедура Назад(Команда)
	ОсуществитьПереходНазад();
КонецПроцедуры

&НаКлиенте
Процедура ЗавершитьНастройку(Команда)
	
	ЗаписатьИзмененияВИБ();
	ПоказатьОповещениеПользователя(НСтр("ru = 'Настройка программы завершена'"), , НСтр("ru = 'Минимально необходимая настройка завершена.'"), БиблиотекаКартинок.Информация32);
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура ПомощникЗаполненияНастроекИСправочников(Команда)
	
	ЗаписатьИзмененияВИБ();
	ОткрытьФорму("Обработка.торо_ПомощникЗаполненияНастроекИСправочников.Форма.Форма");
	Закрыть();

КонецПроцедуры

&НаКлиенте
Процедура ОткрытьПомощникПереносаДанных(Команда)
	ПараметрыФормы = Новый Структура("КонфигурацияИсточник", "ТОИР13");
	ОткрытьФорму("Обработка.торо_ПомощникПереносаДанныхИзДругихКонфигураций.Форма", ПараметрыФормы, ЭтотОбъект, Новый УникальныйИдентификатор());
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьПомощникПереносаДанныхИзТОИР20(Команда)
	ПараметрыФормы = Новый Структура("КонфигурацияИсточник", "ТОИР20");
	ОткрытьФорму("Обработка.торо_ПомощникПереносаДанныхИзДругихКонфигураций.Форма", ПараметрыФормы, ЭтотОбъект, Новый УникальныйИдентификатор());
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПриИзмененииРеквизитов

&НаКлиенте 
Процедура ПриИзмененииПароля()
	Если Пароль <> ПодтверждениеПароля Тогда
		Элементы.СтраницыСовпадениеПаролей.ТекущаяСтраница = Элементы.СтраницаПаролиНеСовпадают;
	ИначеЕсли ПустаяСтрока(Пароль) Тогда
		Элементы.СтраницыСовпадениеПаролей.ТекущаяСтраница = Элементы.СтраницаПаролиНеВведены;
	Иначе
		Элементы.СтраницыСовпадениеПаролей.ТекущаяСтраница = Элементы.СтраницаПаролиСовпадают;
	КонецЕсли;
	
	ПроверитьЗаполнениеКлиент();
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста 
Процедура ОбновитьСтрокуРеквизитовОрганизации(Форма, РеквизитыОрганизации = Неопределено)
	
	СтрокаРеквизитовОрганизации = "";
	
	Если Не РеквизитыОрганизации = Неопределено
		И ПустаяСтрока(РеквизитыОрганизации.ИНН + 
			РеквизитыОрганизации.КПП +
			РеквизитыОрганизации.Префикс) Тогда
		
		РеквизитыОрганизации = Неопределено;
	КонецЕсли;
	
	Если Не РеквизитыОрганизации = Неопределено Тогда
		СтрокаРеквизитовОрганизации = СтрокаРеквизитовОрганизации + 
			?(ПустаяСтрока(РеквизитыОрганизации.ИНН), "", НСтр("ru= 'ИНН:'") + Символы.НПП + РеквизитыОрганизации.ИНН); 
		СтрокаРеквизитовОрганизации = СтрокаРеквизитовОрганизации + 
			?(ПустаяСтрока(РеквизитыОрганизации.КПП), "", 
			?(ПустаяСтрока(СтрокаРеквизитовОрганизации), "", ";" + Символы.НПП) +
			НСтр("ru= 'КПП:'") + Символы.НПП + РеквизитыОрганизации.КПП); 
		
		СтрокаРеквизитовОрганизации = СтрокаРеквизитовОрганизации + 
		?(ПустаяСтрока(РеквизитыОрганизации.Префикс), "", 
				?(ПустаяСтрока(СтрокаРеквизитовОрганизации), "", ";" + Символы.НПП) + 
				НСтр("ru= 'Префикс:'") + Символы.НПП + РеквизитыОрганизации.Префикс); 
	КонецЕсли;
	
	Форма.СтрокаРеквизитовОрганизации = СтрокаРеквизитовОрганизации;
	
КонецПроцедуры

#КонецОбласти

#Область ИнициализацияДанных

&НаСервере 
Процедура ИнициализироватьНачальныеНастройки()
	
	ЮридическоеФизическоеЛицо = 1;
	ТекущийИндексПерехода = 0;
	
	ТекущийПользователь = Пользователи.ТекущийПользователь();
	ОписаниеПользователя = Пользователи.СвойстваПользователяИБ(ТекущийПользователь.ИдентификаторПользователяИБ);
	
	Если  ОписаниеПользователя <> Неопределено Тогда
		НаименованиеПользователя = ОписаниеПользователя.ПолноеИмя;
		ПользовательИнфБазыИмя = ОписаниеПользователя.Имя;
		
		// Перенос Ф.И.О. в реквизит организации
		Наименование = НаименованиеПользователя;
		НаименованиеПолное = НаименованиеПользователя;
		НаименованиеСокращенное = СокращенноеФИО(НаименованиеПолное);
	КонецЕсли;	
	
	ЮрФизЛицо = Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо;
	
КонецПроцедуры

&НаСервере
процедура ИнициализироватьКонстантыТОиР()
	
	НаборКонстантОбъект = Константы.СоздатьНабор("ВалютаУправленческогоУчета, 
	|	торо_ВидРемонтаПриВводеНаОснованииВыявленныхДефектов,
	|   торо_ВидРемонтаПриВводеНаОснованииВнешнихОснований,
	|   торо_ТехнологическаяКартаПриВводеНаОснованииВыявленныхДефектов,
	|	торо_ИерархияДляВводаНовыхОР,
	|	торо_ГрафикРемонтныхРабот,
	|	торо_КоличествоДнейСмещенияВДефектеДляПростоев,
	|	торо_ВидЭксплуатацииДляВводаНаОснованииВыявленногоДефекта,
	|	торо_ПричинаЗакрытияРемонтов,
	|	торо_ПричинаЗакрытияЗаявок,
	|	торо_ТипЦеныДляРасчетаСебестоимостиРемонта,
	|	торо_ТипЦеныДляРасчетаФактическойСебестоимостиРемонта,
	|	торо_НаименованиеОРДляПечати,
	|	торо_ПредставлениеВРДляПечати,
	|	торо_ПредставлениеВРДляСтруктурыРЦ");
	НаборКонстантОбъект.Прочитать();
	ЗначениеВРеквизитФормы(НаборКонстантОбъект, "НаборКонстант");

	ИерархияДляНовыхОР = НаборКонстант.торо_ИерархияДляВводаНовыхОР;
	
КонецПроцедуры

#КонецОбласти

#Область Навигация

&НаКлиенте 
Функция ПроверитьЗаполнениеКлиент()
	
	РезультатПроверки = Новый Структура("ЕстьОшибки, ОписаниеОшибки");
	
	ЕстьОшибки = Ложь;
	ОписаниеОшибки = "";
	
	ИмяСтраницыПерехода = СтраницыПереходов.Получить(ТекущийИндексПерехода).СтраницаПерехода;
	Если ИмяСтраницыПерехода = "СтраницаОбщаяИнформация" Тогда
		Если ПустаяСтрока(НаименованиеПользователя) Тогда
			ЕстьОшибки = Истина;
			ОписаниеОшибки = НСтр("ru= 'Не заполнен пользователь.'"); 
			Элементы.НадписьФИО.ЦветТекста = ЦветЗаголовкаПоляОшибки;
		Иначе
			Элементы.НадписьФИО.ЦветТекста = ЦветЗаголовкаАвто;
		КонецЕсли;
		Если ПустаяСтрока(ПользовательИнфБазыИмя) Тогда
			ЕстьОшибки = Истина;
			ОписаниеОшибки = ОписаниеОшибки + 
				?(ПустаяСтрока(ОписаниеОшибки),"", Символы.ПС) + 
				НСтр("ru= 'Не заполнено имя для входа.'"); 
			Элементы.ПользовательИнфБазыИмя.ЦветТекстаЗаголовка = ЦветЗаголовкаПоляОшибки;
		Иначе
			Элементы.ПользовательИнфБазыИмя.ЦветТекстаЗаголовка = ЦветЗаголовкаАвто;
		КонецЕсли;
		Если Не Пароль = ПодтверждениеПароля Тогда
			ЕстьОшибки = Истина;
			ОписаниеОшибки = ОписаниеОшибки + 
				?(ПустаяСтрока(ОписаниеОшибки),"", Символы.ПС) + 
				НСтр("ru= 'Введенные пароли не совпадают.'"); 
			Элементы.Пароль.ЦветТекстаЗаголовка = ЦветЗаголовкаПоляОшибки;
			Элементы.ПодтверждениеПароля.ЦветТекстаЗаголовка = ЦветЗаголовкаПоляОшибки;
		Иначе
			Элементы.Пароль.ЦветТекстаЗаголовка = ЦветЗаголовкаАвто;
			Элементы.ПодтверждениеПароля.ЦветТекстаЗаголовка = ЦветЗаголовкаАвто;
		КонецЕсли;
	ИначеЕсли ИмяСтраницыПерехода = "СтраницаСведенияОрганизации" Тогда
		Если ПустаяСтрока(Наименование) Тогда
			ЕстьОшибки = Истина;
			ОписаниеОшибки = НСтр("ru= 'Не заполнено наименование организации.'"); 
		КонецЕсли;
	ИначеЕсли ИмяСтраницыПерехода = "СтраницаТОиР" Тогда
		Если НаборКонстант.ВалютаУправленческогоУчета.Пустая() Тогда
			ЕстьОшибки = Истина;
			ОписаниеОшибки = НСтр("ru= 'Не заполнена валюта управленческого учета.'"); 
		КонецЕсли;
		Если НаборКонстант.торо_ИерархияДляВводаНовыхОР.Пустая() Тогда
			ЕстьОшибки = Истина;
			ОписаниеОшибки = НСтр("ru= 'Не заполнена иерархия для ввода новых объектов ремонта.'"); 
		КонецЕсли;
	КонецЕсли;
	
	РезультатПроверки.Вставить("ЕстьОшибки", ЕстьОшибки);
	РезультатПроверки.Вставить("ОписаниеОшибки", ОписаниеОшибки);
	
	Возврат РезультатПроверки;
КонецФункции

&НаКлиентеНаСервереБезКонтекста 
Процедура ДобавитьСтраницуПерехода(Переходы, Страница, ПромежуточныйПереход = Истина, ИндексПерехода = Неопределено, ИмяЭлементаПоУмолчанию = "")
	Если ИндексПерехода = Неопределено Тогда
		НовыйПереход = Переходы.Добавить();
	Иначе
		НовыйПереход = Переходы.Вставить(ИндексПерехода);
	КонецЕсли;
	НовыйПереход.ПромежуточныйПереход = ПромежуточныйПереход;
	НовыйПереход.СтраницаПерехода = Страница.Имя;
	НовыйПереход.ИмяЭлементаПоУмолчанию = ИмяЭлементаПоУмолчанию;
КонецПроцедуры

&НаКлиенте 
Процедура ОсуществитьПереходВперед()
	РезультатПроверки = ПроверитьЗаполнениеКлиент();
	
	Если РезультатПроверки.ЕстьОшибки Тогда
		ПараметрыФормы = Новый Структура("ОписаниеОшибки", РезультатПроверки.ОписаниеОшибки);
		ОткрытьФорму("Обработка.торо_ПомощникНастройкиПараметровБазовойВерсии.Форма.ДиалогОшибки", ПараметрыФормы, ЭтаФорма,,,, Неопределено, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
		Возврат;
	КонецЕсли;
	
	ТекущаяСтраница = Элементы.СтраницыШагов.ТекущаяСтраница;
	
	ИндексТекущейСтраницы = ТекущийИндексПерехода;
	
	Если ИндексТекущейСтраницы < СтраницыПереходов.Количество() - 1 Тогда
		Переход = СтраницыПереходов.Получить(ИндексТекущейСтраницы + 1);
		ИмяСтраницыПерехода = Переход.СтраницаПерехода;
		
		Элементы.СтраницыШагов.ТекущаяСтраница = Элементы[ИмяСтраницыПерехода];
		Элементы.СтраницыДекорацийНавигации.ТекущаяСтраница = Элементы[СтрЗаменить(ИмяСтраницыПерехода, "Страница","СтраницаДекорация")];
		
		Если Переход.ПромежуточныйПереход Тогда
			СтраницаКоманднойПанели = Элементы.СтраницаПромежуточныхШагов;
		ИначеЕсли ИндексТекущейСтраницы + 1 = СтраницыПереходов.Количество() - 1 Тогда
			СтраницаКоманднойПанели = Элементы.СтраницаПоследнегоШага;
		КонецЕсли;
		
		Элементы.СтраницыКомандныхПанелей.ТекущаяСтраница = СтраницаКоманднойПанели;
		УстановитьКнопкуПоУмолчанию(СтраницаКоманднойПанели);
		
		ТекущийИндексПерехода = ТекущийИндексПерехода + 1;
		
		Если Не ПустаяСтрока(Переход.ИмяЭлементаПоУмолчанию) Тогда
			ТекущийЭлемент = Элементы[Переход.ИмяЭлементаПоУмолчанию];
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

&НаКлиенте 
Процедура ОсуществитьПереходНазад()
	
	ТекущаяСтраница = Элементы.СтраницыШагов.ТекущаяСтраница;
	
	ИндексТекущейСтраницы = ТекущийИндексПерехода;
	
	Если ИндексТекущейСтраницы > 0 Тогда
		Переход = СтраницыПереходов.Получить(ИндексТекущейСтраницы - 1);
		ИмяСтраницыПерехода = Переход.СтраницаПерехода;
		
		Элементы.СтраницыШагов.ТекущаяСтраница = Элементы[ИмяСтраницыПерехода];
		Элементы.СтраницыДекорацийНавигации.ТекущаяСтраница = Элементы[СтрЗаменить(ИмяСтраницыПерехода, "Страница","СтраницаДекорация")];
		
		Если Переход.ПромежуточныйПереход Тогда 
			СтраницаКоманднойПанели = Элементы.СтраницаПромежуточныхШагов;
		ИначеЕсли ИндексТекущейСтраницы - 1 = 0 Тогда
			СтраницаКоманднойПанели = Элементы.СтраницаПервогоШага;
		КонецЕсли;
		
		Элементы.СтраницыКомандныхПанелей.ТекущаяСтраница = СтраницаКоманднойПанели;
		УстановитьКнопкуПоУмолчанию(СтраницаКоманднойПанели);
		
		ТекущийИндексПерехода = ТекущийИндексПерехода - 1;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте 
Процедура УстановитьКнопкуПоУмолчанию(СтраницаКоманднойПанели)
	Если Не Элементы.СтраницыКомандныхПанелей.ТекущаяСтраница = СтраницаКоманднойПанели Тогда
		Если СтраницаКоманднойПанели = Элементы.СтраницаПромежуточныхШагов Тогда
			Элементы.Далее1.КнопкаПоУмолчанию = Истина;
		ИначеЕсли СтраницаКоманднойПанели = Элементы.СтраницаПервогоШага Тогда
			Элементы.Далее.КнопкаПоУмолчанию = Истина;
		ИначеЕсли СтраницаКоманднойПанели = Элементы.СтраницаПоследнегоШага Тогда
			Элементы.ЗавершитьНастройку.КнопкаПоУмолчанию = Истина;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область РаботыСРеквизитамиОрганизацииРасчетногоСчетаИКассы

&НаСервере 
Функция РеквизитыОрганизации()
	РеквизитыОрганизации = Новый Массив;
	
	// Организация
	РеквизитыОрганизации.Добавить("ИНН");
	РеквизитыОрганизации.Добавить("КПП");
	РеквизитыОрганизации.Добавить("Наименование");
	РеквизитыОрганизации.Добавить("Префикс");
	РеквизитыОрганизации.Добавить("ЮридическоеФизическоеЛицо");
	
	// Прочие
	РеквизитыОрганизации.Добавить("СтрокаРеквизитовОрганизации");
	
	Возврат РеквизитыОрганизации;
КонецФункции

&НаСервере 
Функция СокращенноеФИО(Знач ФИО)
	МассивФИО = ПолучитьМассивФИО(ФИО);
	
	СокращенноеФИО = МассивФИО[0] + Символы.НПП + Лев(МассивФИО[1], 1) + "." + Лев(МассивФИО[2], 1) + ".";
	
	Возврат СокращенноеФИО;
КонецФункции

&НаСервере 
Функция ПолучитьМассивФИО(Знач ФИО)

	МассивФИО = Новый Массив;
	МассивФИО.Добавить("");
	МассивФИО.Добавить("");
	МассивФИО.Добавить("");
	
	МассивПодстрок = СтроковыеФункцииКлиентСервер.РазложитьСтрокуВМассивПодстрок(ФИО, " ");
	Для Индекс = 0 По МассивПодстрок.ВГраница() Цикл
		Если Индекс < 3 Тогда
			МассивФИО[Индекс] = МассивПодстрок[Индекс];
		Иначе
			МассивФИО[2] = МассивФИО[2] + " " + МассивПодстрок[Индекс];
		КонецЕсли;
	КонецЦикла;

	Возврат МассивФИО;
	
КонецФункции

#КонецОбласти

#Область КонтактнаяИнформация

// СтандартныеПодсистемы.КонтактнаяИнформация

&НаКлиенте
Процедура Подключаемый_КонтактнаяИнформацияПриИзменении(Элемент)
	
	УправлениеКонтактнойИнформациейКлиент.НачатьИзменение(ЭтотОбъект, Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_КонтактнаяИнформацияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	УправлениеКонтактнойИнформациейКлиент.НачатьВыбор(ЭтотОбъект, Элемент, , СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_КонтактнаяИнформацияПриНажатии(Элемент, СтандартнаяОбработка)
	
	УправлениеКонтактнойИнформациейКлиент.НачатьВыбор(ЭтотОбъект, Элемент, , СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_КонтактнаяИнформацияОчистка(Элемент, СтандартнаяОбработка)
	
	УправлениеКонтактнойИнформациейКлиент.НачатьОчистку(ЭтотОбъект, Элемент.Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_КонтактнаяИнформацияВыполнитьКоманду(Команда)
	
	УправлениеКонтактнойИнформациейКлиент.НачатьВыполнениеКоманды(ЭтотОбъект, Команда.Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_КонтактнаяИнформацияОбработкаНавигационнойСсылки(Элемент, НавигационнаяСсылкаФорматированнойСтроки, СтандартнаяОбработка)
	УправлениеКонтактнойИнформациейКлиент.НачатьОбработкуНавигационнойСсылки(ЭтотОбъект, Элемент, НавигационнаяСсылкаФорматированнойСтроки, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПродолжитьОбновлениеКонтактнойИнформации(Результат, ДополнительныеПараметры) Экспорт
	ОбновитьКонтактнуюИнформацию(Результат);
КонецПроцедуры

&НаСервере
Процедура ОбновитьКонтактнуюИнформацию(Результат)
	УправлениеКонтактнойИнформацией.ОбновитьКонтактнуюИнформацию(ЭтотОбъект, Объект, Результат);
КонецПроцедуры

// Конец СтандартныеПодсистемы.КонтактнаяИнформация

&НаСервере 
Процедура ИнициализироватьКонтактнуюИнформацию()
	// Обработчик механизма "Контактная информация"
	
	ЗаполнитьЗначенияСвойств(Объект.КонтактнаяИнформация,ТекущаяОрганизация.КонтактнаяИнформация);
	
	СтруктураКонтактнаяИнформация = Новый Структура("
		|Ссылка, КонтактнаяИнформация", 
		ТекущаяОрганизация, Объект.КонтактнаяИнформация);
	
	// СтандартныеПодсистемы.КонтактнаяИнформация
	ДопПараметрыКИ = УправлениеКонтактнойИнформацией.ПараметрыКонтактнойИнформации();
	ДопПараметрыКИ.ИмяЭлементаДляРазмещения = "ГруппаВводДанных4";
	УправлениеКонтактнойИнформацией.ПриСозданииНаСервере(ЭтаФорма, СтруктураКонтактнаяИнформация, ДопПараметрыКИ);
	// Конец СтандартныеПодсистемы.КонтактнаяИнформация
	
	ВидимостьДоступностьСтильКонтактнойИнформации("ГруппаВводДанных4");
	
	ПереместитьЭлементыПроверкиЭлектроннойПочты();
КонецПроцедуры

&НаСервере 
Процедура ВидимостьДоступностьСтильКонтактнойИнформации(ИмяГруппыКонтактнойИнформации)
	ЭлементыКонтактнойИнформации = Элементы[ИмяГруппыКонтактнойИнформации].ПодчиненныеЭлементы;
	КонтактнаяИнформацияОписание = ЭтаФорма.КонтактнаяИнформацияОписаниеДополнительныхРеквизитов;
	
	ШрифтПоляВвода = Новый Шрифт(, 18);
	ШрифтЗаголовка = Новый Шрифт(, 10);
	
	Для Каждого Элемент Из ЭлементыКонтактнойИнформации Цикл 
		Если Не Элемент.Родитель = Элементы[ИмяГруппыКонтактнойИнформации] 
			ИЛИ ТипЗнч(Элемент) = Тип("ГруппаФормы") Тогда
			Продолжить;
		КонецЕсли;
		
		ИмяЭлемента = Элемент.Имя;
		ОписаниеЭлемента = ОписаниеЭлементаКонтактнойИнформации(ИмяЭлемента, КонтактнаяИнформацияОписание);
		
		Если ОписаниеЭлемента.ТипЭлемента = Перечисления.ТипыКонтактнойИнформации.Другое
			ИЛИ Найти(ОписаниеЭлемента.Представление, НСтр("ru= 'Другая информация'")) > 0 
			ИЛИ Найти(ОписаниеЭлемента.Представление, НСтр("ru= 'Почтовый адрес'")) > 0 Тогда
			Элемент.Видимость = Ложь;
		Иначе
			Элемент.ШрифтЗаголовка	= ШрифтЗаголовка;
			Элемент.Шрифт			= ШрифтПоляВвода;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

&НаСервере 
Функция ОписаниеЭлементаКонтактнойИнформации(ИмяЭлемента, КонтактнаяИнформацияОписание)
	Описание = Новый Структура("
		|ТипЭлемента, Представление", 
		Перечисления.ТипыКонтактнойИнформации.ПустаяСсылка(), "");
	
	Отбор = Новый Структура("ИмяРеквизита", ИмяЭлемента);
	ИскомыеЭлементы = КонтактнаяИнформацияОписание.НайтиСтроки(Отбор);
	Если ИскомыеЭлементы.Количество() > 0 Тогда
		ИскомыйЭлемент = ИскомыеЭлементы[0];
		
		Описание.ТипЭлемента = ИскомыйЭлемент.Тип;
		Описание.Представление = ИскомыйЭлемент.Вид.Наименование;
	КонецЕсли;
	
	Возврат Описание;
КонецФункции

&НаСервере 
Процедура ПереместитьЭлементыПроверкиЭлектроннойПочты()
	ОтборПоТипуКИ = Новый Структура("Тип", Перечисления.ТипыКонтактнойИнформации.АдресЭлектроннойПочты);
	ПоляЭлектроннойПочты = ЭтаФорма.КонтактнаяИнформацияОписаниеДополнительныхРеквизитов.НайтиСтроки(ОтборПоТипуКИ);
	
	Для Каждого ПолеЭлектроннойПочты Из ПоляЭлектроннойПочты Цикл 
		ПолеВводаЭлектроннойПочты = Элементы[ПолеЭлектроннойПочты.ИмяРеквизита];
		
		Элементы.Переместить(Элементы.СтраницыОшибкаЭлектронногоАдреса, ПолеВводаЭлектроннойПочты.Родитель);
	КонецЦикла;	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере 
Процедура ЗаписатьИзмененияВИБ()
	
	ЗаписатьПользователяСервер();
	ЗаписатьДанныеОрганизацииСервер();
	ЗаписатьНаборКонстант();
	ДанныеЗаписаны = Истина;
	
КонецПроцедуры

&НаСервере
Функция ОписаниеПользователяИБ()
	
	Результат = Пользователи.НовоеОписаниеПользователяИБ();
	Пользователи.СкопироватьСвойстваПользователяИБ(
		Результат,
		ЭтаФорма,
		,
		"УникальныйИдентификатор,
		|Роли",
		"ПользовательИнфБазы");
		
	Результат.Вставить("ПодтверждениеПароля", ПодтверждениеПароля);
	
	Если Истина Тогда
		Роли = Новый Массив;
		Роли.Добавить("ПолныеПрава");
		Роли.Добавить("АдминистраторСистемы");
		Результат.Вставить("Роли", Роли);
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

&НаСервере 
Процедура ЗаписатьПользователяСервер()
	
	ОписаниеПользователяИБ = ОписаниеПользователяИБ();
	ОписаниеПользователяИБ.Вставить("Действие", "Записать");
	
	Если Не ТекущийПользователь.Пустая() Тогда
		Пользователь = ТекущийПользователь.ПолучитьОбъект();
	Иначе
		Пользователь = Справочники.Пользователи.СоздатьЭлемент();
	КонецЕсли;
	Пользователь.Наименование = НаименованиеПользователя;
	Пользователь.Служебный = Ложь;
	Пользователь.ДополнительныеСвойства.Вставить(
			"ОписаниеПользователяИБ", ОписаниеПользователяИБ);
			
	Пользователь.ДополнительныеСвойства.Вставить("ЗначениеКопирования", Справочники.Пользователи.ПустаяСсылка());
	Пользователь.ДополнительныеСвойства.Вставить("ГруппаНовогоПользователя", Справочники.ГруппыПользователей.ПустаяСсылка());
	
	Пользователь.ДополнительныеСвойства.ОписаниеПользователяИБ.Вставить("ДоступКИнформационнойБазеРазрешен", Истина);
	Пользователь.ДополнительныеСвойства.ОписаниеПользователяИБ.Вставить("АутентификацияСтандартная", Истина);
	Пользователь.ДополнительныеСвойства.ОписаниеПользователяИБ.Вставить("ПоказыватьВСпискеВыбора", Истина);
	Пользователь.ДополнительныеСвойства.ОписаниеПользователяИБ.Вставить("Пароль", Пароль);
	Пользователь.ДополнительныеСвойства.Вставить("СозданиеАдминистратора",
				НСтр("ru = 'Первый пользователь информационной базы назначается администратором.'"));
	
	Пользователь.Записать();
	
	Константы.ПрефиксУзлаРаспределеннойИнформационнойБазы.Установить(ПрефиксУзлаРаспределеннойИнформационнойБазы);
	
	ТекущийПользователь = Пользователь.Ссылка;
КонецПроцедуры	

&НаСервере
Процедура ЗаписатьНаборКонстант()
	
	НаборКонстантОбъект = РеквизитФормыВЗначение("НаборКонстант");
	НаборКонстантОбъект.Записать();
	
	Если ИерархияДляНовыхОР <> НаборКонстант.торо_ИерархияДляВводаНовыхОР Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		|	торо_СтруктурыОР.Ссылка
		|ИЗ
		|	Справочник.торо_СтруктурыОР КАК торо_СтруктурыОР
		|ГДЕ
		|	торо_СтруктурыОР.РазрешенВводНовыхОР = ИСТИНА";
		
		РезультатЗапроса = Запрос.Выполнить();
		Выборка = РезультатЗапроса.Выбрать();
		
		Пока Выборка.Следующий() Цикл
			ОбъектИерархия = Выборка.Ссылка.ПолучитьОбъект();
			ОбъектИерархия.РазрешенВводНовыхОР = ЛОЖЬ;
			ОбъектИерархия.Записать();
		КонецЦикла;
		
		Если ЗначениеЗаполнено(НаборКонстант.торо_ИерархияДляВводаНовыхОР) Тогда
			Иерархия = НаборКонстант.торо_ИерархияДляВводаНовыхОР.ПолучитьОбъект();
			Иерархия.РазрешенВводНовыхОР = Истина;
			Иерархия.Записать();
		КонецЕсли;
		
		ИерархияДляНовыхОР = НаборКонстант.торо_ИерархияДляВводаНовыхОР;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере 
Процедура ЗаписатьДанныеОрганизацииСервер()
	
	Если Не ЗначениеЗаполнено(ТекущаяОрганизация) Тогда
		Организация = Справочники.Организации.СоздатьЭлемент();
	Иначе
		Организация = ТекущаяОрганизация.ПолучитьОбъект();
	КонецЕсли; 
	
	УправлениеКонтактнойИнформацией.ПередЗаписьюНаСервере(ЭтаФорма, Организация);
	
	СохраняемыеРеквизиты = РеквизитыОрганизации();
	ОписаниеОрганизации = Новый Структура;
	Для Каждого СохраняемыйРеквизит Из СохраняемыеРеквизиты Цикл
		ОписаниеОрганизации.Вставить(СохраняемыйРеквизит, ЭтаФорма[СохраняемыйРеквизит]);
	КонецЦикла;
	
	ЗаполнитьЗначенияСвойств(Организация, ОписаниеОрганизации);
	Организация.Записать();
	
КонецПроцедуры

&НаСервере 
Процедура УстановитьЦветЗаголовкаПоляОшибки()
	ЦветЗаголовкаПоляОшибки = ЦветаСтиля.ПоясняющийОшибкуТекст;
	ЦветЗаголовкаАвто = ЦветаСтиля.ЦветТекстаФормы;
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста 
Процедура УстановитьНазваниеКомандыРеквизитовОрганизации(ЭтаФорма)
	Если ПустаяСтрока(ЭтаФорма.СтрокаРеквизитовОрганизации) Тогда
		ЭтаФорма.Элементы.ИзменитьРеквизитыОрганизации.Заголовок = НСтр("ru = 'Указать'");
	Иначе
		ЭтаФорма.Элементы.ИзменитьРеквизитыОрганизации.Заголовок = НСтр("ru = 'Изменить'");
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПрочитатьДанныеОрганизации()

	ЗаполнитьЗначенияСвойств(ЭтотОбъект,ТекущаяОрганизация);
	
	Если ТекущаяОрганизация.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ЮридическоеЛицо Тогда
		ЮридическоеФизическоеЛицо = 1;
		ЮрФизЛицо = Перечисления.ЮридическоеФизическоеЛицо.ЮридическоеЛицо;
	Иначе
		ЮридическоеФизическоеЛицо = 0;
		ЮрФизЛицо = Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо;
	КонецЕсли;
	
	ПрефиксУзлаРаспределеннойИнформационнойБазы = Константы.ПрефиксУзлаРаспределеннойИнформационнойБазы.Получить();
	
КонецПроцедуры

ВыполняетсяЗакрытие = Ложь;

#КонецОбласти

#КонецОбласти
