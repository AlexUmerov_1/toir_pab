
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЗагрузитьПараметрыНапоминаний();
	
	Если НЕ ПравоДоступа("Редактирование", Метаданные.РегистрыСведений.торо_ПараметрыНапоминанийОПодпискахИТС) Тогда
		Элементы.ПараметрыНапоминаний.ТолькоПросмотр = Истина;
		Элементы.ПараметрыНапоминанийСохранитьПараметрыНапоминаний.Доступность = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ОбновитьДанные();	
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Оповещение = Новый ОписаниеОповещения("ЗаписатьИЗакрытьОповещение", ЭтотОбъект);
	ОбщегоНазначенияКлиент.ПоказатьПодтверждениеЗакрытияФормы(Оповещение, Отказ, ЗавершениеРаботы);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОбновитьИнформацию(Команда)
	
	ОбновитьДанные();
	
КонецПроцедуры

&НаКлиенте
Процедура СохранитьПараметрыНапоминаний(Команда)
	
	Если ПроверитьЗаполнениеПараметровНапоминаний() Тогда 
		ЗаписатьПараметрыНапоминаний();
		Модифицированность = Ложь;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ОбновитьДанные()
	
	СтруктураЛицензии = торо_СЛКСервер.ПолучитьЛицензию();
	Если СтруктураЛицензии = Неопределено Тогда
		
		Элементы.ГруппаДанныеПолучены.Видимость = Ложь;
		Элементы.ГруппаДанныеНеПолучены.Видимость = Истина;
		
	Иначе
		
		Элементы.ГруппаДанныеПолучены.Видимость = Истина;
		Элементы.ГруппаДанныеНеПолучены.Видимость = Ложь;
		
		НаименованиеПродукта = СтруктураЛицензии.ProductName;
		РегистрационныйНомер = СтруктураЛицензии.RegNo;
		
		ИТС_ДатаНачала = СтруктураЛицензии.SupportDate;
		ИТС_ДатаОкончания = СтруктураЛицензии.SupportEndDate;
		ИТС_ТипДоговора = ТипДоговораИТС(СтруктураЛицензии.SupportType);
		
		ИТС_СрокДействияТекст = СформироватьТекстСрокаДействия(ИТС_ДатаНачала, ИТС_ДатаОкончания);
		ИТС_ОсталосьДней = ЧислоДнейДоОкончания(ИТС_ДатаОкончания);
		
		ИТСОтраслевой_ДатаНачала = СтруктураЛицензии.IndustrySupportDate;
		ИТСОтраслевой_ДатаОкончания = СтруктураЛицензии.IndustrySupportEndDate;
		ИТСОтраслевой_ТипДоговора = ТипДоговораИТСОтраслевой(СтруктураЛицензии.IndustrySupportType);

		ИТСОтраслевой_СрокДействияТекст = СформироватьТекстСрокаДействия(ИТСОтраслевой_ДатаНачала, ИТСОтраслевой_ДатаОкончания);
		ИТСОтраслевой_ОсталосьДней = ЧислоДнейДоОкончания(ИТСОтраслевой_ДатаОкончания);
	КонецЕсли;

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция СформироватьТекстСрокаДействия(ДатаНачала, ДатаОкончания)
	
	Текст = Формат(ДатаНачала, "ДЛФ=DD") + " - " + Формат(ДатаОкончания, "ДЛФ=DD");
	Возврат Текст;
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ЧислоДнейДоОкончания(ДатаОкончания)
	
	Если ТипЗнч(ДатаОкончания) = Тип("Дата") И ЗначениеЗаполнено(ДатаОкончания)  Тогда
		
		НачалоТекущейДаты = НачалоДня(ТекущаяДата());
		ЧислоДней = (НачалоДня(ДатаОкончания) - НачалоТекущейДаты)/86400 + 1;
		Возврат ЧислоДней;
		
	Иначе
		
		Возврат 0;
		
	КонецЕсли;
		
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ТипДоговораИТС(КодТипа)
	
	ТипДогвора = "";
	
	Если КодТипа = 0 Тогда
		ТипДогвора = "Неизвестный";
	ИначеЕсли КодТипа = 1 Тогда
		ТипДогвора = "1С:ИТС Проф";
	ИначеЕсли КодТипа = 2 Тогда
		ТипДогвора = "1С:ИТС Техно";
	КонецЕсли;
	
	Возврат ТипДогвора;
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ТипДоговораИТСОтраслевой(КодТипа)
	
	ТипДогвора = "";
	
	Если КодТипа = 0 Тогда
		ТипДогвора = "Неизвестный";
	ИначеЕсли КодТипа = 1 Тогда
		ТипДогвора = "1С:ИТС Отраслевой Базовый";
	ИначеЕсли КодТипа = 2 Тогда
		ТипДогвора = "1С:ИТС Отраслевой 1-й Категории";
	ИначеЕсли КодТипа = 3 Тогда
		ТипДогвора = "1С:ИТС Отраслевой 2-й Категории";
	ИначеЕсли КодТипа = 4 Тогда
		ТипДогвора = "1С:ИТС Отраслевой 3-й Категории";
	ИначеЕсли КодТипа = 5 Тогда
		ТипДогвора = "1С:ИТС Отраслевой 4-й Категории";
	ИначеЕсли КодТипа = 6 Тогда
		ТипДогвора = "1С:ИТС Отраслевой 5-й Категории";
	КонецЕсли;
	
	Возврат ТипДогвора;
	
КонецФункции

&НаСервере
Процедура ЗагрузитьПараметрыНапоминаний()
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	торо_ПараметрыНапоминанийОПодпискахИТС.Событие КАК Событие,
	|	торо_ПараметрыНапоминанийОПодпискахИТС.Напоминать КАК Напоминать,
	|	торо_ПараметрыНапоминанийОПодпискахИТС.СрокНапоминания КАК СрокНапоминания,
	|	торо_ПараметрыНапоминанийОПодпискахИТС.ПараметрыОповещения КАК ПараметрыОповещения
	|ИЗ
	|	РегистрСведений.торо_ПараметрыНапоминанийОПодпискахИТС КАК торо_ПараметрыНапоминанийОПодпискахИТС
	|
	|УПОРЯДОЧИТЬ ПО
	|	торо_ПараметрыНапоминанийОПодпискахИТС.Событие.Порядок";
	
	Результат = Запрос.Выполнить().Выгрузить();
	ПараметрыНапоминаний.Загрузить(Результат);
		
КонецПроцедуры

&НаСервере
Процедура ЗаписатьПараметрыНапоминаний()
	
	НаборЗаписей = РегистрыСведений.торо_ПараметрыНапоминанийОПодпискахИТС.СоздатьНаборЗаписей();
	НаборЗаписей.Загрузить(ПараметрыНапоминаний.Выгрузить());
	НаборЗаписей.Записать();
	
КонецПроцедуры

&НаКлиенте
Функция ПроверитьЗаполнениеПараметровНапоминаний()
	
	ОшибкаЗаполнения = Ложь;
	НомерСтроки = 1;
	
	Для каждого Строка из ПараметрыНапоминаний Цикл
		Если Строка.Напоминать Тогда
			Если НЕ ЗначениеЗаполнено(Строка.СрокНапоминания) Тогда
				ТекстСообщения = НСтр("ru = 'Не заполнен срок напоминания!'");
				ПутьКПолю = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("ПараметрыНапоминаний", НомерСтроки, "СрокНапоминания");
				ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения,,ПутьКПолю,,ОшибкаЗаполнения);
			КонецЕсли;
			
			Если НЕ ЗначениеЗаполнено(Строка.ПараметрыОповещения) Тогда
				ТекстСообщения = НСтр("ru = 'Не заполнены параметры оповещения!'");
				ПутьКПолю = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("ПараметрыНапоминаний", НомерСтроки, "ПараметрыОповещения");
				ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения,,ПутьКПолю,,ОшибкаЗаполнения);
			КонецЕсли;
		КонецЕсли;
		НомерСтроки = НомерСтроки + 1;
	КонецЦикла;

	Возврат НЕ ОшибкаЗаполнения;
	
КонецФункции

&НаКлиенте
Процедура ЗаписатьИЗакрытьОповещение(Результат, Контекст) Экспорт
	
	Если ПроверитьЗаполнениеПараметровНапоминаний() Тогда 
		ЗаписатьПараметрыНапоминаний();
		Модифицированность = Ложь;
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
