
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЗаполнитьЗначенияСвойств(Объект, Параметры);
	
	Если Параметры.Свойство("ТаблицаКритичности") Тогда
		ТаблицаКритичности.Загрузить(ПолучитьИзВременногоХранилища(Параметры.ТаблицаКритичности));
		ИспользоватьВыделениеПо = "По критичности дефекта";
		Элементы.ПанельУстановкаПалитры.ТекущаяСтраница = Элементы.ПанельУстановкаПалитры.ПодчиненныеЭлементы.ПалитраПоКритичности;
	ИначеЕсли Параметры.Свойство("ТаблицаПриоритетностиОР") Тогда
		ТаблицаПриоритетностиОР.Загрузить(ПолучитьИзВременногоХранилища(Параметры.ТаблицаПриоритетностиОР));
		ИспользоватьВыделениеПо = "По критичности объекта ремонта";
		Элементы.ПанельУстановкаПалитры.ТекущаяСтраница = Элементы.ПанельУстановкаПалитры.ПодчиненныеЭлементы.ПалитраПоПриоритетуОР;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	торо_КритичностьДефекта.Ссылка КАК Критичность
	|ИЗ
	|	Справочник.торо_КритичностьДефекта КАК торо_КритичностьДефекта
	|
	|УПОРЯДОЧИТЬ ПО
	|	торо_КритичностьДефекта.Код";
	
	ЦветПоУмолчанию = Новый Цвет(255,255,255);
	
	Если ТаблицаКритичности.Количество() = 0 Тогда
		
		РезультатЗапроса = Запрос.Выполнить().Выгрузить();
		
		Массив = Новый Массив;
		Массив.Добавить(Тип("Цвет"));
		РезультатЗапроса.Колонки.Добавить("Цвет",Новый ОписаниеТипов(Массив));
		
		РезультатЗапроса.ЗаполнитьЗначения(ЦветПоУмолчанию,"Цвет");
		
		ТаблицаКритичности.Загрузить(РезультатЗапроса);
		
	Иначе 
		
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() цикл
			Если ТаблицаКритичности.НайтиСтроки(Новый Структура("Критичность", Выборка.Критичность)).Количество() = 0 Тогда
				Нс = ТаблицаКритичности.Добавить();
				Нс.Критичность = Выборка.Критичность;
				Нс.Цвет = ЦветПоУмолчанию;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	торо_ПриоритетыОбъектовРемонта.Ссылка КАК Приоритет
	|ИЗ
	|	Справочник.торо_ПриоритетыОбъектовРемонта КАК торо_ПриоритетыОбъектовРемонта
	|
	|УПОРЯДОЧИТЬ ПО
	|	торо_ПриоритетыОбъектовРемонта.Код";
	
	ЦветПоУмолчанию = Новый Цвет(255,255,255);
	
	Если ТаблицаПриоритетностиОР.Количество() = 0 Тогда
		РезультатЗапроса = Запрос.Выполнить().Выгрузить();
		
		Массив = Новый Массив;
		Массив.Добавить(Тип("Цвет"));
		РезультатЗапроса.Колонки.Добавить("Цвет",Новый ОписаниеТипов(Массив));
		
		РезультатЗапроса.ЗаполнитьЗначения(ЦветПоУмолчанию,"Цвет");
		
		ТаблицаПриоритетностиОР.Загрузить(РезультатЗапроса);
		
	Иначе 
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() цикл
			Если ТаблицаПриоритетностиОР.НайтиСтроки(Новый Структура("Приоритет", Выборка.Приоритет)).Количество() = 0 Тогда
				Нс = ТаблицаПриоритетностиОР.Добавить();
				Нс.Приоритет = Выборка.Приоритет;
				Нс.Цвет = ЦветПоУмолчанию;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;

	Если Объект.ПериодОбновления = 0 Тогда
	
		Объект.ПериодОбновления = 180;
	
	КонецЕсли;
	
	Элементы.ИспользоватьВыделениеПо.СписокВыбора.Вставить(0,"По критичности дефекта","По критичности дефекта");
	Элементы.ИспользоватьВыделениеПо.СписокВыбора.Вставить(1,"По критичности объекта ремонта","По критичности объекта ремонта");
	ИспользоватьВыделениеПо = ?(ЗначениеЗаполнено(ИспользоватьВыделениеПо), ИспользоватьВыделениеПо, "По критичности дефекта");
	
	Если Параметры.Свойство("ТекущейДаты") И Параметры.ТекущейДаты = Истина Тогда
		Объект.ДатаОтбора = ТекущаяДата();
	КонецЕсли;
	
	УстановитьУсловноеОформление();
		
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ИспользоватьВыделениеПоОбработкаВыбора(Элементы.ИспользоватьВыделениеПо, ИспользоватьВыделениеПо, Истина);
	фВидДаты = Объект.ВидДаты;
	Элементы.ДатаАктуальности.Доступность = (фВидДаты = 1);
	УстановитьДоступностьОтбораДат();
		
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ИспользоватьВыделениеПоОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	Если ВыбранноеЗначение = "По критичности дефекта" Тогда
		
		Элементы.ПанельУстановкаПалитры.ТекущаяСтраница = Элементы.ПанельУстановкаПалитры.ПодчиненныеЭлементы.ПалитраПоКритичности;
		
	ИначеЕсли ВыбранноеЗначение = "По критичности объекта ремонта" Тогда
		
		Элементы.ПанельУстановкаПалитры.ТекущаяСтраница = Элементы.ПанельУстановкаПалитры.ПодчиненныеЭлементы.ПалитраПоПриоритетуОР;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьВыделениеПоПриИзменении(Элемент)
	
	УстановитьУсловноеОформление();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыборВидаДатыПриИзменении(Элемент)
	
	флДатаАктуальности = (фВидДаты = 1);
	Если НЕ флДатаАктуальности Тогда
		Объект.ДатаАктуальности = ТекущаяДата();
	КонецЕсли;
	Элементы.ДатаАктуальности.Доступность = флДатаАктуальности;
	
КонецПроцедуры

&НаКлиенте
Процедура НеОтображатьРемонтыРаньшеДатыПриИзменении(Элемент)
	УстановитьДоступностьОтбораДат();
КонецПроцедуры

&НаКлиенте
Процедура ТекущейДатыПриИзменении(Элемент)
	УстановитьДоступностьОтбораДат();
	Объект.ДатаОтбора = ТекущаяДата();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыТаблицаКритичности
&НаКлиенте
Процедура ТаблицаКритичностиЦветПриИзменении(Элемент)
	
	УстановитьУсловноеОформление();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы
&НаКлиенте
Процедура Применить(Команда)
	
	СтруктураПараметров = Новый Структура("ВидДаты, ДатаАктуальности, Завершенные, НеПроведенные, ПериодОбновления, АвтоматическоеОбновление, ПлановыйПериод, Периодичность, РежимВыделенияСтрок, ПодчиненныеПодразделения, НеОтображатьРемонтыРаньшеДаты, ТекущейДаты, ДатаОтбора",
	фВидДаты, Объект.ДатаАктуальности, Объект.Завершенные, Объект.НеПроведенные, Объект.ПериодОбновления, Объект.АвтоматическоеОбновление, Объект.ПлановыйПериод, Объект.Периодичность, ИспользоватьВыделениеПо, Объект.ПодчиненныеПодразделения,  Объект.НеОтображатьРемонтыРаньшеДаты,  Объект.ТекущейДаты,  Объект.ДатаОтбора);
	
	Если ИспользоватьВыделениеПо = "По критичности дефекта" Тогда
		ВладелецФормы.ТаблицаКритичности.Очистить();
		Для каждого Стр Из ТаблицаКритичности Цикл
			НС = ВладелецФормы.ТаблицаКритичности.Добавить();
			ЗаполнитьЗначенияСвойств(НС, Стр);
		КонецЦикла;
	ИначеЕсли ИспользоватьВыделениеПо = "По критичности объекта ремонта" Тогда
		ВладелецФормы.ТаблицаПриоритетностиОР.Очистить();
		Для каждого Стр Из ТаблицаПриоритетностиОР Цикл
			НС = ВладелецФормы.ТаблицаПриоритетностиОР.Добавить();
			ЗаполнитьЗначенияСвойств(НС, Стр);
		КонецЦикла;
	КонецЕсли;
	
	Оповестить("ПерезаполнитьТаблицы", СтруктураПараметров);
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗакрытьФорму(Команда)
	Закрыть();
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	Если ИспользоватьВыделениеПо = "По критичности дефекта" Тогда
		Для каждого Стр Из ТаблицаКритичности Цикл
			
			ЭлементУсловногоОформления = УсловноеОформление.Элементы.Добавить();
			ОформляемоеПоле = ЭлементУсловногоОформления.Поля.Элементы.Добавить();
			ОформляемоеПоле.Поле = Новый ПолеКомпоновкиДанных("ТаблицаКритичностиЦвет");
			ЭлементОтбора = ЭлементУсловногоОформления.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
			ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("ТаблицаКритичности.Критичность");
			ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
			ЭлементОтбора.ПравоеЗначение = Стр.Критичность;
			ЭлементУсловногоОформления.Оформление.УстановитьЗначениеПараметра("ЦветФона", Стр.Цвет);
			
		КонецЦикла;
	ИначеЕсли ИспользоватьВыделениеПо = "По критичности объекта ремонта" Тогда
		
		Для каждого Стр Из ТаблицаПриоритетностиОР Цикл
			
			ЭлементУсловногоОформления = УсловноеОформление.Элементы.Добавить();
			ОформляемоеПоле = ЭлементУсловногоОформления.Поля.Элементы.Добавить();
			ОформляемоеПоле.Поле = Новый ПолеКомпоновкиДанных("ТаблицаПриоритетностиОРЦвет");
			ЭлементОтбора = ЭлементУсловногоОформления.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
			ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("ТаблицаПриоритетностиОР.Приоритет");
			ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
			ЭлементОтбора.ПравоеЗначение = Стр.Приоритет;
			ЭлементУсловногоОформления.Оформление.УстановитьЗначениеПараметра("ЦветФона", Стр.Цвет);
			
		КонецЦикла;
		
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура УстановитьДоступностьОтбораДат()
	Если Объект.НеОтображатьРемонтыРаньшеДаты Тогда
		Если Объект.ТекущейДаты Тогда
			Элементы.ДатаОтбора.Доступность = Ложь;
			Элементы.ТекущейДаты.Доступность = Истина;
		Иначе
			Элементы.ДатаОтбора.Доступность = Истина;
			Элементы.ТекущейДаты.Доступность = Истина;
		КонецЕсли;
	Иначе
		Элементы.ДатаОтбора.Доступность = Ложь;
		Элементы.ТекущейДаты.Доступность = Ложь;
		Объект.ТекущейДаты = Ложь;
	КонецЕсли;
КонецПроцедуры

#КонецОбласти
