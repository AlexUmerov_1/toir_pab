////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПЕРЕМЕННЫЕ

&НаКлиенте
Перем ВидРемонтаДоРедактирования;
&НаКлиенте
Перем КопированиеВидаРемонта;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЗагрузитьТаблицуВидовПланов();
	ЗагрузитьТаблицуВидовРемонтов();
	 
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	Если Модифицированность Тогда
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ОбработкаПередЗакрытием", ЭтаФорма);
		
		СписокКнопок = Новый СписокЗначений;
        СписокКнопок.Добавить(КодВозвратаДиалога.Да, "Да");
        СписокКнопок.Добавить(КодВозвратаДиалога.Отмена, "Нет");
		
		ПоказатьВопрос(ОписаниеОповещения, НСтр("ru = 'В форме имеются несохраненные изменения. Сохранить изменения перед закрытием?'"), СписокКнопок); 
		Отказ = Истина;
		Возврат;
		
	КонецЕсли; 
	
	Объект.ВидыРемонтов.Очистить();
	
КонецПроцедуры


&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	ПроверитьЗаполнениеТЧВидыРемонтов(Отказ);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы
&НаКлиенте
Процедура ВидыСкользящихПлановПриИзменении(Элемент)
	ТекущиеДанные = Элементы.ВидыСкользящихПланов.ТекущиеДанные;
	Если Не ТекущиеДанные = Неопределено Тогда
		ТекущиеДанные.Изменен = Истина;
	КонецЕсли; 
КонецПроцедуры

&НаКлиенте
Процедура ВидыРемонтовПриИзменении(Элемент)

	ТекущиеДанные = Элементы.ВидыСкользящихПланов.ТекущиеДанные;
	Если Не ТекущиеДанные = Неопределено Тогда
		ТекущиеДанные.Изменен = Истина;
	КонецЕсли;
	Модифицированность = Истина;

КонецПроцедуры

&НаКлиенте
Процедура ВидыСкользящихПлановПриАктивизацииСтроки(Элемент)
	
	ТекущиеДанные = Элементы.ВидыСкользящихПланов.ТекущиеДанные;
	Если Не ТекущиеДанные = Неопределено Тогда
		Элементы.ВидыРемонтов.ОтборСтрок = Новый ФиксированнаяСтруктура(Новый Структура("ВидСкользящегоПлана",ТекущиеДанные.ВидСкользящегоПлана));
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ВидыСкользящихПлановТочностьПланированияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ТекущиеДанные = Элементы.ВидыСкользящихПланов.ТекущиеДанные;
	Элемент.СписокВыбора.Очистить();
	Если Не ТекущиеДанные = Неопределено Тогда

		Если ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Стратегический") 
			ИЛИ ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Долгосрочный") Тогда
			
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияПлана.Год"));
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияПлана.Месяц"));
			
		ИначеЕсли ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Годовой") Тогда
			
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияПлана.Месяц"));
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияПлана.Неделя"));
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияПлана.День"));
			
		ИначеЕсли ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Среднесрочный") Тогда
			
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияПлана.Неделя"));
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияПлана.День"));
		Иначе
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияПлана.День"));
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияПлана.Час"));
		КонецЕсли; 
	КонецЕсли; 

КонецПроцедуры

&НаКлиенте
Процедура ВидыСкользящихПлановВидИсполнителяРаботНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ТекущиеДанные = Элементы.ВидыСкользящихПланов.ТекущиеДанные;
	Элемент.СписокВыбора.Очистить();
	Если Не ТекущиеДанные = Неопределено Тогда

		Если ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Стратегический") 
			ИЛИ ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Долгосрочный") 
			ИЛИ ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Годовой") Тогда
			
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияИсполнителя.Подразделение"));
						
		ИначеЕсли ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Среднесрочный") Тогда
			
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияИсполнителя.Подразделение"));
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияИсполнителя.Бригада"));
		Иначе
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.торо_ДетализацияИсполнителя.Бригада"));
		КонецЕсли; 
	КонецЕсли; 

КонецПроцедуры

&НаКлиенте
Процедура ВидыСкользящихПлановПериодичностьПланированияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ТекущиеДанные = Элементы.ВидыСкользящихПланов.ТекущиеДанные;
	Элемент.СписокВыбора.Очистить();
	Если Не ТекущиеДанные = Неопределено Тогда

		Если ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Стратегический") 
			ИЛИ ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Долгосрочный") 
			ИЛИ ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Годовой") Тогда
			
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.Периодичность.Год"));
						
		ИначеЕсли ТекущиеДанные.ВидСкользящегоПлана = ПредопределенноеЗначение("Справочник.торо_ВидыСкользящихПланов.Среднесрочный") Тогда
			
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.Периодичность.Месяц"));
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.Периодичность.Квартал"));
		Иначе
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.Периодичность.Неделя"));
			Элемент.СписокВыбора.Добавить(ПредопределенноеЗначение("Перечисление.Периодичность.День"));
		КонецЕсли; 
	КонецЕсли; 

КонецПроцедуры

&НаКлиенте
Процедура ВидыРемонтовПередОкончаниемРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования, Отказ)
	
	ТекущаяСтрокаВР = Элементы.ВидыРемонтов.ТекущиеДанные;
	ТекущиеДанные   = Элементы.ВидыСкользящихПланов.ТекущиеДанные;
	
	Если НЕ ОтменаРедактирования Тогда	
		
		Если ТекущиеДанные = Неопределено Или ТекущаяСтрокаВР = Неопределено Тогда
		    Отказ = Истина;
			Возврат;
		КонецЕсли;

		Если НоваяСтрока Тогда
			Если Не ТекущиеДанные = Неопределено
				И НЕ ТекущаяСтрокаВР = Неопределено Тогда
				ТекущаяСтрокаВР.ВидСкользящегоПлана = ТекущиеДанные.ВидСкользящегоПлана;
			КонецЕсли;
		КонецЕсли;
	ИначеЕсли НЕ НоваяСтрока Тогда		
		ТекущаяСтрокаВР.ВидРемонта = ВидРемонтаДоРедактирования;		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВидыРемонтовПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	КопированиеВидаРемонта = Копирование;

	// Запись данных до редактирования
	Если НЕ НоваяСтрока Тогда
		ВидРемонтаДоРедактирования = Элементы.ВидыРемонтов.ТекущиеДанные.ВидРемонта; 	
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Сохранить(Команда)
	СохранитьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьПоУмолчанию(Команда)
	ЗаполнитьПоУмолчаниюНаСервере();
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЗагрузитьТаблицуВидовПланов()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	торо_ВидыСкользящихПланов.Ссылка КАК ВидСкользящегоПлана,
	|	торо_ВидыСкользящихПланов.Использование КАК Использование,
	|	торо_ВидыСкользящихПланов.ПериодичностьПланирования КАК ПериодичностьПланирования,
	|	торо_ВидыСкользящихПланов.ЧислоОтображаемыхПериодов КАК ЧислоОтображаемыхПериодов,
	|	торо_ВидыСкользящихПланов.ТочностьПланирования КАК ТочностьПланирования,
	|	торо_ВидыСкользящихПланов.ВидИсполнителяРабот КАК ВидИсполнителяРабот
	|ИЗ
	|	Справочник.торо_ВидыСкользящихПланов КАК торо_ВидыСкользящихПланов
	|
	|УПОРЯДОЧИТЬ ПО
	|	ПериодичностьПланирования УБЫВ,
	|	ЧислоОтображаемыхПериодов";
	
	РезультатЗапроса = Запрос.Выполнить();
	ВидыСкользящихПланов.Загрузить(РезультатЗапроса.Выгрузить());
	
КонецПроцедуры

&НаСервере
Процедура ЗагрузитьТаблицуВидовРемонтов()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	торо_ВидыСкользящихПлановВидыРемонтов.Ссылка КАК ВидСкользящегоПлана,
	|	торо_ВидыСкользящихПлановВидыРемонтов.ВидРемонта КАК ВидРемонта
	|ИЗ
	|	Справочник.торо_ВидыСкользящихПланов.ВидыРемонтов КАК торо_ВидыСкользящихПлановВидыРемонтов";
	
	РезультатЗапроса = Запрос.Выполнить();
	Объект.ВидыРемонтов.Загрузить(РезультатЗапроса.Выгрузить());
	
КонецПроцедуры

&НаСервере
Процедура СохранитьНаСервере()
	
	ЕстьОшибки = Ложь;
	ПроверитьЗаполнениеТЧВидыРемонтов(ЕстьОшибки);
	
	Если Не ЕстьОшибки Тогда
	
		Для Каждого Строка Из ВидыСкользящихПланов Цикл
			Если Строка.Изменен Тогда
				ВидПлана = Строка.ВидСкользящегоПлана.ПолучитьОбъект();
				ЗаполнитьЗначенияСвойств(ВидПлана, Строка);
				ВидПлана.ВидыРемонтов.Очистить();
				
				СписокДобавленных = Новый СписокЗначений;
				
				МассивВидовРемонтов = Объект.ВидыРемонтов.НайтиСтроки(Новый Структура("ВидСкользящегоПлана", Строка.ВидСкользящегоПлана));
				
				Для каждого СтрокаСВидомРемонта Из МассивВидовРемонтов Цикл
					Если СписокДобавленных.НайтиПоЗначению(СтрокаСВидомРемонта.ВидРемонта) = Неопределено Тогда
						СписокДобавленных.Добавить(СтрокаСВидомРемонта.ВидРемонта);
						НС = ВидПлана.ВидыРемонтов.Добавить();
						НС.ВидРемонта = СтрокаСВидомРемонта.ВидРемонта;
					КонецЕсли; 
				КонецЦикла; 
				
				Попытка
					ВидПлана.Записать();
					Строка.Изменен = Ложь;
				Исключение
				   ТекстСообщения = НСтр("ru = 'Не удалось сохранить изменения вида скользящего плана %1. Причина: %2'");
				   Сообщение = Новый СообщениеПользователю;
				   ТекстСообщения = СтрШаблон(ТекстСообщения, Строка.ВидСкользящегоПлана, ОписаниеОшибки()); 
				   Сообщение.Текст = ТекстСообщения;
				   Сообщение.Сообщить(); 
				   ЕстьОшибки = Истина;
				КонецПопытки; 
				
			КонецЕсли; 
		КонецЦикла; 
	КонецЕсли;
	
	Если Не ЕстьОшибки Тогда
		Модифицированность = Ложь;
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте 
Процедура ОбработкаПередЗакрытием(Результат, ДопПараметры) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		СохранитьНаСервере();
	Иначе
		Модифицированность = Ложь;
	КонецЕсли; 
	
	Закрыть();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьПоУмолчаниюНаСервере()

	ЛокализуемыеРеквизитыОбъекта = МультиязычностьСервер.МультиязычныеРеквизитыОбъекта(Метаданные.Справочники.торо_ВидыСкользящихПланов);
	ПредопределенныеДанные = ОбновлениеИнформационнойБазыСлужебный.ПредопределенныеДанныеОбъекта(Метаданные.Справочники.торо_ВидыСкользящихПланов, Справочники.торо_ВидыСкользящихПланов, ЛокализуемыеРеквизитыОбъекта);
	
	Для каждого Строка из ПредопределенныеДанные Цикл
		ЭлементОбъект = Справочники.торо_ВидыСкользящихПланов[Строка.ИмяПредопределенныхДанных].ПолучитьОбъект();
		ЗаполнитьЗначенияСвойств(ЭлементОбъект, Строка,,"ИмяПредопределенныхДанных, Код");
		ЭлементОбъект.Записать();
	КонецЦикла;
	
	ЗагрузитьТаблицуВидовПланов();
	ЗагрузитьТаблицуВидовРемонтов();
	
КонецПроцедуры

&НаСервере
Процедура ПроверитьЗаполнениеТЧВидыРемонтов(Отказ)
	Для Каждого ТекущаяСтрокаВР Из Объект.ВидыРемонтов Цикл
   		Если Не ЗначениеЗаполнено(ТекущаяСтрокаВР.ВидРемонта) Тогда
		    ТекстСообщения = НСтр("ru = 'Обнаружено незаполненное поле ""Вид ремонта"" в таблице ""Виды ремонтов""!'");
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения,,"Объект.ВидыРемонтов",, Отказ);
			Возврат;
		КонецЕсли; 
		
		СтруктураПоиска = Новый Структура("ВидСкользящегоПлана, ВидРемонта", ТекущаяСтрокаВР.ВидСкользящегоПлана, ТекущаяСтрокаВР.ВидРемонта);
		МассивВР = Объект.ВидыРемонтов.НайтиСтроки(СтруктураПоиска);
		Если МассивВР.Количество() > 1 Тогда
			ШаблонСообщения = НСтр("ru = 'Обнаружены повторяющиеся поля ""Вид ремонта"" ""(%1)"" для текущего плана ""(%2)""!'");
			ТекстСообщения = СтрШаблон(ШаблонСообщения, ТекущаяСтрокаВР.ВидРемонта, ТекущаяСтрокаВР.ВидСкользящегоПлана);
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения,,"Объект.ВидыРемонтов",, Отказ);
			Возврат;									
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

#КонецОбласти
