#Область ОбработчикиСобытийФормы
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("РемонтыОборудования") Тогда
		
		Для каждого Элем Из Параметры.РемонтыОборудования Цикл
			
			НС = РемонтыОборудования.Добавить();
			ЗаполнитьЗначенияСвойств(НС, Элем);
			
		КонецЦикла;
		
	ИначеЕсли Параметры.Свойство("ДефектыОборудования") Тогда 
				
		Для каждого Элем Из Параметры.ДефектыОборудования Цикл
			
			НС = РемонтыОборудования.Добавить();
			ЗаполнитьЗначенияСвойств(НС, Элем);
			
		КонецЦикла;
		
	КонецЕсли;
	
	Элементы.РемонтыОборудованияОтказавшийЭлемент.Видимость = Параметры.ПоказыватьОтказавшийЭлемент;
	
КонецПроцедуры
#КонецОбласти

#Область ОбработчикиКомандФормы
&НаКлиенте
Процедура УстановитьФлажки(Команда)
	
	Для Каждого СтрокаСписка Из РемонтыОборудования Цикл
		СтрокаСписка.Печатать = Истина;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура СнятьФлажки(Команда)
	
	Для Каждого СтрокаСписка Из РемонтыОборудования Цикл
		СтрокаСписка.Печатать = Ложь;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура Выбрать(Команда)
	
	МассивВозврата = Новый Массив;
	
	Для каждого Стр Из РемонтыОборудования Цикл
		Если Стр.Печатать Тогда
			МассивВозврата.Добавить(Стр.ID);
		КонецЕсли;
	КонецЦикла;
	
	Если НЕ МассивВозврата.Количество() Тогда
		
		ТекстСообщения = НСтр("ru = 'Необходимо выбрать хотя бы 1 ремонт!'");
		ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения);
		
	Иначе
		
		Закрыть(МассивВозврата);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗакрытьФорму(Команда)
	Закрыть();
КонецПроцедуры

#КонецОбласти