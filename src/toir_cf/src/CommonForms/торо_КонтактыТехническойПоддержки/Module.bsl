#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// БиблитекаИнтеретПоддержки.ИнтеграцияКоннект
	Элементы.ГруппаКоннект.Видимость = ИнтеграцияСКоннект.НастройкиИнтеграции().ОтображатьКнопкуЗапуска;
	// Конец БиблитекаИнтеретПоддержки.ИнтеграцияКоннект

	ЗаполнитьТекстОбращения();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОткрытьПочту(Команда)
	
	ФайловаяСистемаКлиент.ОткрытьНавигационнуюСсылку("mailto:helptoir@desnol.ru");
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСкайп(Команда)
	
	ФайловаяСистемаКлиент.ОткрытьНавигационнуюСсылку("skype:remont_expert?chat");
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьФорум(Команда)
	
	ФайловаяСистемаКлиент.ОткрытьНавигационнуюСсылку("https://1ctoir.ru/forum/");
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЗаполнитьТекстОбращения()
	
	ШаблонТекстаОбращения = 
		НСтр("ru='Регистрационный номер:  <Укажите рег. номер>
		|Название организации: <Укажите название организации>
		|Версия конфигурации 1С:ТОИР КОРП: %1
		|Версия платформы 1С:Предприятие: %2
		|Вид клиентского приложения: %3'");
	
	СистемнаяИнформация = Новый СистемнаяИнформация();
	ВерсияПлатформы = СистемнаяИнформация.ВерсияПриложения;
	ВерсияПрограммы = Метаданные.Версия;
	ПараметрыКлиента = СтандартныеПодсистемыСервер.ПараметрыКлиентаНаСервере();
	РежимРаботы = ПараметрыКлиента["ИспользуемыйКлиент"];
	
	ТекстОбращения = СтрШаблон(ШаблонТекстаОбращения, ВерсияПрограммы, ВерсияПлатформы, РежимРаботы);
	
КонецПроцедуры

#КонецОбласти
