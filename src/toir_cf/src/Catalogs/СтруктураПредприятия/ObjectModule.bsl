
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ)
	
	Если Не ЭтоНовый() И ПолучитьФункциональнуюОпцию("торо_РасширенныйРежимМТО") Тогда
		
		Параметры = Новый Структура;
		Параметры.Вставить("СкладДоИзменения", Ссылка.Склад);
		
		ДополнительныеСвойства.Вставить("ПараметрыАктуализацииЗаказовНаВнутреннееПотребление", Параметры);		
	КонецЕсли; 
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если Не Отказ И ДополнительныеСвойства.Свойство("ПараметрыАктуализацииЗаказовНаВнутреннееПотребление") Тогда
		
		СкладДоИзменения = ДополнительныеСвойства.ПараметрыАктуализацииЗаказовНаВнутреннееПотребление.СкладДоИзменения;
		СталЗаполненСклад = Не ЗначениеЗаполнено(СкладДоИзменения) И ЗначениеЗаполнено(Склад);
		
		Если СталЗаполненСклад Тогда
			торо_МТОСервер.ОтметитьНеобходимостьАктуализацииЗаказовНаВнутреннееПотреблениеПриИзмененииСклада(Ссылка);
		КонецЕсли;
	КонецЕсли;	
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли