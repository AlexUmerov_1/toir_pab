#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Отбор.Свойство("СписокПричин") Тогда
		ЭтотОбъект.СписокПричин = Параметры.Отбор.СписокПричин;
		Элементы.ПоказатьВсеПричины.Видимость = Истина;
		ПоказатьВсеПричины = Ложь;
	КонецЕсли;
	
	МультиязычностьСервер.ПриСозданииНаСервере(ЭтотОбъект);

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	Если Элементы.ПоказатьВсеПричины.Видимость Тогда
		УстановитьОтборПоФлажкуПоказатьВсеПричины();
	КонецЕсли;

КонецПроцедуры
#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы
&НаКлиенте
Процедура ПоказатьВсеПричиныПриИзменении(Элемент)
	
	УстановитьОтборПоФлажкуПоказатьВсеПричины();
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьОтборПоФлажкуПоказатьВсеПричины()
	
	Если ПоказатьВсеПричины Тогда
		ОбщегоНазначенияКлиентСервер.УдалитьЭлементыГруппыОтбораДинамическогоСписка(Список, "Ссылка");
	Иначе
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Ссылка",  ЭтотОбъект.СписокПричин, ВидСравненияКомпоновкиДанных.ВСписке,, Истина);
	КонецЕсли;
	
КонецПроцедуры 
#КонецОбласти


