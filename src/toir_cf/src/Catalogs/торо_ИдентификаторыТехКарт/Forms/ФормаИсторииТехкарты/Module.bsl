#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	СоответствиеДляМультиязычности = Новый Соответствие;
	СоответствиеДляМультиязычности.Вставить("Список", "Справочник.торо_ТехКарты");
	торо_МультиязычностьСервер.ПриСозданииНаСервереОбработкаДинамическихСписков(ЭтаФорма, СоответствиеДляМультиязычности);
	
	Если Параметры.Свойство("Идентификатор")
		И ЗначениеЗаполнено(Параметры.Идентификатор)Тогда
				
		Идентификатор = Параметры.Идентификатор;
		
		Список.Параметры.УстановитьЗначениеПараметра("ИдентификаторТехКарты",Идентификатор);
		
	КонецЕсли;
	
	ФОИСпользоватьОпасныеОперации             = ПолучитьФункциональнуюОпцию("торо_ИспользоватьОпасноеПроизводство");
	ФОИспользоватьПовышеннойОпасностиОперации = ПолучитьФункциональнуюОпцию("торо_ИспользоватьНарядыНаВыполнениеРабот");
	
	Элементы.СодержитОпасныеОперации.Видимость           = ФОИСпользоватьОпасныеОперации;
	Элементы.СодержитРаботыПовышеннойОпасности.Видимость = ФОИспользоватьПовышеннойОпасностиОперации;
		
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ТекущиеДанные = Элементы.Список.ТекущиеДанные;
	
	Если Не ТекущиеДанные = Неопределено Тогда
		Закрыть(ТекущиеДанные.ТехКарта);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Выбрать(Команда)
	
	ТекущиеДанные = Элементы.Список.ТекущиеДанные;
	
	Если Не ТекущиеДанные = Неопределено Тогда
		Закрыть(ТекущиеДанные.ТехКарта);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Добавить(Команда)
	Закрыть(ПредопределенноеЗначение("Справочник.торо_ТехКарты.ПустаяСсылка"));
КонецПроцедуры

#КонецОбласти

