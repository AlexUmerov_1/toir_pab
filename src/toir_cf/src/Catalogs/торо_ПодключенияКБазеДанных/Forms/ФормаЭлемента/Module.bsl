
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Элементы.ТекстЗапросаПометкиОбрабатываемых.Доступность = Объект.ИспользоватьПометкуДанных;
	Элементы.ТекстЗапросаУдаленяОбработанных.Доступность = Объект.ИспользоватьУдалениеДанных;
	Элементы.РазмерПорцииТекст.Доступность = Объект.ИспользованиеРазбиениеДанныхНаПорции;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ПериодПолученияДанныхТекст = торо_ОбщегоНазначения.ПродолжительностьВЧасах(Объект.ПериодПолученияДанных);
	РазмерПорцииТекст = торо_ОбщегоНазначения.ПродолжительностьВЧасах(Объект.РазмерПорции);
	
	УстановитьВидимостьГруппПараметровСоединения();
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	Если Объект.ИспользоватьПроизвольнуюСтрокуПодключения Тогда
		Если СтрНайти(Объект.ПроизвольнаяСтрокаПодключения, "%Логин%") = 0 Тогда
		    ТекстСообщения = НСтр("ru = 'В строке подключения не указано ключевое слово для логина!'");
			ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения,,,, Отказ);
		КонецЕсли;
		
		Если СтрНайти(Объект.ПроизвольнаяСтрокаПодключения, "%Пароль%") = 0 Тогда
		    ТекстСообщения = НСтр("ru = 'В строке подключения не указано ключевое слово для пароля!'");
			ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения,,,, Отказ);
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ИспользоватьПометкуДанныхПриИзменении(Элемент)
	Элементы.ТекстЗапросаПометкиОбрабатываемых.Доступность = Объект.ИспользоватьПометкуДанных;
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьУдалениеДанныхПриИзменении(Элемент)
	Элементы.ТекстЗапросаУдаленяОбработанных.Доступность = Объект.ИспользоватьУдалениеДанных;
КонецПроцедуры

&НаКлиенте
Процедура ИспользованиеРазбиениеДанныхНаПорцииПриИзменении(Элемент)
	Элементы.РазмерПорцииТекст.Доступность = Объект.ИспользованиеРазбиениеДанныхНаПорции;
КонецПроцедуры

&НаКлиенте
Процедура РазмерПорцииТекстНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	торо_ЗаполнениеДокументовКлиент.ОткрытьФормуПодбораПродолжительности(Объект.РазмерПорции, Элемент, Объект.Ссылка);
КонецПроцедуры

&НаКлиенте
Процедура РазмерПорцииТекстОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	РазмерПорцииТекст = торо_ОбщегоНазначения.ПродолжительностьВЧасах(ВыбранноеЗначение);
	Объект.РазмерПорции = ВыбранноеЗначение;
КонецПроцедуры

&НаКлиенте
Процедура ПериодПолученияДанныхТекстНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	торо_ЗаполнениеДокументовКлиент.ОткрытьФормуПодбораПродолжительности(Объект.ПериодПолученияДанных, Элемент, Объект.Ссылка);
КонецПроцедуры

&НаКлиенте
Процедура ПериодПолученияДанныхТекстОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	ПериодПолученияДанныхТекст = торо_ОбщегоНазначения.ПродолжительностьВЧасах(ВыбранноеЗначение);
	Объект.ПериодПолученияДанных = ВыбранноеЗначение;
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьПроизвольнуюСтрокуСоединенияПриИзменении(Элемент)
	УстановитьВидимостьГруппПараметровСоединения();
	
	Если Объект.ИспользоватьПроизвольнуюСтрокуПодключения Тогда
		Объект.Сервер = "";
		Объект.БазаДанных = "";
	Иначе			
		Объект.ПроизвольнаяСтрокаПодключения = "";
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура УстановитьВидимостьГруппПараметровСоединения()
	Элементы.ГруппаСерверИБазаДанных.Видимость = Не Объект.ИспользоватьПроизвольнуюСтрокуПодключения;
	Элементы.ГруппаПроизвольнаяСтрокаПодключения.Видимость = Объект.ИспользоватьПроизвольнуюСтрокуПодключения;
КонецПроцедуры

#КонецОбласти 