////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПЕРЕМЕННЫЕ
&НаКлиенте
Перем ДатаНачалаСинхронизации;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Если Параметры.Свойство("ДанныеЗадания") И ЗначениеЗаполнено(Параметры.ДанныеЗадания) Тогда
	    ДанныеЗадания = Параметры.ДанныеЗадания;
	Иначе
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
	Если Параметры.Свойство("АдресПараметровЗапуска") Тогда
	    АдресПараметровЗапуска = Параметры.АдресПараметровЗапуска;
	КонецЕсли;
	
	ЭтаФорма.Заголовок = ДанныеЗадания.НаименованиеЗадания;
	Элементы.СтатусОбменаДанными.Заголовок = ДанныеЗадания.НаименованиеЗадания;
	
	ЗакрыватьПриЗавершении = ДанныеЗадания.ЗакрыватьФормуВыполненияПриЗавершении;
	ЗапускатьАвтоматически = ДанныеЗадания.ЗапускатьВыполнениеАвтоматически;
	ЗакрыватьФормуВладельцаПриВыполнении = ДанныеЗадания.ЗакрыватьФормуВладельцаПриВыполнении;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	Если ЗапускатьАвтоматически Тогда
		ОбработатьПереходСтраниц();
	    ЗапуститьВыполнение();
	Иначе
	    Элементы.СтраницыОбменаДанными.ТекущаяСтраница = Элементы.ЗагрузкаДанных;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	Если ЗавершениеРаботы Или Не Элементы.СтраницыОбменаДанными.ТекущаяСтраница = Элементы.ОбменДанным Тогда
		Если ЗакрыватьФормуВладельцаПриВыполнении И Не ВладелецФормы = Неопределено Тогда
		    ВладелецФормы.Закрыть();
		КонецЕсли;
		
		Возврат;
	КонецЕсли;
		
	ТекстВопроса = НСтр("ru = 'Выполняется обмен данными с АИС Диспетчер. При закрытии формы обмен будет прерван. Продолжить?'");
	ОписаниеОповещенияПередЗакрытием = Новый ОписаниеОповещения("ПередЗакрытиемПослеВопроса", ЭтотОбъект);
	ПоказатьВопрос(ОписаниеОповещенияПередЗакрытием, ТекстВопроса, РежимДиалогаВопрос.ДаНет);
	Отказ = Истина;
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СледующийШаг(Команда)
	ОбработатьПереходСтраниц();
	ЗапуститьВыполнение();
КонецПроцедуры

&НаКлиенте
Процедура ЗавершитьОбмен(Команда)
	Если Не ВладелецФормы = Неопределено Тогда
	    ВладелецФормы.Закрыть();
	Иначе
	    Закрыть();
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ОбработатьПереходСтраниц(Ошибка = Ложь)
	СтраницыФормы = Элементы.СтраницыОбменаДанными;
	Если Ошибка Тогда
		СтраницыФормы.ТекущаяСтраница = Элементы.ОбменЗавершенСОшибкой;
    ИначеЕсли СтраницыФормы.ТекущаяСтраница = Элементы.ЗагрузкаДанных Тогда
	    СтраницыФормы.ТекущаяСтраница = Элементы.ОбменДанным;
	ИначеЕсли СтраницыФормы.ТекущаяСтраница = Элементы.ОбменДанным Тогда
		СтраницыФормы.ТекущаяСтраница = Элементы.ЗаписьЗавершенаУспешно;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемПослеВопроса(Результат, ДополнительныеПараметры) Экспорт
	Если Результат = КодВозвратаДиалога.Нет Тогда
	    Возврат;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(IDЗадания) Тогда
	    торо_ИнтеграцияСАИСДиспетчер.ОтменитьВыполнениеФоновогоЗадания(IDЗадания);
	КонецЕсли;
	
	Элементы.СтраницыОбменаДанными.ТекущаяСтраница = Элементы.ОбменЗавершенСОшибкой;
	
	Закрыть(РезультатВыполнения);
КонецПроцедуры

#Область ФоновыеЗадания

&НаКлиенте
Процедура ЗапуститьВыполнение()
	ДатаНачалаСинхронизации = ТекущаяДата();
	ПроцентВыполнения = 0;
	
	СтруктураФоновогоЗадания = ЗапуститьВыполнениеНаСервере(ДанныеЗадания);
	
	Если Не ЗначениеЗаполнено(СтруктураФоновогоЗадания) Тогда
	    Элементы.СтраницыОбменаДанными.ТекущаяСтраница = Элементы.ОбменЗавершенСОшибкой;
		Возврат;
	КонецЕсли;
	
	IDЗадания = СтруктураФоновогоЗадания.ИдентификаторЗадания;
	
	ПараметрыОжидания = ДлительныеОперацииКлиент.ПараметрыОжидания(ЭтотОбъект);
	ПараметрыОжидания.ВыводитьОкноОжидания = Ложь;
	ПараметрыОжидания.ВыводитьПрогрессВыполнения = Истина;
	Если ДанныеЗадания.ПодключитьОбработчикОповещенияОПрогрессе Тогда
	    ПараметрыОжидания.ОповещениеОПрогрессеВыполнения = Новый ОписаниеОповещения(ДанныеЗадания.ОбработчикОповещенияОПрогрессе, ЭтотОбъект);
	КонецЕсли;
	
	Если ДанныеЗадания.ПодключитьОбработчикЗавершения Тогда
	    ДлительныеОперацииКлиент.ОжидатьЗавершение(СтруктураФоновогоЗадания,
													Новый ОписаниеОповещения(ДанныеЗадания.ОбработчикЗавершения, ЭтотОбъект),
													ПараметрыОжидания);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Функция ЗапуститьВыполнениеНаСервере(ДанныеЗадания)
	ПараметрыЗапуска = ПолучитьИзВременногоХранилища(АдресПараметровЗапуска);
	
	СтруктураФоновогоЗадания = торо_ИнтеграцияСАИСДиспетчер.ВыполнитьФоновоеЗаданиеНаСервере(УникальныйИдентификатор, ДанныеЗадания, ПараметрыЗапуска);
	Если Не ЗначениеЗаполнено(СтруктураФоновогоЗадания) Тогда
		Возврат Новый Структура();
	КонецЕсли;
	
	Возврат СтруктураФоновогоЗадания;
КонецФункции

#Область ОбработчикиОповещенийФоновыхЗаданий

&НаКлиенте 
Процедура ОбработчикОповещенияОПрогрессе(Результат, ДополнительныеПараметры) Экспорт
	Прогресс = Результат.Прогресс;
	Если Не ТипЗнч(Прогресс) = Тип("Структура")
		Или ТипЗнч(Прогресс) = Тип("Структура") И Прогресс.Свойство("ЗавершеноАварийно")
		Или Результат.Свойство("Статус") И Результат.Статус = "Выполнено" Тогда
		Возврат;
	КонецЕсли;
	
	Если Прогресс.Свойство("Текст") Тогда
		Элементы.СтатусОбменаДанными.Заголовок = Прогресс.Текст;
	КонецЕсли;
	
	Если Прогресс.Свойство("Процент") Тогда
		ПроцентВыполнения = Прогресс.Процент;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ОбработчикОкончанияФоновогоЗадания(Результат, ДополнительныеПараметры) Экспорт
	Если Результат = Неопределено Тогда
		Элементы.СтраницыОбменаДанными.ТекущаяСтраница = Элементы.ОбменЗавершенСОшибкой;
		Возврат;
	КонецЕсли;
	
	РезультатВыполнения = ОбработчикОкончанияФоновогоЗаданияНаСервереБезКонтекста(Результат.АдресРезультата);
	
	ОбработатьПереходСтраниц(РезультатВыполнения.Статус = "Ошибка");
	
	Если ЗакрыватьПриЗавершении Тогда
		Закрыть(РезультатВыполнения);
	КонецЕсли;
КонецПроцедуры

&НаСервереБезКонтекста
Функция ОбработчикОкончанияФоновогоЗаданияНаСервереБезКонтекста(АдресРезультата)
	Результат = ПолучитьИзВременногоХранилища(АдресРезультата);
	АдресРезультата = ПоместитьВоВременноеХранилище(Результат);
	СтруктураВозврата = Новый Структура("Статус, АдресРезультата", Результат.Статус, АдресРезультата);
	Возврат СтруктураВозврата;
КонецФункции

&НаКлиенте
Процедура ПерейтиВЖурналРегистрации(Команда)
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("СобытиеЖурналаРегистрации", торо_ИнтеграцияСАИСДиспетчер.ПолучитьСобытияЖурналаРегистрации());
	ПараметрыФормы.Вставить("ДатаНачала", ДатаНачалаСинхронизации);
	ПараметрыФормы.Вставить("ДатаОкончания", ТекущаяДата());
	ОткрытьФорму("Обработка.ЖурналРегистрации.Форма", ПараметрыФормы, ЭтотОбъект);
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецОбласти