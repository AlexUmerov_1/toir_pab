
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПометитьНаУдалениеНеиспользуемыеУзлы(Команда)
	ОткрытьФорму("Обработка.торо_МобильныеПриложения.Форма.ФормаПометитьНаУдалениеНеиспользуемыеУзлы", Новый Структура("МобильноеПриложение", ПредопределенноеЗначение("Перечисление.торо_МобильноеПриложение.МобильныеБригады")), ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	Если ИмяСобытия = "ПометитьНаУдалениеНеиспользуемыеУзлы" Тогда
		Элементы.Список.Обновить();
	КонецЕсли;
КонецПроцедуры

#КонецОбласти
