
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка) 

	Объект.РежимВыгрузкиДокументов = Перечисления.РежимыВыгрузкиОбъектовОбмена.ВыгружатьПоУсловию;
	Объект.РежимВыгрузкиПоУсловию  = Перечисления.РежимыВыгрузкиОбъектовОбмена.ВыгружатьПоУсловию;
	объект.РежимВыгрузкиПриНеобходимости = Перечисления.РежимыВыгрузкиОбъектовОбмена.ВыгружатьПриНеобходимости;
		
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если Объект.ИспользоватьОтборПоОрганизациям Тогда
		ОтборПоОрганизациям = "ПоВыбраннымОрганизациям";
	Иначе
		ОтборПоОрганизациям = "ПоВсемОрганизациям"; 
	КонецЕсли;
	
	Если Объект.ИспользоватьОтборПоСкладам Тогда
		ОтборПоСкладам = "ПоВыбраннымСкладам";
	Иначе
		ОтборПоСкладам = "ПоВсемСкладам"; 
	КонецЕсли;
	
	ОтборПоОрганизациямПриИзменении(Истина);
	ОтборПоСкладамПриИзменении(Истина);
КонецПроцедуры

&НаСервере
Процедура ПриЗаписиНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	ОбменДаннымиСервер.ФормаУзлаПриЗаписиНаСервере(ТекущийОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ОтборПоОрганизациямПриИзменении(Элемент)
	Элементы.Организации.Видимость = (ОтборПоОрганизациям = "ПоВыбраннымОрганизациям");
	Объект.ИспользоватьОтборПоОрганизациям = (ОтборПоОрганизациям = "ПоВыбраннымОрганизациям");
КонецПроцедуры

&НаКлиенте
Процедура ОтборПоСкладамПриИзменении(Элемент)
	Элементы.Склады.Видимость = (ОтборПоСкладам  = "ПоВыбраннымСкладам");
	Объект.ИспользоватьОтборПоСкладам = (ОтборПоСкладам = "ПоВыбраннымСкладам");
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСклады

&НаКлиенте
Процедура СкладыПередОкончаниемРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования, Отказ)
	ПередОкончаниемРедактированияТЧ("Склады", "Склад", ОтменаРедактирования, Отказ);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыОрганизации

&НаКлиенте
Процедура ОрганизацииПередОкончаниемРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования, Отказ)
	ПередОкончаниемРедактированияТЧ("Организации", "Организация", ОтменаРедактирования, Отказ);
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ПередОкончаниемРедактированияТЧ(ИмяТЧ, ИмяКолонки, ОтменаРедактирования, Отказ)
	Если ОтменаРедактирования Тогда
	    Возврат;
	КонецЕсли;
	
	ТекущиеДанные = Элементы[ИмяТЧ].ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
	    Возврат;
	КонецЕсли;
	
	СтруктураПоиска = Новый Структура(ИмяКолонки, ТекущиеДанные[ИмяКолонки]);
	НайденныеСтроки = Объект[ИмяТЧ].НайтиСтроки(СтруктураПоиска);
	Если НайденныеСтроки.Количество() < 2 Тогда
	    Возврат;
	КонецЕсли;
	
	ТекстСообщения = НСтр("ru='Данное значение уже добавлено!'");
	ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения,,,, Отказ);
КонецПроцедуры

#КонецОбласти
	  