#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(Параметры.КлючНазначенияИспользования) Тогда
		Если Параметры.КлючНазначенияИспользования = "ОбъектыРемонта" И Параметры.Свойство("СписокОтбора") Тогда
			ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Ссылка", Параметры.СписокОтбора, ВидСравненияКомпоновкиДанных.НеВСписке,,,РежимОтображенияЭлементаНастройкиКомпоновкиДанных.Недоступный);
		КонецЕсли;
	КонецЕсли;
	
	МультиязычностьСервер.ПриСозданииНаСервере(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти